<?php

/*----------------form text-------------------------------------*/
$lang['send_verification_heading_text'] = 'Send Verification email';
$lang['send_verification_subheading_text'] = '';

$lang['label_email_text'] = 'Email address';
$lang['placeholder_email_text'] = 'Enter email address';

$lang['verification_email_send_success_text'] = 'Verification email is sent';
$lang['user_email_not_found_text'] = 'No user is found by the email address';


$lang['submit_btn_text'] = 'Send';
$lang['login_text'] = 'Login';
$lang['register_text'] = 'Register';

/*----------------mail text-------------------------*/
$lang['verification_subject_text'] = 'Verify your account';
$lang['verification_message_text'] = 'Click the lik below to verify your account';
$lang['link_text'] = 'Verify';