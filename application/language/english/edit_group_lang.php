<?php
/*page texts*/
$lang['page_title_text'] = 'Edit Group';
$lang['page_subtitle_text'] = 'Group\'s information';
$lang['box_title_text'] = 'Group Information Form';
$lang['no_group_found_text'] = 'No Group Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Groups';
$lang['breadcrumb_page_text'] = 'Edit Group';

/*Add user form texts*/
$lang['label_group_name_text'] = 'Group Name';
$lang['label_group_description_text'] = 'Group Description';



$lang['placeholder_group_name_text'] = 'Enter Group\'s Name';
$lang['placeholder_group_description_text'] = 'Enter Group Description';



$lang['button_submit_text'] = 'Update Group';

/*validation error texts*/

$lang['group_name_required_text'] = 'Group Name is Required';
$lang['group_name_alpha_dash_text'] = 'The Group Name field may only contain alpha-numeric characters, underscores, and dashes.';
