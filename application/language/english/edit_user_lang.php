<?php
/*page texts*/
$lang['page_title_text'] = 'Edit User';
$lang['page_subtitle_text'] = 'Edit user\'s basic information';
$lang['box_title_text'] = 'User Information Form';
$lang['no_user_found_text'] = 'No User Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Users';
$lang['breadcrumb_page_text'] = 'Edit User';

/*Add user form texts*/
$lang['label_firstname_text'] = 'First Name';
$lang['label_lastame_text'] = 'Last Name';
$lang['label_company_text'] = 'Company';
$lang['label_email_text'] = 'Email Address';
$lang['label_phone_text'] = 'Phone Number';
$lang['label_company_name_text'] = 'Company Name';
$lang['label_password_text'] = 'Password';
$lang['label_confirm_password_text'] = 'Confirm Password';

$lang['placeholder_firstname_text'] = 'Enter First Name';
$lang['placeholder_lastame_text'] = 'Enter Last Name';
$lang['placeholder_company_text'] = 'Enter Company';
$lang['placeholder_email_text'] = 'Enter a valid Email Address ';
$lang['placeholder_phone_text'] = 'Enter Phone Number';
$lang['placeholder_company_name_text'] = 'Enter Company Name ';
$lang['placeholder_password_text'] = 'Enter Password';
$lang['placeholder_confirm_password_text'] = 'Confirm Entered Password';

$lang['only_if_necessary_text'] = '(only if necessary)';

$lang['add_an_user_text'] = 'Add A User';
$lang['button_submit_text'] = 'Update User';

/*validation error texts*/
$lang['identity_text'] = 'Username/Email';
$lang['firstname_required_text'] = 'First Name is Required';
$lang['lastname_required_text'] = 'Last Name is Required';
$lang['company_required_text'] = 'Company is Required';

$lang['identity_required_text'] = 'Username/Email is rquired';
$lang['identity_not_unique_text'] = 'Username/Email already exists';

$lang['email_required_text'] = 'Email Address is required';
$lang['email_not_valid_text'] = 'Email Address is not valid';
$lang['email_not_unique_text'] = 'Email Address already exists';

$lang['password_required_text'] = 'Password is required';
$lang['password_min_length_text'] = 'Minimum length of password is: ';
$lang['password_max_length_text'] = 'Maximum length of password is: ';
$lang['password_not_match_text'] = 'Passwords do not match';

$lang['confirm_password_required_text'] = 'Password Confirmation is required';

/*group selection validation*/
$lang['group_selection_error_text'] = 'Cannot select %1$s and %2$s at the same time';
$lang['group_selection_error_pg_thrift_text'] = 'Cannot select %1$s  at the same time';

$lang['group_selection_error_pg_thrift_text'] = 'This account cannot be %1$s  account at the same time';

/*force tf*/
$lang['label_google_tf_auth_forced_text'] = 'Force Google Two Factor Authentication?';
$lang['yes_text'] = 'Yes';
$lang['no_text'] = 'No';

?>
