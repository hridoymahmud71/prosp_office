<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *   This Library depends on the Settings_model
 *      Remember to create a settings model if there is none
 *          or
 *      Put the configs manually
 * */

class Custom_image_library
{

    public $CI;


    public $if_settings_type_exists;
    public $all_image_settings = array();

    public function __construct()
    {

        $this->CI = &get_instance();

        $this->CI->load->library('upload');
        $this->CI->load->library('image_lib');
        $this->CI->load->library('session');


        $this->CI->lang->load('image');

    }


    public function getMainImageDirectory()
    {
        return $this->CI->config->item('pg_upload_path') . '/image/';

    }

    public function getThumbImageDirectory()
    {
        return $this->CI->config->item('pg_upload_path') . '/image/thumb/';

    }


    /*
     *  uploadImage gets called from the controllers
     * @params: $file_details  - array  //file details contains $_FILES['field_name']
     *          $field_name    - string
     *
     *          exmample:   $field_name = 'site_logo';
                            $file_details = $_FILES['site_logo'];
     *
     * @return: if true
     *              $image_details - array
     *          else
     *              false   -   bool
     *
     * @flashdata: for errors:  image_upload_errors
     *                          image_resize_errors
     *            for success:  image_upload_success
     *                          image_resize_success
     *
     *  */
    public function uploadImage($file_details, $field_name)
    {
        //file details contains $_FILES['field_name']
        $image_config = $this->getImageConfig($file_details['name']);

        $this->CI->upload->initialize($image_config);

        //do_upload uploads the file
        if (!$this->CI->upload->do_upload($field_name)) {
            $this->CI->session->set_flashdata('image_upload_errors', $this->CI->upload->display_errors());
            return false;
        } else {
            $this->CI->session->set_flashdata('image_upload_success', $this->CI->lang->line('image_upload_success'));
        }

        //$this->upload->data() returns uploaded file's details
        $image_details = $this->CI->upload->data();

        $thumb_config = $this->getThumbConfig($image_details);

        $this->CI->image_lib->initialize($thumb_config);

        //create_thumb
        if (!$this->CI->image_lib->resize()) {
            $this->CI->session->set_flashdata('image_resize_errors', $this->CI->image_lib->display_errors());
            return false;
        } else {
            $this->CI->session->set_flashdata('image_resize_success', $this->CI->lang->line('image_resize_success'));
            //image details is returned only after resizing
            return $image_details;
        }

    }


    /*
     * ----------read carefully----------------------------------------------------
     *
     * This func returns image details with success ond error messages together
     *
     * call the function and print the return array to understand
     *
     * */
    public function uploadImage_revisedFunc($file_details, $field_name)
    {
        //file details contains $_FILES['field_name']
        $image_config = $this->getImageConfig($file_details['name']);

        $this->CI->upload->initialize($image_config);

        //do_upload uploads the file

        $messages = array();
        $return_array = array();

        if (!$this->CI->upload->do_upload($field_name)) {
            $messages['image_upload_error'] = $this->CI->upload->display_errors();
            $return_array['image_upload_error'][] = $messages['image_upload_error'];
            $return_array['image_upload_success'][] = 'no_upload_success';
            $image_details = $this->CI->upload->data();
            $return_array['image_details'][] = $image_details;
            return $return_array;

        } else {
            $messages['image_upload_success'] = $this->CI->lang->line('image_upload_success');
            $return_array['image_upload_success'][] = $messages['image_upload_success'];
            $return_array['image_upload_error'][] = 'no_upload_error';

            /*$image_details = $this->CI->upload->data();
            $return_array[] = $image_details;*/

        }

        //$this->upload->data() returns uploaded file's details
        $image_details = $this->CI->upload->data();

        $thumb_config = $this->getThumbConfig($image_details);

        $this->CI->image_lib->initialize($thumb_config);

        //create_thumb
        if (!$this->CI->image_lib->resize()) {
            $messages['image_resize_error'] = $this->CI->image_lib->display_errors();
            $return_array['image_resize_error'][] = $messages['image_resize_error'];
            $return_array['image_resize_success'][] = 'no_resize_success';

            $return_array['image_details'][] = $image_details;

            return $return_array;
        } else {
            $messages['image_resize_success'] = $this->CI->lang->line('image_resize_success');
            $return_array['image_resize_success'][] = $messages['image_resize_success'];
            $return_array['image_resize_error'][] = 'no_resize_error';

            $image_details = $this->CI->upload->data();
            $return_array['image_details'][] = $image_details;

            return $return_array;
        }

    }


    public function getImageConfig($image_name)
    {
        $config['encrypt_name'] = TRUE;
        $config['file_name'] = $image_name;
        $config['upload_path'] = $this->getMainImageDirectory();
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 5120;
        $config['max_width'] = 4000;
        $config['max_height'] = 4000;


        return $config;
    }


    public function getThumbConfig($image_details)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $this->getMainImageDirectory() . $image_details['file_name'];
        $config['new_image'] = $this->getThumbImageDirectory()  . $image_details['file_name'];
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '';
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 75;


        return $config;
    }

}