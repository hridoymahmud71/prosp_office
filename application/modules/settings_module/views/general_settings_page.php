<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #office-logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #partner-logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #thrift-logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #trustee-logo-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #banner-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .img-preview {
        max-width: 50%;
    }
</style>

<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrumb_home_text') ?></a></li>
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrumb_section_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">
                    <?php if ($this->session->flashdata('group_add_success')) { ?>

                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="add-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#add-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('add_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('group_update_success')) { ?>
                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#update-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('update_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h5 class="box-title"><?php echo lang('page_subtitle_text') ?></h5>

                                        <!-- <div class="col-md-offset-2 col-md-8" style="color: maroon;font-size: larger"> -->
                                        <?php if ($this->session->flashdata('validation_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?php echo $this->session->flashdata('validation_errors') ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!-- </div> -->

                                        <?php if ($this->session->flashdata('image_upload_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_upload_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <?php if ($this->session->flashdata('image_resize_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_resize_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <!--demo-->
                                        <?php if ($this->session->flashdata('file_upload_errors')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('file_upload_errors'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--demo-->

                                    </div>
                                    <div class="col-md-2"></div>

                                    <div class=" col-md-offset-2 col-md-12" style="color: darkgreen;font-size: larger">
                                        <!--demo-->
                                        <?php if ($this->session->flashdata('file_upload_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-danger alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('file_upload_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--demo-->
                                        <br>
                                        <?php if ($this->session->flashdata('update_success_text')) { ?>
                                            <div class="text-center alert alert-success alert-dismissible fade show"
                                                 role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <strong><?php echo lang('update_success_text') ?></strong>
                                            </div>
                                        <?php } ?>

                                        <?php if ($this->session->flashdata('image_upload_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-success alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_upload_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <br>
                                        <?php if ($this->session->flashdata('image_resize_success')) { ?>
                                            <div class="col-md-6">
                                                <div class="text-center alert alert-success alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                    <strong><?= $this->session->flashdata('image_resize_success'); ?></strong>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <!-- form start -->
                                <form action="<?php echo base_url() . 'settings_module/update_general_settings' ?>"
                                      role="form"
                                      id="" method="post" enctype="multipart/form-data">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="site_name"><?php echo lang('label_site_name_text') ?></label>

                                            <input type="text" name="site_name" class="form-control" id="site_name"
                                                   value="<?php
                                                   if ($all_general_settings) {
                                                       foreach ($all_general_settings as $a_general_settings) {
                                                           if (($a_general_settings->settings_key) == 'site_name')
                                                               echo $a_general_settings->settings_value;
                                                       }
                                                   }
                                                   ?>"
                                                   placeholder="">
                                        </div>

                                        <div class="form-group">
                                            <label for="site_email"><?php echo lang('label_site_email_text') ?></label>

                                            <input type="text" name="site_email" class="form-control" id="site_email"
                                                   value="<?php
                                                   if ($all_general_settings) {
                                                       foreach ($all_general_settings as $a_general_settings) {
                                                           if (($a_general_settings->settings_key) == 'site_email')
                                                               echo $a_general_settings->settings_value;
                                                       }
                                                   }
                                                   ?>"
                                                   placeholder="<?php echo lang('placeholder_site_email_text') ?>">
                                        </div>

                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_found = false; ?>

                                        <div class="form-group">
                                            <label for="site_logo"><?php echo lang('label_site_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'site_logo')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_site_logo_text') ?></span>
                                                        <input id="logo-id" name="site_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_site_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_a_found = false; ?>

                                        <div class="form-group">
                                            <label for="office_site_logo"><?php echo lang('label_office_site_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'office_site_logo')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_a_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-office-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_a_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-office-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_office_site_logo_text') ?></span>
                                                        <input id="office-logo-id" name="office_site_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_office_site_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_b_found = false; ?>

                                        <div class="form-group">
                                            <label for="partner_site_logo"><?php echo lang('label_partner_site_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'partner_site_logo')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_b_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-partner-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_b_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-partner-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_partner_site_logo_text') ?></span>
                                                        <input id="partner-logo-id" name="partner_site_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_partner_site_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_c_found = false; ?>

                                        <div class="form-group">
                                            <label for="thrift_site_logo"><?php echo lang('label_thrift_site_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'thrift_site_logo')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_c_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-thrift-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_c_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-thrift-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_thrift_site_logo_text') ?></span>
                                                        <input id="thrift-logo-id" name="thrift_site_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_thrift_site_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image_d_found = false; ?>

                                        <div class="form-group">
                                            <label for="trustee_site_logo"><?php echo lang('label_trustee_site_logo_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'trustee_site_logo')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image_d_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-trustee-logo"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Logo" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image_d_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-trustee-logo"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Logo">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadLogo" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_trustee_site_logo_text') ?></span>
                                                        <input id="trustee-logo-id" name="trustee_site_logo" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_trustee_site_logo_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->


                                        <!--image upload snippet starts-->
                                        <!--if true image from database, else static image //currently not working -->
                                        <?php $image2_found = false; ?>

                                        <div class="form-group">
                                            <label for="site_banner"><?php echo lang('label_site_banner_text') ?></label>
                                            <div class="main-img-preview">

                                                <?php if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key == 'site_banner')
                                                            && ($a_general_settings->settings_value != '')
                                                        ) {
                                                            ?>
                                                            <div><?php $image2_found = true ?></div>
                                                            <img class="thumbnail img-preview img-preview-banner"
                                                                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $a_general_settings->settings_value ?>"
                                                                 title="Preview Banner" alt="">
                                                        <?php }
                                                    }
                                                } ?>

                                                <?php if ($image2_found == false) { ?>

                                                    <img class="thumbnail img-preview img-preview-banner"
                                                         src="<?php echo base_url()
                                                             . 'base_demo_images/placeholder_image_demo.jpg' ?>"
                                                         title="Preview Banner">

                                                <?php } ?>

                                            </div>

                                            <div class="input-group">
                                                <input id="fakeUploadBanner" class="form-control fake-shadow"
                                                       placeholder="Choose File" disabled="disabled">
                                                <div class="input-group-btn">
                                                    <div class="fileUpload btn btn-primary fake-shadow">
                                                        <span><i class="glyphicon glyphicon-upload"></i> <?php echo lang('upload_site_logo_text') ?></span>
                                                        <input id="banner-id" name="site_banner" type="file"
                                                               value=""
                                                               class="attachment_upload">
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="help-block"><?php echo lang('help_site_banner_text') ?></p>
                                        </div>
                                        <!--image upload snippet ends-->

                                        <div class="form-group">
                                            <label for="thrifter_registration_terms_html"><?php echo lang('label_thrifter_registration_terms_html_text') ?></label>

                                            <textarea name="thrifter_registration_terms_html"
                                                      id="thrifter_registration_terms_html" cols="30" rows="10"><?php
                                                if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key) == 'thrifter_registration_terms_html')
                                                            echo $a_general_settings->settings_value;
                                                    }
                                                }
                                                ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label for="site_maintenance_html"><?php echo lang('label_site_maintenance_html_text') ?></label>

                                            <textarea name="site_maintenance_html"
                                                      id="site_maintenance_html" cols="30" rows="10"><?php
                                                if ($all_general_settings) {
                                                    foreach ($all_general_settings as $a_general_settings) {
                                                        if (($a_general_settings->settings_key) == 'site_maintenance_html')
                                                            echo $a_general_settings->settings_value;
                                                    }
                                                }
                                                ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label for="partner_site_under_maintenance"><?php echo lang('label_partner_site_under_maintenance_text') ?>
                                            </label>

                                            <select class="form-control" name="partner_site_under_maintenance" id="">
                                                <option value="no"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'partner_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'no') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('partner_site_under_maintenance_no_text') ?></option>
                                                <option value="yes"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'partner_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'yes') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('partner_site_under_maintenance_yes_text') ?></option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="thrift_site_under_maintenance"><?php echo lang('label_thrift_site_under_maintenance_text') ?>
                                            </label>

                                            <select class="form-control" name="thrift_site_under_maintenance" id="">
                                                <option value="no"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'thrift_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'no') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('thrift_site_under_maintenance_no_text') ?></option>
                                                <option value="yes"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'thrift_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'yes') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('thrift_site_under_maintenance_yes_text') ?></option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="trustee_site_under_maintenance"><?php echo lang('label_trustee_site_under_maintenance_text') ?>
                                            </label>

                                            <select class="form-control" name="trustee_site_under_maintenance" id="">
                                                <option value="no"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'trustee_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'no') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('trustee_site_under_maintenance_no_text') ?></option>
                                                <option value="yes"
                                                    <?php
                                                    if ($all_general_settings) {
                                                        foreach ($all_general_settings as $a_general_settings) {
                                                            if (($a_general_settings->settings_key) == 'trustee_site_under_maintenance')
                                                                if ($a_general_settings->settings_value == 'yes') {
                                                                    echo ' selected ';
                                                                }
                                                        }
                                                    }
                                                    ?>
                                                ><?= lang('trustee_site_under_maintenance_yes_text') ?></option>
                                            </select>
                                        </div>


                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">


                                        <button type="submit" id="btnsubmit"
                                                class="btn btn-primary"><?php echo lang('button_submit_text') ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                </div>
                <!-- /.row -->
                </section>
                <!-- /.content -->
                <div class="clearfix"></div>
            </div>
        </div><!-- end col -->
    </div><!-- end col -->
</div>
<!-- end row -->
</div>
<!-- </div> -->
<!-- </div> -->


<script>
    $(document).ready(function () {
        var brand = document.getElementById('logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#logo-id").change(function () {
            readURL(this);
        });

    });


</script>

<script>
    $(document).ready(function () {
        var brand = document.getElementById('office-logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-office-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#office-logo-id").change(function () {
            readURL(this);
        });

    });


</script>

<script>
    $(document).ready(function () {
        var brand = document.getElementById('partner-logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-partner-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#partner-logo-id").change(function () {
            readURL(this);
        });

    });


</script>

<script>
    $(document).ready(function () {
        var brand = document.getElementById('thrift-logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-thrift-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#thrift-logo-id").change(function () {
            readURL(this);
        });

    });


</script>


<script>
    $(document).ready(function () {
        var brand = document.getElementById('trustee-logo-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadLogo').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-trustee-logo').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#trustee-logo-id").change(function () {
            readURL(this);
        });

    });


</script>


<script>
    $(document).ready(function () {
        var brand = document.getElementById('banner-id');
        brand.className = 'attachment_upload';
        brand.onchange = function () {
            document.getElementById('fakeUploadBanner').value = this.value.substring(12);
        };

        // Source: http://stackoverflow.com/a/4459419/6396981
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-preview-banner').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#banner-id").change(function () {
            readURL(this);
        });
    });


</script>

<script>
    $(function () {
        tinymce.init({
            selector: '#thrifter_registration_terms_html',
            fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });

        tinymce.init({
            selector: '#site_maintenance_html',
            height: 500,
            menubar: true,
            fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });
    });
</script>