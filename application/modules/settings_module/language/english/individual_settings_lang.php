<?php

/*page texts*/
$lang['page_title_text'] = 'Individual Settings';
$lang['page_subtitle_text'] = 'Edit and update individual settings here';
$lang['box_title_text'] = 'Individual Settings Form';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Individual Settings';

/*Individual settings form texts*/

$lang['color_separator_lang'] = 'Color';
$lang['label_individual_chosen_color_text'] = 'Individual Chosen Color';
$lang['label_individual_chosen_color_deeper_text'] = 'Individual Chosen Color (Deeper)';

$lang['label_individual_logo_text'] = 'Individual Logo';
$lang['help_individual_logo_text'] = 'Upload Individual Logo';




$lang['upload_individual_logo_text'] = 'Upload Logo';



$lang['button_submit_text'] = 'Update Individual Settings';



/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Individual Settings';

