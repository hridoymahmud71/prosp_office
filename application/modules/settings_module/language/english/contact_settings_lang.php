<?php

/*page texts*/
$lang['page_title_text'] = 'Contact Settings';
$lang['page_subtitle_text'] = 'Edit and update contact settings here';
$lang['box_title_text'] = 'Contact Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Contact Settings';

$lang['admins_contact_separator_lang'] = 'Admin\'s Contact';
$lang['companys_contact_separator_lang'] = 'Company\'s Contact';

$lang['companys_social_media_links_separator_lang'] = 'Company\'s Social Media Links';

$lang['button_submit_text'] = 'Update Contact Settings';

/*Contact settings form texts*/
$lang['label_admin_contact_email_text'] = 'Admin\'s Contact Email';
$lang['label_admin_contact_phone_text'] = 'Admin\'s Contact Phone';
$lang['label_company_contact_email_text'] = 'Company\'s Contact Email';
$lang['label_company_contact_phone_text'] = 'Company\'s Contact Phone';
$lang['label_company_contact_address_text'] = 'Company\'s Contact Address';

$lang['label_company_facebook_id_text'] = 'Company\'s Facebook ID';
$lang['label_company_twitter_id_text'] = 'Company\'s Twitter ID';
$lang['label_company_youtube_id_text'] = 'Company\'s Youtube ID';

$lang['placeholder_admin_contact_email_text'] = 'Enter Admin\'s Contact Email';
$lang['placeholder_admin_contact_phone_text'] = 'Enter Admin\'s Contact Phone';
$lang['placeholder_company_contact_email_text'] = 'Enter Company\'s Contact Email';
$lang['placeholder_company_contact_phone_text'] = 'Enter Company\'s Contact Phone';
$lang['placeholder_company_contact_address_text'] = 'Enter Company\'s Contact Address';

$lang['placeholder_company_facebook_id_text'] = 'example : facebook.com/YourId';
$lang['placeholder_company_twitter_id_text'] = 'example : twitter.com/YourId';
$lang['placeholder_company_youtube_id_text'] = 'example : youtube.com/YourId';

/*validation error texts*/
$lang['admin_contact_email_valid_email_text'] = 'Admin\'s Contact Email is not valid';
$lang['company_contact_email_valid_email_text'] = 'Company\'s Contact Email is not valid';

/*other texts*/
$lang['noting_to_update_text'] = 'Nothing To Update';

/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Contact Settings';

