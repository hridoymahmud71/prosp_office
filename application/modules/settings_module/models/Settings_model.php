<?php

class Settings_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function ifSettingsExist($a_settings_code,$a_settings_key)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);

        $query_result = $this->db->get();
        $num_rows = $query_result->num_rows();

        if($num_rows > 0) {
            return true;
        }
        else{
            return false ;
        }
    }

    public function ifSettingsTypeExist($a_settings_code)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_settings');
        $this->db->where('settings_code',$a_settings_code);

        $query_result = $this->db->get();
        $num_rows = $query_result->num_rows();

        if($num_rows > 0) {
            return true;
        }
        else{
            return false ;
        }
    }

    public function addSettings($a_settings_code,$a_settings_key,$a_settings_value)
    {
        $this->db->set('settings_code', $a_settings_code);
        $this->db->set('settings_key', $a_settings_key);
        $this->db->set('settings_value', $a_settings_value);
        $this->db->insert('rspm_tbl_settings');
    }

    public function updateSettings($a_settings_code,$a_settings_key,$a_settings_value)
    {
        $this->db->set('settings_value',$a_settings_value );
        $this->db->where('settings_code', $a_settings_code);
        $this->db->where('settings_key', $a_settings_key);
        $this->db->update('rspm_tbl_settings');
    }

    public function getASetting($a_settings_code,$a_settings_key)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);

        $query_result = $this->db->get();
        $row = $query_result->row();

        return $row;

    }

    public function getASettingsValue($a_settings_code,$a_settings_key)
    {
        $this->db->select('settings_value');
        $this->db->from('rspm_tbl_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);

        $query_result = $this->db->get();
        $row = $query_result->row();

        return $row;

    }

    public function getSettings($a_settings_code)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_settings');
        $this->db->where('settings_code',$a_settings_code);

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //------------------------------------------------------------------------------------------------------------------
    public function ifUserSettingsExist($a_settings_code,$a_settings_key,$user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);
        $this->db->where('user_id',$user_id);

        $query_result = $this->db->get();
        $num_rows = $query_result->num_rows();

        if($num_rows > 0) {
            return true;
        }
        else{
            return false ;
        }
    }

    public function ifUserSettingsTypeExist($a_settings_code,$user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('user_id',$user_id);

        $query_result = $this->db->get();
        $num_rows = $query_result->num_rows();

        if($num_rows > 0) {
            return true;
        }
        else{
            return false ;
        }
    }

    public function addUserSettings($a_settings_code,$a_settings_key,$a_settings_value,$user_id)
    {
        $this->db->set('settings_code', $a_settings_code);
        $this->db->set('settings_key', $a_settings_key);
        $this->db->set('settings_value', $a_settings_value);
        $this->db->set('user_id', $user_id);
        $this->db->insert('rspm_tbl_user_settings');

    }

    public function updateUserSettings($a_settings_code,$a_settings_key,$a_settings_value,$user_id)
    {
        $this->db->set('settings_value',$a_settings_value );
        $this->db->where('settings_code', $a_settings_code);
        $this->db->where('settings_key', $a_settings_key);
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_settings');
    }

    public function getUserASetting($a_settings_code,$a_settings_key,$user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);
        $this->db->where('user_id',$user_id);

        $query_result = $this->db->get();
        $row = $query_result->row();

        return $row;

    }

    public function getAUserSettingsValue($a_settings_code,$a_settings_key,$user_id)
    {
        $this->db->select('settings_value');
        $this->db->from('rspm_tbl_user_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('settings_key',$a_settings_key);
        $this->db->where('user_id',$user_id);

        $query_result = $this->db->get();
        $row = $query_result->row();

        return $row;

    }

    public function getUserSettings($a_settings_code,$user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_settings');
        $this->db->where('settings_code',$a_settings_code);
        $this->db->where('user_id',$user_id);

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

}
