<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->library('image_lib');

        //customized lib from application/libraries
        $this->load->library('custom_image_library');
        $this->load->library('custom_file_library');


        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_log_library');

        $this->load->helper(array('form', 'url'));

        $this->lang->load('general_settings');


    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        } else {

            $a_settings_code = 'general_settings';
            $data['all_general_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            // $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/general_settings_page", $data);
            } else {
                $this->load->view("settings_module/general_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updateGeneralSettings()
    {
        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        $data['site_name'] = trim($this->input->post('site_name'));
        $data['site_email'] = trim($this->input->post('site_email'));
        $data['thrifter_registration_terms_html'] = trim($this->input->post('thrifter_registration_terms_html'));
        $data['site_maintenance_html'] = trim($this->input->post('site_maintenance_html'));

        $data['partner_site_under_maintenance'] = $this->input->post('partner_site_under_maintenance');
        $data['thrift_site_under_maintenance'] = $this->input->post('thrift_site_under_maintenance');
        $data['trustee_site_under_maintenance'] = $this->input->post('trustee_site_under_maintenance');

        /*echo "<pre>";
        print_r($_POST);die();*/


        $this->form_validation->set_rules('site_name', 'site_name', 'required',
            array(
                'required' => $this->lang->line('site_name_required')
            )
        );


        $this->form_validation->set_rules('site_email', 'site_email', 'required|valid_email',
            array(
                'required' => $this->lang->line('site_email_required'),
                'valid_email' => $this->lang->line('site_email_not_valid')
            )
        );

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('settings_module/general_settings');
        } else {

            /*uploading image files starts*/
            if (($_FILES['site_logo']['name']) != '') {

                $field_name = 'site_logo';
                $file_details = $_FILES['site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['site_logo'] = $site_logo_image_details['file_name'];
            }

            if (($_FILES['office_site_logo']['name']) != '') {

                $field_name = 'office_site_logo';
                $file_details = $_FILES['office_site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['office_site_logo'] = $site_logo_image_details['file_name'];
            }

            if (($_FILES['partner_site_logo']['name']) != '') {

                $field_name = 'partner_site_logo';
                $file_details = $_FILES['partner_site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['partner_site_logo'] = $site_logo_image_details['file_name'];
            }

            if (($_FILES['thrift_site_logo']['name']) != '') {

                $field_name = 'thrift_site_logo';
                $file_details = $_FILES['thrift_site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['thrift_site_logo'] = $site_logo_image_details['file_name'];
            }

            if (($_FILES['trustee_site_logo']['name']) != '') {

                $field_name = 'trustee_site_logo';
                $file_details = $_FILES['trustee_site_logo'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_logo_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_logo_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['trustee_site_logo'] = $site_logo_image_details['file_name'];
            }

            if (($_FILES['site_banner']['name']) != '') {

                $field_name = 'site_banner';
                $file_details = $_FILES['site_banner'];

                /*
                 * Function: uploadImage()
                 * @params: $file_details  - array
                 *          $field_name    - string
                 *
                 * @return: if true
                 *              $image_details - array
                 *          else
                 *              false   -   bool
                 *  */

                $site_banner_image_details = $this->custom_image_library->uploadImage($file_details, $field_name);
                if ($site_banner_image_details == false) {
                    redirect('settings_module/general_settings');
                }
                $data['site_banner'] = $site_banner_image_details['file_name'];
            }
            /*uploading image files ends*/

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'general_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'general_settings',                                                     //3.    $type
                '',                                                                     //4.    $type_id
                'updated',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->session->set_flashdata('update_success_text', $this->lang->line('update_success_text'));
            redirect('settings_module/general_settings');
        }

    }


}