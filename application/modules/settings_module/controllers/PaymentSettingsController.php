<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentSettingsController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->library('form_validation');

        //customized lib from modules/settings_module/libraries
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_log_library');

        $this->lang->load('payment_settings');

    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        } else {
            $a_settings_code = 'payment_settings';
            $data['all_payment_settings'] = $this->custom_settings_library->getSettings($a_settings_code);

            $this->load->view("common_module/header");
            // $this->load->view("common_module/common_left");

            if ($data) {
                $this->load->view("settings_module/payment_settings_page", $data);
            } else {
                $this->load->view("settings_module/payment_settings_page");
            }

            $this->load->view("common_module/footer");
        }
    }


    public function updatePaymentSettings()
    {
        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        $data = array();

        if ($this->input->post('payment_method_check')) {
            $data['payment_method_check'] = trim($this->input->post('payment_method_check'));
        }

        /*paystack <starts>*/
        if ($this->input->post('paystack_test_mode')) {
            $data['paystack_test_mode'] = trim($this->input->post('paystack_test_mode'));
        }

        if ($this->input->post('paystack_test_secret_key')) {
            $data['paystack_test_secret_key'] = trim($this->input->post('paystack_test_secret_key'));
        }

        if ($this->input->post('paystack_test_public_key')) {
            $data['paystack_test_public_key'] = trim($this->input->post('paystack_test_public_key'));
        }


        if ($this->input->post('paystack_live_secret_key')) {
            $data['paystack_live_secret_key'] = trim($this->input->post('paystack_live_secret_key'));
        }

        if ($this->input->post('paystack_live_public_key')) {
            $data['paystack_live_public_key'] = trim($this->input->post('paystack_live_public_key'));
        }
        /*paystack <ends>*/

        /*flutterwave <starts>*/
        if ($this->input->post('flutterwave_environment')) {
            $data['flutterwave_environment'] = trim($this->input->post('flutterwave_environment'));
        }

        if ($this->input->post('flutterwave_test_merchant_key')) {
            $data['flutterwave_test_merchant_key'] = trim($this->input->post('flutterwave_test_merchant_key'));
        }

        if ($this->input->post('flutterwave_test_api_key')) {
            $data['flutterwave_test_api_key'] = trim($this->input->post('flutterwave_test_api_key'));
        }


        if ($this->input->post('flutterwave_live_merchant_key')) {
            $data['flutterwave_live_merchant_key'] = trim($this->input->post('flutterwave_live_merchant_key'));
        }

        if ($this->input->post('flutterwave_live_api_key')) {
            $data['flutterwave_live_api_key'] = trim($this->input->post('flutterwave_live_api_key'));
        }
        /*flutterwave <ends>*/


        /*if ($data == null) {
            $this->session->set_flashdata('noting_to_update', $this->lang->line('noting_to_update_text'));
        }*/

        /*-------------------------set rules starts--------------------------------*/

        /*paystack rules <starts>*/
        if ($this->input->post('paystack_test_mode') == 'on') {

            $this->form_validation->set_rules('paystack_test_secret_key', 'Paystack Test Secret Key', 'required');
        }

        if ($this->input->post('paystack_test_mode') == 'on') {

            $this->form_validation->set_rules('paystack_test_public_key', 'Paystack Test Public Key', 'required');
        }

        if ($this->input->post('paystack_test_mode') == 'off') {

            $this->form_validation->set_rules('paystack_live_secret_key', 'Paystack Live Secret Key', 'required');
        }

        if ($this->input->post('paystack_test_mode') == 'off') {

            $this->form_validation->set_rules('paystack_live_public_key', 'Paystack Live Public Key', 'required');
        }
        /*paystack rules <ends>*/

        /*flutterwave rules <starts>*/
        if ($this->input->post('flutterwave_environment') == 'staging') {

            $this->form_validation->set_rules('flutterwave_test_merchant_key', 'Flutterwave Test Merchant Key', 'required');
        }

        if ($this->input->post('flutterwave_environment') == 'staging') {

            $this->form_validation->set_rules('flutterwave_test_api_key', 'Flutterwave Test API Key', 'required');
        }

        if ($this->input->post('flutterwave_environment') == 'production') {

            $this->form_validation->set_rules('flutterwave_live_merchant_key', 'Flutterwave Live Merchant Key', 'required');
        }

        if ($this->input->post('flutterwave_environment') == 'production') {

            $this->form_validation->set_rules('flutterwave_live_api_key', 'Flutterwave Live API Key', 'required');
        }
        /*flutterwave rules <ends>*/


        /*---------------------set rules ends------------------------------------------*/

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('validation_errors', validation_errors());
            var_dump(validation_errors());
        } else {

            /*
             * if settings already exists, update
             * if not, add.
            */
            $a_settings_code = 'payment_settings';
            foreach ($data as $a_settings_key => $a_settings_value) {

                if (($this->custom_settings_library->ifSettingsExist($a_settings_code, $a_settings_key)) == true) {

                    $this->custom_settings_library->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

                } else {

                    $this->custom_settings_library->addSettings($a_settings_code, $a_settings_key, $a_settings_value);

                }

            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'payment_settings',                                                     //3.    $type
                '',                                                                     //4.    $type_id
                'updated',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('settings_module/payment_settings');

    }


}