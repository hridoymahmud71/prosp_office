<?php
/*page texts*/



/*FAQ Portion*/

$lang['page_title_add_text'] = 'All FAQ';
$lang['page_title_edit_text'] = 'Edit FAQ';

$lang['page_subtitle_text'] = 'FAQ\'s information';
$lang['box_title_text'] = 'FAQ Information Form';

$lang['breadcrumb_home_text'] = 'FAQ Settings';
$lang['breadcrumb_section_text'] = 'FAQ';
$lang['breadcrumb_add_page_text'] = 'All FAQ';

$lang['FAQ_question'] = 'FAQ Question';
$lang['FAQ_ans'] = 'FAQ Answer';
$lang['action'] = 'Action';
$lang['status'] = 'Status';
$lang['FAQ_edit_button'] = 'Edit';
$lang['FAQ_submit_button'] = 'Submit';
$lang['label_FAQ_question'] = 'Question';
$lang['label_FAQ_short_text'] = 'FAQ Answer';

$lang['fac_name_required_text'] = 'FAQ question is required';
$lang['placeholder_FAQ_name_text'] = 'Enter FAQ Question';

$lang['button_submit_create_text'] = 'Create FAQ';
$lang['button_submit_update_text'] = 'Update FAQ';
$lang['faq_create_success'] = 'FAQ Info Insert Successfully';
$lang['faq_update_success'] = 'FAQ Info Update Successfully';
//FAQ Portion end
$lang['delete_success'] = 'Delete Info Successfully';

/*News Portion*/
$lang['all_news'] = 'All News';
$lang['add_news'] = 'Add News';
$lang['edit_news'] = 'Edit News';

$lang['news_subtitle_text'] = 'News\'s information';
$lang['news_title_text'] = 'News Information Form';

$lang['breadcrumb_news_text'] = 'News Settings';
$lang['breadcrumb_news_section_text'] = 'News';
$lang['breadcrumb_add_news_text'] = 'All News';

$lang['news_title'] = 'News Title';
$lang['news_description'] = 'News Description';
$lang['action'] = 'Action';
$lang['news_edit_button'] = 'Edit';
$lang['news_submit_button'] = 'Submit';
$lang['label_news_title'] = 'Title';
$lang['label_news_image'] = 'Image';
$lang['label_news_short_text'] = 'News Description';

$lang['news_name_required_text'] = 'News title is required';
$lang['placeholder_news_title_text'] = 'Enter News Title';

$lang['button_submit_create_news'] = 'Create News';
$lang['button_submit_update_news'] = 'Update News';
$lang['news_create_success'] = 'News Info Insert Successfully';
$lang['news_update_success'] = 'News Info Update Successfully';

/*News Portion*/
$lang['all_pages'] = 'All Pages';
$lang['add_pages'] = 'Add Pages';
$lang['edit_pages'] = 'Edit Pages';

$lang['pages_subtitle_text'] = 'Page\'s information';
$lang['pages_title_text'] = 'Pages Information Form';

$lang['breadcrumb_pages_text'] = 'Pages Settings';
$lang['breadcrumb_pages_section_text'] = 'Pages';
$lang['breadcrumb_add_pages_text'] = 'All Pages';

$lang['pages_title'] = 'Pages Title';
$lang['pages_description'] = 'Pages Description';
$lang['pages_meta_title'] = 'Meta Title';
$lang['action'] = 'Action';
$lang['pages_edit_button'] = 'Edit';
$lang['pages_submit_button'] = 'Submit';
$lang['label_pages_title'] = 'Pages Title';
$lang['label_pages_meta_title'] = 'Meta Title';
$lang['label_pages_meta_description'] = 'Meta Description';
$lang['label_pages_image'] = 'Image';
$lang['label_pages_short_text'] = 'Pages Description';

$lang['pages_name_required_text'] = 'Pages title is required';
$lang['placeholder_pages_title_text'] = 'Enter Pages Title';
$lang['placeholder_pages_meta_title'] = 'Meta Title';
$lang['label_pages_meta_description'] = 'Meta Description';

$lang['button_submit_create_pages'] = 'Create Pages';
$lang['button_submit_update_pages'] = 'Update Pages';
$lang['pages_create_success'] = 'Pages Info Insert Successfully';
$lang['pages_update_success'] = 'Pages Info Update Successfully';