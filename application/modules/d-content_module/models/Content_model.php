<?php

class Content_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllInfo($order_by,$table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($order_by,'DESC');
        
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getInfoById($columnName,$columnVal,$table) {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($columnName,$columnVal);
        
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function insertInfo($table,$data) {
        $this->db->insert($table,$data);
        return TRUE;
    }
    
    public function updateInfo($columnName,$columnVal,$table,$data) {
        $this->db->where($columnName, $columnVal);
        $this->db->update($table, $data);
        
        return TRUE;
    }
    
    public function deleteInfo($columnName,$columnVal,$table) {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($table);
        
        return TRUE;
    }

}