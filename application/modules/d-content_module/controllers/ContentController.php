<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContentController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('content_module/Content_model');

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->lang->load('content_form');
            $data['form_name'] = 'add_faq_form';
            $data['all_faq'] = $this->Content_model->getAllInfo('id','faq');

            $this->load->view("common_module/header");
            $this->load->view("content_module/all_faq", $data);
            $this->load->view("common_module/footer");

        }
    }

    public function addFaq()
    {
        $data['faq_question'] = $this->input->post('faq_question');
        $data['faq_answer'] = $this->input->post('faq_answer');
        $data['is_active'] = 1;

        $this->form_validation ->set_rules('faq_question','Faq Question','required');
        $this->form_validation->set_rules('faq_answer','Faq Answer','required');
        if($data['faq_question'] == '' || $data['faq_answer'] == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t"," "), '', strip_tags($data['faq_answer'])))){
            echo 0;
        }
//        $this->form_validation->set_rules('faq_question', 'faq_question',
//            'required',
//            array(
//                'required' => $this->lang->line('fac_name_required_text')
//            )
//        );
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->session->set_flashdata('faq_question', $this->input->post('faq_question'));
//            redirect('content_module');
//        } 
        else {
            $currency_added = $this->Content_model->insertInfo('faq',$data);
            if ($currency_added == true) {
                $this->session->set_flashdata('faq_create_success', 'faq_create_success');
                redirect('content_module');
            }

        }

    }

    public function editFaq()
    {
        $data['faq_question'] = $this->input->post('faq_question');
        $data['faq_answer'] = $this->input->post('faq_answer');        
        $this->form_validation->set_rules('faq_question', 'faq_question',
            'required',
            array(
                'required' => $this->lang->line('fac_name_required_text')
            )
        );
        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('faq_question', $this->input->post('faq_question'));
            redirect('content_module');
        }
        else
        {
            $currency_updated = $this->Content_model->updateInfo('id',$this->input->post('id'),'faq',$data);
            if ($currency_updated == true)
            {
                $this->session->set_flashdata('faq_update_success', 'faq_update_success');
                redirect('content_module');
            }
        }
    }
    
    public function deleteFaq($id) {
        $currency_updated = $this->Content_model->deleteInfo('id',$id,'faq');

        if ($currency_updated == true) {
            $this->session->set_flashdata('delete_success', 'delete_success');
            redirect('content_module');
        }
    }
    
    public function changeFaqStatus($id) {
        $get_info = $this->Content_model->getInfoById('id',$id,'faq');
        
        if($get_info[0]['is_active'] == 0){
            $data['is_active'] = 1;
            $this->Content_model->updateInfo('id',$id,'faq',$data);
        }
        if($get_info[0]['is_active'] == 1){
            $data['is_active'] = 0;
            $this->Content_model->updateInfo('id',$id,'faq',$data);
        }
        redirect('content_module');
    }

    public function all_news(){
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->lang->load('content_form');
            $data['form_name'] = 'add_news_form';
            $data['all_news'] = $this->Content_model->getAllInfo('id','news');

            $this->load->view("common_module/header");
            $this->load->view("content_module/all_news", $data);
            $this->load->view("common_module/footer");

        }
    }
    
    public function addNews() {
        $data['title'] = $this->input->post('title');
        $data['news_text'] = $this->input->post('news_text');
        $data['is_active'] = 1;
        
        if($data['title']  == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t"," "), '', strip_tags($data['title'])))){
            echo 0;
        }else if( $data['news_text'] == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t"," "), '', strip_tags($data['news_text'])))){
            echo 1;
        }else{
            $new_name = uniqid() . '_newsImage';
            $config['file_name'] = $new_name;
            $config['upload_path'] = $this->config->item('pg_upload_path').'/news/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $error = array();
            $fdata = array();
            if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
                //echo $error;
            } else {
                $fdata = $this->upload->data();
                $data['image'] = '/news/' . $fdata['file_name'];
            }

            $currency_added = $this->Content_model->insertInfo('news', $data);

            if ($currency_added == true) {
                $this->session->set_flashdata('news_create_success', 'news created successfully');
                redirect('content_module/all_news');
            }
        }
        
    }
    
    public function editNews(){
        
        if (empty($_FILES['image']['name'])) {
            $data['image'] = $this->input->post('image2');
        } else {
            $new_name = uniqid() . '_newsImage';
            $config['file_name'] = $new_name;
            $config['upload_path'] = $this->config->item('pg_upload_path').'/news/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $error = array();
            $fdata = array();
            if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
            } else {
                $fdata = $this->upload->data();
               $data['image'] = '/news/' . $fdata['file_name'];
            }
        }
        $data['title'] = $this->input->post('title');
        $data['news_text'] = $this->input->post('news_text');
        
        $currency_updated = $this->Content_model->updateInfo('id',$this->input->post('id'),'news',$data);

        if ($currency_updated == true) {
            $this->session->set_flashdata('news_update_success', 'news_update_success');
            redirect('content_module/all_news');
        }
    }
    
    public function changeNewsStatus($id) {
        $get_info = $this->Content_model->getInfoById('id',$id,'news');
        
        if($get_info[0]['is_active'] == 0){
            $data['is_active'] = 1;
            $this->Content_model->updateInfo('id',$id,'news',$data);
        }
        if($get_info[0]['is_active'] == 1){
            $data['is_active'] = 0;
            $this->Content_model->updateInfo('id',$id,'news',$data);
        }
        redirect('content_module/all_news');
    }
    
    
    public function deleteNews($id) {
        $currency_updated = $this->Content_model->deleteInfo('id',$id,'news');

        if ($currency_updated == true) {
            $this->session->set_flashdata('delete_success', 'delete_success');
            redirect('content_module/all_news');
        }
    }
    
    public function all_pages() {
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->lang->load('content_form');
            $data['form_name'] = 'add_pages_form';
            $data['all_pages'] = $this->Content_model->getAllInfo('id','pg_pages');
            
            $this->load->view("common_module/header");
            $this->load->view("content_module/all_pages", $data);
            $this->load->view("common_module/footer");
        }
    }
    
    public function addpages() {
        
        $data['pages_title'] = $this->input->post('name');
        $data['meta_title'] = $this->input->post('meta_title');
        $data['meta_description'] = $this->input->post('meta_description');
        $data['pages_description'] = $this->input->post('description');
        $data['is_active'] = 1;
        
        if($data['pages_title'] == ''){
            echo 0;
        }else if($data['meta_title'] == ''){
            echo 1;
        }else if (trim($data['pages_description']) == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t"), '', strip_tags($data['pages_description'])))) {
            echo 2;
        }else{
            $currency_added = $this->Content_model->insertInfo('pg_pages', $data);

            if ($currency_added == true) {
                $this->session->set_flashdata('pages_create_success', 'pages_create_success');
                redirect('content_module/all_pages');
            }
        }


        
    }
    
    public function editPages(){
        $data['pages_title'] = $this->input->post('name');
        $data['meta_title'] = $this->input->post('meta_title');
        $data['meta_description'] = $this->input->post('meta_description');
        $data['pages_description'] = $this->input->post('description');
        
        $currency_updated = $this->Content_model->updateInfo('id',$this->input->post('id'),'pg_pages',$data);

        if ($currency_updated == true) {
            $this->session->set_flashdata('pages_update_success', 'pages_update_success');
            redirect('content_module/all_pages');
        }
    }
    
    public function changePagesStatus($id) {
        $get_info = $this->Content_model->getInfoById('id',$id,'pg_pages');
        
        if($get_info[0]['is_active'] == 0){
            $data['is_active'] = 1;
            $this->Content_model->updateInfo('id',$id,'pg_pages',$data);
        }
        if($get_info[0]['is_active'] == 1){
            $data['is_active'] = 0;
            $this->Content_model->updateInfo('id',$id,'pg_pages',$data);
        }
        redirect('content_module/all_pages');
    }


}

