
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php
                    echo lang('add_pages');
                    ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url() . 'content_module/all_pages' ?>"><i
                                class="fa fa-cogs"></i><?php echo lang('breadcrumb_pages_text') ?></a></li>
                    
                    <li class="breadcrumb-item active">
                        <?php
                            echo lang('breadcrumb_add_pages_text');
                        
                        ?>
                    </li>

                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">

                    <?php if ($this->session->flashdata('pages_create_success')) { ?>
                        <br>
                        <div class="panel panel-success copyright-wrap" id="add-success-panel">
                            <div class="panel-body">
                                <?php echo lang('pages_create_success') ?></a>
                            </div>
                        </div>
                    <?php } ?>
                        
                    <?php if ($this->session->flashdata('pages_update_success')) { ?>
                        <br>
                        
                        <div class="panel panel-success copyright-wrap" id="update-success-panel">
                            <div class="panel-body">
                                <?php echo lang('pages_update_success');?>
                            </div>
                        </div>
                       
                    <?php } ?>
                        <?php if ($this->session->flashdata('delete_success')) { ?>
                        <br>
                        
                        <div class="panel panel-success copyright-wrap" id="update-success-panel">
                            <div class="panel-body">
                                <?php echo lang('delete_success');?>
                            </div>
                        </div>
                       
                    <?php } ?>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" id="btnsubmit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-bottom: 10px;">
                                    <?php echo lang('button_submit_create_pages'); ?>
                                </button>
                                <!--Modal Start-->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog" style="max-width: 830px;">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo lang('add_pages');?></h4>
                                            </div>

                                            <div class="modal-body">
                                                <form action="" method="POST" id="pages_form" enctype="multipart/form-data">
                                                    <div class="success_msg" style="padding: 10px;border: 1px solid green;background: green;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    <div class="error_title_msg" style="padding: 10px;border: 1px solid #b90a0a;background: #b90a0a;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    
                                                    
                                                    <div class="form-group">
                                                        <label for="name" class="control-label"><?php echo lang('label_pages_title');?></label>
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="<?php echo lang('placeholder_pages_title_text');?>">
                                                    </div>
                                                    <div class="error_meta_msg" style="padding: 10px;border: 1px solid #b90a0a;background: #b90a0a;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    
                                                    <div class="form-group">
                                                        <label for="name" class="control-label"><?php echo lang('label_pages_meta_title');?></label>
                                                        <input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="<?php echo lang('label_pages_meta_title');?>">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="name" class="control-label"><?php echo lang('label_pages_meta_description');?></label>
                                                        <textarea class="form-control" name="meta_description" id="meta_description" style="resize: none;"></textarea>
                                                    </div>
                                                    <div class="error_description_msg" style="padding: 10px;border: 1px solid #b90a0a;background: #b90a0a;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    <div class="form-group">
                                                        <label for="name" class="control-label"><?php echo lang('label_pages_short_text');?></label>
                                                        <textarea id="message_text" class="form-control" name="description"></textarea>
                                                    </div>

                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                        <a href="<?= base_url('admin/add_department'); ?>">
                                                        </a>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                                <!--Modal End-->

                            <div class="col-md-12">
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <table id="inbox-table" class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('pages_title') ?></th>
                                                <th><?php echo lang('pages_meta_title') ?></th>
                                                <th><?php echo lang('pages_description') ?></th>
                                                <th><?php echo lang('status') ?></th>
                                                <th><?php echo lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($all_pages as $row){?>
                                            <tr>
                                                <td><?php echo $row['pages_title'];?></td>
                                                <td><?php echo $row['meta_title'];?></td>
                                                <td>
                                                    <?php
                                                        if (str_word_count($row['pages_description'], 0) > 15) {
                                                            $words = str_word_count($row['pages_description'], 2);
                                                            $pos = array_keys($words);
                                                            $text = substr($row['pages_description'], 0, $pos[15]) . '...';
                                                            echo $text;
                                                        }else{
                                                            echo $row['pages_description'];
                                                        }
                                                        
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if($row['is_active'] == 1){
                                                        echo 'Active';
                                                    }else{
                                                        echo 'Inactive';
                                                    }?>
                                                </td>
                                                <td>
                                                    <a href="" data-toggle="modal" data-target="#editModal<?php echo $row['id'];?>">
                                                        <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                                    </a>
                                                    <!-- <a href="content_module/changePagesStatus/<?php echo $row['id'];?>">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </a> -->

                                                    <?php if($row['is_active'] == 1){?>
                                                        <a href="content_module/changePagesStatus/<?php echo $row['id'];?>">
                                                            <span class="label label-danger">
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    <?php }else{?>
                                                        <a href="content_module/changePagesStatus/<?php echo $row['id'];?>">
                                                            <span class="label label-success">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    <?php }?>

                                                    
<!--                                                    <a href="content_module/deletepages/<?php echo $row['id'];?>" onclick="return deleteConfirm()">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </a>-->
                                                    
                                                    <!--Edit Modal Start-->
                                                        <div class="modal fade" id="editModal<?php echo $row['id'];?>" role="dialog">
                                                            <div class="modal-dialog" style="max-width: 830px;">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title"><?php echo lang('edit_pages'); ?></h4>
                                                                    </div>

                                                                    <div class="modal-body">
                                                                        <form action="content_module/editPages" method="POST" id="add-basket-form" enctype="multipart/form-data">
                                                                            <input type="hidden" name="id" value="<?php echo $row['id'];?>">
                                                                            <div class="form-group">
                                                                                <label for="question"><?php echo lang('label_pages_title'); ?></label>
                                                                                <input type="text" class="form-control" id="question" name="name" style="padding: .5rem 0px;" value="<?php echo $row['pages_title']; ?>">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="name" class="control-label"><?php echo lang('label_pages_meta_title'); ?></label>
                                                                                <input type="text" class="form-control" style="padding: .5rem 0px;" name="meta_title" value="<?php echo $row['meta_title']; ?>">
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="name" class="control-label"><?php echo lang('label_pages_meta_description'); ?></label>
                                                                                <textarea class="form-control" name="meta_description" style="padding: .5rem 0px;" id="description1" style="resize: none;"><?php echo $row['meta_description']; ?></textarea>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="name" class="control-label"><?php echo lang('label_pages_short_text'); ?></label>
                                                                                <textarea class="form-control message_text" style="padding: .5rem 0px;" name="description"><?php echo $row['pages_description']; ?></textarea>
                                                                            </div>
                                                                            <button type="submit" class="btn btn-default"><?= lang('pages_edit_button') ?></button>
                                                                        </form>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                        <!--Edit Modal End-->
                                                    
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <div class="clearfix"></div>
                </div>
            </div><!-- end col -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div>


<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>


<script>
    $(function () {

        tinymce.init({
            selector: '#message_text',
            height: 250,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });
        tinymce.init({
            selector: '.message_text',
            height: 250,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });

        $('#inbox-table').DataTable({
            
        });

    });


</script>

<script>
    function deleteConfirm(){
        var chk = confirm('Are You Sure to Delete This?');
        if(chk){
            return true;
        }else{
            return false;
        }
    }
</script>

<script>
    $(function(){
        $('#pages_form').submit(function(event){
            event.preventDefault();
            var name = $('#name').val();
            var meta_title = $('#meta_title').val();
            var description = $('#message_text').val();
            var meta_description = $('#meta_description').val();
            $('.error_title_msg').hide();
            $('.error_meta_msg').hide();
            $('.error_description_msg').hide();
            $.ajax({
                    type: 'POST',
                    url: 'content_module/addPages',
                    data: {
                     'name': name,
                     'meta_title': meta_title,
                     'description': description,
                     'meta_description': meta_description,
                    },
                    dataType: 'html',
                    success: function(results){
                        if(results == 0){
                            $('.error_title_msg').show();
                            $('.error_title_msg').html('Title Field Can Not Be Empty');
                            return false;
                        }else if(results == 1){
                            $('.error_meta_msg').show();
                            $('.error_meta_msg').html('Meta Title Field Can Not Be Empty');
                            return false; 
                        }else if(results == 2){
                            $('.error_description_msg').show();
                            $('.error_description_msg').html('Description Field Can Not Be Empty');
                            return false;
                        }else{
                            $('.success_msg').show();
                            $('.success_msg').html('Save Successfully');
                            location.reload();
                        }
                    }
              });


        });

    });
</script>