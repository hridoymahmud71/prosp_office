
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php
                    echo lang('add_news');
                    ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url() . 'content_module/all_news' ?>"><i
                                class="fa fa-cogs"></i><?php echo lang('breadcrumb_news_text') ?></a></li>                    
                    <li class="breadcrumb-item active">
                        <?php
                            echo lang('breadcrumb_add_news_text');                        
                        ?>
                    </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">
                    <?php if ($this->session->flashdata('news_create_success')) { ?>
                    <div class="text-center alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?php echo lang('news_create_success') ?></strong>
                    </div>
                    <?php } ?>
                        
                    <?php if ($this->session->flashdata('news_update_success')) { ?>
                    <div class="text-center alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?php echo lang('news_update_success') ?></strong>
                    </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('delete_success')) { ?>
                    <div class="text-center alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong><?php echo lang('delete_success') ?></strong>
                    </div>
                    <?php } ?>

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" id="btnsubmit" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="margin-bottom: 10px;">
                                    <?php echo lang('button_submit_create_news'); ?>
                                </button>
                                <!--Modal Start-->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog" style="max-width: 830px;">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><?php echo lang('add_news');?></h4>
                                            </div>

                                            <div class="modal-body">
                                                <form action="" method="POST" id="add-basket-form" enctype="multipart/form-data">
                                                    
                                                    <div class="success_msg" style="padding: 10px;border: 1px solid green;background: green;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    <div class="error_title_msg" style="padding: 10px;border: 1px solid #b90a0a;background: #b90a0a;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    
                                                    <div class="form-group">
                                                        <label for="question"><?php echo lang('label_news_title');?></label>
                                                        <input type="text" class="form-control" id="title" name="title" placeholder="<?php echo lang('placeholder_news_title_text');?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label><?php echo lang('label_news_image');?></label>
                                                        <input type="file" class="dropify" data-height="200" name="image"/>
                                                    </div>
                                                    
                                                    <div class="error_description_msg" style="padding: 10px;border: 1px solid #b90a0a;background: #b90a0a;color: #fff;font-weight: 700;border-radius: 5px;display: none"></div>
                                                    <div class="form-group">
                                                        <label for=""><?= lang('label_news_short_text') ?></label>
                                                        <textarea id="message_text" class="form-control" name="news_text" rows="3"></textarea>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-success"><?= lang('news_submit_button') ?></button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                                <!--Modal End-->

                            <div class="col-md-12">
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <table id="inbox-table" class="table table-bordered table-responsive">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('news_title') ?></th>
                                                <th><?php echo lang('news_description') ?></th>
                                                <th><?php echo lang('status') ?></th>
                                                <th><?php echo lang('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($all_news as $row){?>
                                            <tr>
                                                <td><?php echo $row['title'];?></td>
                                                <td>
                                                    <?php
                                                        if (str_word_count($row['news_text'], 0) > 30) {
                                                            $words = str_word_count($row['news_text'], 2);
                                                            $pos = array_keys($words);
                                                            $text = substr($row['news_text'], 0, $pos[30]) . '...';
                                                            echo $text;
                                                        }else{
                                                            echo $row['news_text'];
                                                        }
                                                        
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php if($row['is_active'] == 1){
                                                        echo 'Active';
                                                    }else{
                                                        echo 'Inactive';
                                                    }?>
                                                </td>
                                                <td>
                                                    <a href="" data-toggle="modal" data-target="#editModal<?php echo $row['id'];?>">
                                                        <i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                                    </a>
<!--                                                    <a href="content_module/deleteNews/<?php echo $row['id'];?>" onclick="return deleteConfirm()">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </a>-->
                                                    <!-- <a href="content_module/changeNewsStatus/<?php echo $row['id'];?>">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </a> -->
                                                    <?php if($row['is_active'] == 1){?>
                                                        <a href="content_module/changeNewsStatus/<?php echo $row['id'];?>">
                                                            <span class="label label-danger">
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    <?php }else{?>
                                                        <a href="content_module/changeNewsStatus/<?php echo $row['id'];?>">
                                                            <span class="label label-success">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    <?php }?>
                                                    
                                                    <!--Edit Modal Start-->
                                                        <div class="modal fade" id="editModal<?php echo $row['id'];?>" role="dialog">
                                                            <div class="modal-dialog" style="max-width: 830px;">

                                                                <!-- Modal content-->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title"><?php echo lang('edit_news'); ?></h4>
                                                                    </div>

                                                                    <div class="modal-body">
                                                                        <form action="content_module/editNews" method="POST" id="add-basket-form" enctype="multipart/form-data">
                                                                            <input type="hidden" name="id" value="<?php echo  $row['id'];?>">
                                                                            <div class="form-group">
                                                                                <label for="question"><?php echo lang('label_news_title'); ?></label>
                                                                                <input type="text" class="form-control" id="question" name="title" style="padding: .5rem 0px;" value="<?php echo $row['title']; ?>">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('label_news_image'); ?></h4>
                                                                                <input type="file" class="dropify" data-height="200" name="image" data-default-file="<?php echo $this->config->item('pg_upload_source_path'). $row['image']; ?>"/>
                                                                                <input type="hidden" name="image2" value="<?php echo $row['image']; ?>">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for=""><?= lang('label_news_short_text') ?></label>
                                                                                <textarea class="message_text" class="form-control" name="news_text" rows="3"><?php echo $row['news_text']; ?></textarea>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-default"><?=lang('news_edit_button')?></button>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                        <!--Edit Modal End-->
                                                    
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <div class="clearfix"></div>
                </div>
            </div><!-- end col -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div>


<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
    
    .dropify-wrapper{
        padding: 5px 0px;
    }
</style>


<script>
    $(function () {

        tinymce.init({
            selector: '#message_text',
            height: 250,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });
        tinymce.init({
            selector: '.message_text',
            height: 250,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });

        $('#inbox-table').DataTable({
            
        });

    });


</script>

<script>
    function deleteConfirm(){
        var chk = confirm('Are You Sure to Delete This?');
        if(chk){
            return true;
        }else{
            return false;
        }
    }
</script>

<script>
    $(function(){
        $('#add-basket-form').submit(function(event){
            event.preventDefault();
            var title = $('#title').val();
            var message_text = $('#message_text').val();
            $('.error_title_msg').hide();
            $('.error_description_msg').hide();
            $.ajax({
                type: 'POST',
                url: 'content_module/addNews',
                data: {
                 'title': title,
                 'news_text': message_text
                },
                dataType: 'html',
                success: function(results){
                    if(results == 0){
                        $('.error_title_msg').show();
                        $('.error_title_msg').html('Title Field Can Not Be Empty');
                        return false;
                    }else if(results == 1){
                        $('.error_description_msg').show();
                        $('.error_description_msg').html('Description Field Can Not Be Empty');
                        return false;
                    }
                    else{
                        $('.success_msg').show();
                        $('.success_msg').html('Save Successfully');
                        location.reload(); 
                    }
                }
            });
        });



       $('.dropify').dropify({
           messages: {
               'default': 'Drag and drop a file here or click',
               'replace': 'Drag and drop or click to replace',
               'remove': 'Remove',
               'error': 'Ooops, something wrong appended.'
           },
           error: {
               'fileSize': 'The file size is too big (1M max).'
           }
       });


    });
</script>