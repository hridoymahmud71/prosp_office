<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                            <!-- <small><?php echo lang('page_subtitle_text') ?></small> -->
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">
                                <small><?php echo lang('page_subtitle_text') ?></small>
                            </h4>
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- Default box -->
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo lang('admin_box_title_text') ?></h3>
                                            </div>
                                            <div class="box-body">
                                                <table class="table table-bordered -table-striped table-responsive">
                                                    <tr>
                                                        <td><?php echo lang('system_admin_contact_email_text') ?>
                                                            <span class="pull-right"><i class="fa fa-envelope"></i></span>
                                                        </td>

                                                        <td><?php echo $admin_contact_email ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo lang('system_admin_contact_phone_text') ?>
                                                           <span class="pull-right"><i class="fa fa-phone"></i></span>
                                                        </td>

                                                        <td><?php echo $admin_contact_phone ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- Default box -->
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title"><?php echo lang('company_box_title_text') ?></h3>
                                            </div>
                                            <div class="box-body">
                                                <table class="table table-bordered -table-striped table-responsive">
                                                    <tr>
                                                        <td><?php echo lang('company_contact_email_text') ?>
                                                            <span class="pull-right"><i class="fa fa-envelope"></i></span>
                                                        </td>
                                                        <td><?php echo $company_contact_email ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo lang('company_contact_phone_text') ?>
                                                            <span class="pull-right"><i class="fa fa-phone"></i></span>

                                                        </td>
                                                        <td><?php echo $company_contact_phone ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo lang('company_contact_address_text') ?>
                                                            <span class="pull-right"><i class="fa fa-building"></i></span>
                                                        </td>
                                                        <td><?php echo $company_contact_address ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>

                                                    <tr>
                                                        <td><?php echo lang('company_facebook_id_text') ?>
                                                            <span class="pull-right"><i class="fa fa-facebook"></i></span>
                                                        </td>
                                                        <td><?php echo $company_facebook_id ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo lang('company_twitter_id_text') ?>
                                                            <span class="pull-right"><i class="fa fa-twitter"></i></span>
                                                        </td>
                                                        <td><?php echo $company_twitter_id ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?php echo lang('company_youtube_text') ?>
                                                            <span class="pull-right"><i class="fa fa-youtube-play"></i></span>
                                                        </td>
                                                        <td><?php echo $company_youtube_id ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>


                            </section>
                            <!-- /.content -->


                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
</div>