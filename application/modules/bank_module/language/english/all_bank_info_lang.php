<?php

/*page texts*/
$lang['page_title_text'] = 'Bank Information';
$lang['breadcrum_home_text'] = 'Bank';
$lang['breadcrumb_page_add_text'] = 'All Bank';
$lang['page_form_title_text'] = 'All Bank Information';

// BANK COLUMN


$lang['column_bank_name_text'] = 'Bank Name';
$lang['column_address_text'] = 'Address';
$lang['column_website_text'] = 'Website';
$lang['column_email_text'] = 'Email';
$lang['column_phone_text'] = 'Phone';
$lang['column_status_text'] = 'Status';
$lang['column_actions_text'] = 'Action';

//modal paystack banks
$lang['modal_paystack_trigger_btn_text'] = 'Paystack Banks With Code';
$lang['modal_paystack_title_text'] = 'Paystack Banks With Code';
$lang['modal_column_paystsack_bank_text'] = 'Paystack Bank';
$lang['modal_column_paystsack_bank_code_text'] = 'Code';
$lang['modal_column_active_text'] = 'Active';
$lang['modal_column_actions_text'] = 'Deleted';
$lang['modal_yes_text'] = 'Yes';
$lang['modal_no_text'] = 'No';
$lang['paystack_bank_unavailable_text'] = 'No Paystack Banks Are Found';


// all bank page

$lang['status_success_change_text'] = 'Status Successfully Changed';
$lang['data_update_success_change_text'] = 'Bank information Successfully Updated';
$lang['bank_delete_text'] = 'Bank Information Successfully Removed';

$lang['swal_title'] = 'Are you sure to delete this Bank Information ?';



?>