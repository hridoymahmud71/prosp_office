
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                            <!-- <small><?php echo lang('page_subtitle_add_text')?></small> -->
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php if($this->session->flashdata('bank_detail_insert')){?>
            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong><?php echo lang('success_bank_upload_text')?></strong>
            </div>
            <?php }?>

            <?php if($this->session->flashdata('bank_detail_err')){?>
            <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong><?php echo lang('bank_upload_err_text') ?></strong>
            </div>
            <?php }?>
            <div class="row">
                <div class="col-8">
                    <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30"><?php echo lang('page_form_title_text') ?>
                                <!-- trigger Paystack Bank modal -->
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    <?= lang('modal_paystack_trigger_btn_text')?>
                                </button>
                            </h4>

                        <!-- Paystack Bank Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"><?= lang('modal_paystack_title_text')?></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="p-20">
                                            <?php if($paystack_bank_list) { $i= 0 ; ?>
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th><?= lang('modal_column_paystsack_bank_text')?></th>
                                                        <th><?= lang('modal_column_paystsack_bank_code_text')?></th>
                                                        <th><?= lang('modal_column_active_text')?></th>
                                                        <th><?= lang('modal_column_actions_text')?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($paystack_bank_list as $a_paystack_bank) { $i++; ?>
                                                        <tr>
                                                            <th scope="row"><?= $i?></th>
                                                            <td><?= $a_paystack_bank->name?></td>
                                                            <td><?= $a_paystack_bank->code?></td>
                                                            <td><?= $a_paystack_bank->active==1?lang('modal_yes_text'):lang('modal_no_text') ?></td>
                                                            <td><?= $a_paystack_bank->is_deleted?lang('modal_yes_text'):lang('modal_no_text') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            <?php } else { ?>
                                                <?= lang('paystack_bank_unavailable_text')?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">

                                <form role="form" action="bank_module/insert_bank_info" method="post" data-parsley-validate novalidate>
                                    <div class="form-group row">
                                        <label for="bank_name" class="col-sm-4 form-control-label"><?php echo lang('bank_name_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="bank_name" class="form-control" name="bank_name" 
                                                   id="bank_name" placeholder="<?php echo lang('bank_name_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="paystack_bank_code" class="col-sm-4 form-control-label"><?php echo lang('paystack_bank_code_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="bank_name" class="form-control" name="paystack_bank_code"
                                                   id="paystack_bank_code" placeholder="<?php echo lang('paystack_bank_code_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_email" class="col-sm-4 form-control-label"><?php echo lang('bank_email_text');?></label>
                                        <div class="col-sm-7">
                                            <input type="bank_email"  parsley-type="bank_email" class="form-control" name="bank_email"
                                                   id="bank_email" placeholder="<?php echo lang('bank_email_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_website" class="col-sm-4 form-control-label"><?php echo lang('bank_website_text');?></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="bank_website"
                                                   id="bank_website" placeholder="<?php echo lang('bank_website_text');?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_phone" class="col-sm-4 form-control-label"><?php echo lang('bank_phone_text');?></label>
                                        <div class="col-sm-7">
                                            <input type="text"  parsley-type="bank_phone" class="form-control" name="bank_phone"
                                                   id="bank_phone" placeholder="<?php echo lang('bank_phone_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_address" class="col-sm-4 form-control-label"><?php echo lang('bank_address_text');?></label>
                                        <div class="col-sm-7">
                                            <input type="text"  parsley-type="bank_address" class="form-control" name="bank_address"
                                                   id="bank_address" placeholder="<?php echo lang('bank_address_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>

                                    <div class="form-group text-center m-b-0">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                            <?php echo lang('file_submit_text');?>
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            <?php echo lang('modal_cancel_text');?>
                                        </button>
                                    </div>
                                </form>
                            </div><!-- end row -->
                        </div>
                    </div><!-- end col -->
                </div>
            </div>
        </div>
