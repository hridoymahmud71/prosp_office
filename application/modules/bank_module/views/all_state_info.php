        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrumb_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="float-left col-lg-6 col-sm-6 col-xs-6 col-md-6 col-xl-6">
                        <div class="page-title-box">
                        
                        <?php if($this->session->flashdata('state_inserted')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('success_state_insert_text')?></strong>
                            </div>
                        <?php }?>                        
                        <?php if($this->session->flashdata('state_insert_err')){?>
                            <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('error_state_insert_text')?></strong>
                            </div>
                        <?php }?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="page-title float-left">
                                        <?php echo lang('page_subtitle_text') ?>
                                    </h4>
                                </div>
                            </div>
                            
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <!-- general form elements -->
                                        <div class="box box-primary">
                                            <!-- form start -->
                                            <form action="bank_module/insert_state_info" role="form" id="" method="post" enctype="multipart/form-data">
                                                <div class="form-group row">
                                                    <label for="state_name" class="col-sm-4 form-control-label"><?php echo lang('label_state_name_text');?><span class="text-danger">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" required parsley-type="state_name" class="form-control" name="state_name"
                                                               id="state_name" placeholder="<?php echo lang('label_state_name_text') ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="state_capital_name" class="col-sm-4 form-control-label"><?php echo lang('label_state_branch_name_text');?></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" parsley-type="state_capital_name" class="form-control" name="state_capital_name" id="state_capital_name" placeholder="<?php echo lang('label_state_branch_name_text')?>">
                                                    </div>
                                                </div>
                                                <div class="form-group text-center m-b-0">
                                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                        <?php echo lang('input_submit_text');?>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="float-right col-lg-6 col-sm-6 col-xs-6 col-md-6 col-xl-6">
                        <div class="page-title-box">

                        
                        <?php if($this->session->flashdata('state_status_change')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('status_success_change_text')?></strong>
                            </div>
                        <?php }?>
                        <?php if($this->session->flashdata('delete_state')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('state_delete_text')?></strong>
                            </div>
                        <?php }?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <h4 class="page-title float-left">
                                        <?php echo lang('page_all_subtitle_text') ?>
                                    </h4>
                                </div>
                            </div>
                            <!-- Main content -->
                            <section class="content">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <!-- general form elements -->
                                        <div class="box box-primary">
                                            <!-- form start -->
                                            <table id="datatable" class="table table-striped table-bordered table-hover table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo lang('page_state_name_text') ?></th>
                                                        <th><?php echo lang('page_state_capital_text') ?></th>
                                                        <th><?php echo lang('page_state_status_text') ?></th>
                                                        <th><?php echo lang('page_state_action_text') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($state_info as $row){?>
                                                    <tr>
                                                        <td><?=$row->state_name;?></td>
                                                        <td><?=$row->state_capital_name;?></td>
                                                        <td>
                                                            <?php if($row->state_status==1) { ?>
                                                            <span class="label label-primary"><?php echo "Active" ?></span>
                                                                &nbsp;
                                                                <a title="Deactivate" href="bank_module/state_status_deactive/<?=$row->state_id;?>">
                                                                    <span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                                </a>
                                                            <?php } else {?>
                                                             <span class="label label-primary"><?php echo "Inactive" ?></span>
                                                                    &nbsp;
                                                                    <a title="Active" href="bank_module/state_status_active/<?=$row->state_id;?>">
                                                                        <span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>
                                                                    </a>
                                                            <?php }?>
                                                        </td>
                                                        <td>
                                                            <!-- &nbsp;
                                                            <a title="<?php echo lang('tooltip_edit_text') ?>" style="color: #2b2b2b" href="bank_module/edit_bank_info/<?=$row->state_id;?>"
                                                               class=""><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>
                                                            </a> -->
                                                            &nbsp;
                                                            <a title="<?php echo lang('tooltip_delete_text') ?>" id="" style="color: #2b2b2b" href="bank_module/delete_state_info/<?=$row->state_id;?>"
                                                               class="confirmation">
                                                                <i id="remove" class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            // scrollablex:true;
        });
    });
</script>

<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>