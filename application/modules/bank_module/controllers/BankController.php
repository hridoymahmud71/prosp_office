<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BankController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('');
        }
        //$this->load->model('Contact_model');
        $this->load->model('user/Ion_auth_model');
        $this->load->model('Bank_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');
        $this->load->library('payment_module/custom_payment_library');
        // require_once(FCPATH.'external_libraries/excel_reader.php');
    }

    public function all_bank_info()
    {
        $this->lang->load('all_bank_info');

        $data['all_bank'] = $this->Bank_model->get_all_bank_list();
        $data['paystack_bank_list'] = $this->getPaystackBankList();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("all_bank_view_page", $data);
        $this->load->view("common_module/footer");
    }

    public function add_bank_info()
    {
        $this->lang->load('add_bank_info');

        $data['paystack_bank_list'] = $this->getPaystackBankList();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("add_bank_page", $data);
        $this->load->view("common_module/footer");
    }

    public function insert_bank_info()
    {
        $data['bank_name'] = trim($this->input->post('bank_name'));
        $data['paystack_bank_code'] = trim($this->input->post('paystack_bank_code'));
        $data['bank_address'] = trim($this->input->post('bank_address'));
        $data['bank_website'] = trim($this->input->post('bank_website'));
        $data['bank_email'] = trim($this->input->post('bank_email'));
        $data['bank_phone'] = trim($this->input->post('bank_phone'));

        $this->form_validation->set_rules('bank_name', 'First name', 'required');
        $this->form_validation->set_rules('paystack_bank_code', 'Paystack Bank Code', 'required');
        /*$this->form_validation->set_rules('bank_address', 'Address', 'required');
        $this->form_validation->set_rules('bank_email', 'Email', 'required');
        $this->form_validation->set_rules('bank_phone', 'Phone', 'required');*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('bank_detail_err', 'employer Name required');
            redirect('bank_module/add_bank_info');
        }

        $this->Bank_model->insert_bank_detail($data);
        $this->session->set_flashdata('bank_detail_insert', 'Bank information Successfully Inserted');
        redirect('bank_module/add_bank_info');

    }

    public function edit_bank_info()
    {
        $this->lang->load('edit_bank_info');

        $data['paystack_bank_list'] = $this->getPaystackBankList();

        $bank_id = $this->uri->segment(3);
        $data['bank_info'] = $this->Bank_model->get_bank_info($bank_id);

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("edit_bank_page", $data);
        $this->load->view("common_module/footer");

    }

    public function update_bank_info()
    {
        $bank_id = $this->input->post('bank_id');

        $data['bank_name'] = trim($this->input->post('bank_name'));
        $data['paystack_bank_code'] = trim($this->input->post('paystack_bank_code'));
        $data['bank_address'] = trim($this->input->post('bank_address'));
        $data['bank_website'] = trim($this->input->post('bank_website'));
        $data['bank_email'] = trim($this->input->post('bank_email'));
        $data['bank_phone'] = trim($this->input->post('bank_phone'));

        $this->form_validation->set_rules('bank_name', 'First name', 'required');
        $this->form_validation->set_rules('paystack_bank_code', 'Paystack Bank Code', 'required');
        /*$this->form_validation->set_rules('bank_address', 'Address', 'required');
      $this->form_validation->set_rules('bank_email', 'Email', 'required');
      $this->form_validation->set_rules('bank_phone', 'Phone', 'required');*/

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('bank_detail_err', 'employer Name required');
            redirect('bank_module/edit_bank_info/' . $bank_id);
        }

        $this->Bank_model->update_bank_detail($data, $bank_id);
        $this->session->set_flashdata('bank_detail_update', 'Bank information Successfully Updated');
        redirect('bank_module/all_bank_info');
    }

    public function delete_bank_info()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_deletion_status'] = 1;
        $this->Bank_model->update_bank_status($data, $bank_id);
        $this->session->set_flashdata('delete_bank', 'Bank Information Successfully Removed');
        redirect('bank_module/all_bank_info');
    }

    public function all_state_info()
    {
        $this->lang->load('all_state_info');

        $data['state_info'] = $this->Bank_model->get_state_info();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("all_state_info", $data);
        $this->load->view("common_module/footer");
    }

    public function state_status_active()
    {
        $state_id = $this->uri->segment(3);
        $data['state_status'] = 1;
        $this->Bank_model->update_state_status($data, $state_id);
        $this->session->set_flashdata('state_status_change', 'Status Successfully Changed');
        redirect('bank_module/all_state_info');
    }

    public function state_status_deactive()
    {
        $state_id = $this->uri->segment(3);
        $data['state_status'] = 0;
        $this->Bank_model->update_state_status($data, $state_id);
        $this->session->set_flashdata('state_status_change', 'Status Successfully Changed');
        redirect('bank_module/all_state_info');
    }

    public function insert_state_info()
    {
        $data['state_name'] = $this->input->post('state_name');
        $data['state_capital_name'] = $this->input->post('state_capital_name');

        $this->form_validation->set_rules('state_name', 'State name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('state_insert_err', 'State Name required');
            redirect('bank_module/all_state_info');
        }

        $this->Bank_model->insert_state_name($data);
        $this->session->set_flashdata('state_inserted', 'State Successfully Inserted');
        redirect('bank_module/all_state_info');
    }

    public function update_state_info()
    {

    }

    public function delete_state_info()
    {
        $state_id = $this->uri->segment(3);
        $data['state_deletion_status'] = 1;
        $this->Bank_model->update_state_status($data, $state_id);
        $this->session->set_flashdata('delete_state', 'State Information Successfully Removed');
        redirect('bank_module/all_state_info');
    }

    public function bank_status_active()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_active_status'] = 1;
        $this->Bank_model->update_bank_status($data, $bank_id);
        $this->session->set_flashdata('status_change', 'Status Successfully Changed');
        redirect('bank_module/all_bank_info');
    }

    public function bank_status_deactive()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_active_status'] = 0;
        $this->Bank_model->update_bank_status($data, $bank_id);
        $this->session->set_flashdata('status_change', 'Status Successfully Changed');
        redirect('bank_module/all_bank_info');
    }

    public function getPaystackBankList()
    {
        $return = false;

        $ret_data = $this->custom_payment_library->getPaystackBankList();

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {

            if ($ret_data['got_data']->data != null) {
                $return = $ret_data['got_data']->data;
            }

        } else {

            // no need to show error comment the texts below

            /*echo '<pre>';
            if ($ret_data['error_response_object']) {
                print_r($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                echo $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                echo $ret_data['error_message'];
            }
            echo '</pre>';  */

        }

        return $return;

    }

    //Do not use these  only for dev purpose
    /*public function ipbib19931215()
    {
        $this->insertPaystackBanksInBatch();
    }*/

    /*private function insertPaystackBanksInBatch()
    {
        $paystack_banks = $this->getPaystackBankList();

        if ($paystack_banks) {

            $ins_data = array();

            foreach ($paystack_banks as $paystack_bank) {
                $ins_bank['bank_name'] = $paystack_bank->name;
                $ins_bank['paystack_bank_code'] = $paystack_bank->code;
                $ins_bank['bank_website'] = '';
                $ins_bank['bank_email'] = '';
                $ins_bank['bank_phone'] = '';
                $ins_bank['bank_active_status'] = $paystack_bank->active == 1 ? $paystack_bank->active : 0;
                $ins_bank['bank_deletion_status'] = $paystack_bank->is_deleted == 1 ? $paystack_bank->is_deleted : 0;

                $ins_data[] = $ins_bank;
            }

            if(!empty($ins_data)){
                $this->db->insert_batch('pg_bank', $ins_data);
            }


        }

    }*/


}