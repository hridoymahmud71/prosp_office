<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['bank_module/all_bank_info'] = 'BankController/all_bank_info';
$route['bank_module/add_bank_info'] = 'BankController/add_bank_info';
$route['bank_module/insert_bank_info'] = 'BankController/insert_bank_info';
$route['bank_module/edit_bank_info/(:any)'] = 'BankController/edit_bank_info';
$route['bank_module/update_bank_info'] = 'BankController/update_bank_info';
$route['bank_module/delete_bank_info/(:any)'] = 'BankController/delete_bank_info';

$route['bank_module/all_state_info'] = 'BankController/all_state_info';
$route['bank_module/add_state_info'] = 'BankController/add_state_info';
$route['bank_module/update_states_info'] = 'BankController/update_states_info';
$route['bank_module/insert_state_info'] = 'BankController/insert_state_info';
$route['bank_module/delete_state_info/(:any)'] = 'BankController/delete_state_info';

$route['bank_module/bank_status_active/(:any)'] = 'BankController/bank_status_active';
$route['bank_module/bank_status_deactive/(:any)'] = 'BankController/bank_status_deactive';

$route['bank_module/state_status_active/(:any)'] = 'BankController/state_status_active';
$route['bank_module/state_status_deactive/(:any)'] = 'BankController/state_status_deactive';


