<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bank_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function get_all_bank_list()
    {
        $this->db->select('*');
        $this->db->from('pg_bank');
        $this->db->where('bank_deletion_status=',0);

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    public function update_bank_status($data,$bank_id)
    {
        $this->db->where('bank_id',$bank_id);
        $this->db->update('pg_bank',$data);
    }

    public function insert_bank_detail($data)
    {
        $this->db->insert('pg_bank',$data);
    }

    public function get_bank_info($bank_id)
    {
        $this->db->select('*');
        $this->db->from('pg_bank');
        $this->db->where('bank_id',$bank_id);

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    public function update_bank_detail($data, $bank_id)
    {
        $this->db->where('bank_id',$bank_id);
        $this->db->update('pg_bank',$data);
    }

    public function get_state_info()
    {
        $this->db->select('*');
        $this->db->from('pg_state');
        $this->db->where('state_deletion_status',0);

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    public function insert_state_name($data)
    {
        $this->db->insert('pg_state',$data);
    }

    public function update_state_status($data,$state_id)
    {
        $this->db->where('state_id',$state_id);
        $this->db->update('pg_state',$data);
    }

    // public function check_email_id($email)
    // {
    //     $this->db->select('email');
    //     $this->db->from('users');
    //     $this->db->where('email',$email);

    //     $query = $this->db->get();
    //     $num_rows = $query->num_rows();
    //     return $num_rows;
    // }

    // public function insert_employee_credential($data1)
    // {
    //     $this->db->insert('users', $data1);
    //     return $this->db->insert_id();
    // }

    // public function insert_employee_optional_credential($data2)
    // {
    //     $this->db->insert('rspm_tbl_user_details', $data2);
    // }

    // public function update_employee_credential($data1,$user_id)
    // {
    //     $this->db->where('id',$user_id);
    //     $this->db->update('users',$data1);
    // }

    // public function update_employee_optional_credential($data2,$user_id)
    // {
    //     $this->db->where('user_id',$user_id);
    //     $this->db->update('rspm_tbl_user_details',$data2);
    // }

    // public function insert_as_group_member($grp_mem)
    // {
    //     $this->db->insert('users_groups',$grp_mem);
    // }

    // public function insert_as_employee($grp_employee)
    // {
    //     $this->db->insert('users_groups',$grp_employee);
    // }

    // public function get_employee_info($user_id)
    // {
    //     $this->db->select('*');
    //     $this->db->from('users');
    //     $this->db->join('rspm_tbl_user_details','users.id=rspm_tbl_user_details.user_id');
    //     $this->db->where('users.id',$user_id);

    //     $query = $this->db->get();
    //     $queryresult = $query->result();
    //     return $queryresult;
    // }

    // public function activate_product($product_id)
    // {
    // 	$this->db->set('product_is_active', 1);
    // 	$this->db->where('product_id',$product_id);
    // 	$this->db->update('pg_product');
    // }

    // public function deactivate_product($product_id)
    // {
    // 	$this->db->set('product_is_active', 0);
    // 	$this->db->where('product_id',$product_id);
    // 	$this->db->update('pg_product');
    // }

    // public function get_all_product()
    // {
    // 	$this->db->select('*');
    // 	$this->db->from('pg_product');
    // 	$this->db->where('product_deletion_status',0);
    	
    // 	$query = $this->db->get();
    //     $result = $query->result();
    //     return $result;
    // }

    // public function get_product_data($product_id)
    // {
    // 	$this->db->select('*');
    // 	$this->db->from('pg_product');
    // 	$this->db->where('product_id',$product_id);
    	
    // 	$query = $this->db->get();
    //     $result = $query->result();
    //     return $result;
    // }

    // public function update_product_data($data,$product_id)
    // {
    // 	$this->db->where('product_id',$product_id);
    // 	$this->db->update('pg_product',$data);
    // }

    // public function delete_product($product_id)
    // {
    // 	$this->db->set('product_deletion_status',1);
    // 	$this->db->where('product_id',$product_id);
    // 	$this->db->update('pg_product');
    // }

    // public function get_selected_search_data($fname,$lname,$email,$employer,$status_chk)
    // {
    //     $this->db->select('users.id, users.first_name, users.last_name, users.email, users.active, users.created_on, rspm_tbl_user_details.user_employer_id, rspm_tbl_user_details.thrifter_id_num');
    //     $this->db->from('users');
    //     $this->db->join('rspm_tbl_user_details','users.id=rspm_tbl_user_details.user_id');
    //     $this->db->join('users_groups','users.id=users_groups.user_id');
    //     if($employer!='')
    //     {
    //         $this->db->where('rspm_tbl_user_details.user_employer_id',$employer);
    //     }
    //     $this->db->where('users_groups.group_id',7);
    //     if($fname!='')
    //     {
    //         $this->db->like('users.first_name',$fname);
    //     }
    //     if($lname!='')
    //     {
    //         $this->db->like('users.last_name',$lname);
    //     }
    //     if($email!='')
    //     {
    //         $this->db->like('users.email',$email);
    //     }
    //     if($status_chk!='')
    //     {
    //         $this->db->where('users.active',$status_chk);
    //     }
    //     $query = $this->db->get();
    //     $result = $query->result_array();
    //     return $result;
    // }

    // public function get_employer_name($user_employer_id)
    // {
    //     $this->db->select('users.first_name');
    //     $this->db->from('users');
    //     $this->db->join('rspm_tbl_user_details','rspm_tbl_user_details.user_id=users.id');
    //     $this->db->where('rspm_tbl_user_details.user_employer_id',$user_employer_id);

    //     $query = $this->db->get();
    //     $result = $query->result_array();
    //     return $result;
    // }

    // public function get_employer_info()
    // {
    //     $this->db->select('users.id, users.first_name, users.last_name, users.email');
    //     $this->db->from('users');
    //     $this->db->join('users_groups','users.id=users_groups.user_id');
    //     $this->db->where('users_groups.group_id',6);

    //     $query = $this->db->get();
    //     $result = $query->result();
    //     return $result;
    // }

    // public function get_employer_data($employee_id)
    // {
    //     $query ='SELECT * FROM rspm_tbl_user_details as ud WHERE ud.user_id='.$employee_id;

    //     $this->db->select('*');
    //     $this->db->from('users as u');
    //     $this->db->where('u.id',$query);


    //     $query = $this->db->get();
    //     $queryresult = $query->result();
    //     return $queryresult;
    // }

    // public function delete_employer_id($user_id)
    // {
    //     $this->db->set('user_employer_id','0');
    //     $this->db->where('user_id',$user_id);
    //     $this->db->update('rspm_tbl_user_details');
    // }

    // public function chk_thrift_unique_id($chk_thrift_id)
    // {
    //     $this->db->select('thrifter_id_num');
    //     $this->db->from('rspm_tbl_user_details');
    //     $this->db->where('thrifter_id_num',$chk_thrift_id);

    //     $query = $this->db->get();
    //     $result = $query->num_rows();
    //     return $result;
    // }

    // public function check_employer_email($email)
    // {
    //     $this->db->select('email');
    //     $this->db->from('users');
    //     $this->db->where('email',$email);

    //     $query = $this->db->get();
    //     $result = $query->num_rows();
    //     return $result;
    // }

    // public function insert_employer_credential($data1)
    // {
    //     $this->db->insert('users', $data1);
    //     return $this->db->insert_id();
    // }

    // public function insert_employer_optional_credential($data2)
    // {
    //     $this->db->insert('rspm_tbl_user_details', $data2);
    // }

    // public function insert_as_employer($grp_employer)
    // {
    //     $this->db->insert('users_groups',$grp_employer);
    // }


}