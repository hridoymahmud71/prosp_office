<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function insertALog($data)
    {
        $this->db->insert('rspm_tbl_log',$data);
    }

    public function getLogs($user_id,$limit,$offset)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_log as lg');
        $this->db->where('lg.log_created_by', $user_id);
        $this->db->or_where('lg.log_created_for',$user_id );
        $this->db->or_where('lg.log_created_for!=',$user_id );
        $this->db->order_by('log_created_at','desc');

        if(!$offset){
            $this->db->limit($limit);
        }else{
            $this->db->limit($limit,$offset);
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function countTotalLogs($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_log as lg');
        $this->db->where('lg.log_created_by', $user_id);
        $this->db->or_where('lg.log_created_for',$user_id );
        $this->db->or_where('lg.log_created_for!=',$user_id );

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    /* ----- get logs starts ----- */

    public function getLogs_asArray($user_id,$limit,$offset)
    {

        //echo '$limit:'.$limit.'<br>'.'$offset:'.$offset;die();

        $this->db->select('*');
        $this->db->from('rspm_tbl_log as lg');
        $this->db->where('lg.log_created_by', $user_id);
        $this->db->or_where('lg.log_created_for',$user_id );
        $this->db->or_where('lg.log_created_for!=',$user_id );
        $this->db->order_by('log_created_at','desc');

        if(!$offset){
            $this->db->limit($limit);
        }else{
            $this->db->limit($limit,$offset);
        }


        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
    }

    public function getUserCreatedLogs_asArray($user_id,$limit)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_log as lg');
        $this->db->where('lg.log_created_by', $user_id);
        $this->db->order_by('log_created_at','desc');
        $this->db->limit($limit);

        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
    }

    public function getUserRelatedLogs_asArray($user_id,$limit)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_log as lg');
        $this->db->where('lg.log_created_for',$user_id );
        $this->db->order_by('log_created_at','desc');
        $this->db->limit($limit);

        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
    }

    /* ----- get logs ends ----- */

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $user_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProduct($product_id)
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_id', $product_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getThriftGroup($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getThriftPayment($payment_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_payment_id', $payment_id);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getThriftPaymentRecieve($payment_recieve_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->where('tr.thrift_group_payment_id', $payment_recieve_id);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getProject($project_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project');
        $this->db->where('project_id', $project_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task');
        $this->db->where('task_id', $task_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getFile($file_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file');
        $this->db->where('file_id', $file_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getInvoice($invoice_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_invoice');
        $this->db->where('invoice_id', $invoice_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getTicket($ticket_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_ticket');
        $this->db->where('ticket_id', $ticket_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

}