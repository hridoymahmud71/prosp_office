<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogController extends MX_Controller
{

    function __construct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('log_module/Log_model');

        $this->load->library('form_validation');
        $this->load->library('custom_log_library');

        // application/libraries
        $this->load->library('custom_datetime_library');
    }

    public function viewLog()
    {
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('log_view');

        $data = array();

        $user_id = $this->session->userdata('user_id');

        $limit = 20;

        $data['timeline_elements'] = $this->getTimelineElements($user_id,$limit,$offset = false);

        $data['logItemsLimit'] = $limit;
        $data['totalLogs'] = $this->Log_model->countTotalLogs($user_id);

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("log_module/log_view_page", $data);
        $this->load->view("common_module/footer");

    }

    public function loadMoreLogs()
    {
        $this->lang->load('log_view');

        $user_id = $this->session->userdata('user_id');
        $limit = 20;

        //echo $this->input->post('itemOffset');die();
        $data['timeline_elements'] = $this->getTimelineElements($user_id,$limit,$offset = $this->input->post('itemOffset'));

        $additional_logs =  $this->load->view("log_module/log_view_ajax_page", $data);

        echo $additional_logs;

    }

    public function getTimelineElements($user_id,$limit,$offset)
    {

        $logs_array = $this->Log_model->getLogs_asArray($user_id, $limit,$offset);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {


                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                /*checking is today or yesterday  starts*/

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];


                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/



            }

            return $logs_array;

        } else {
            return false;
        }

    }

}