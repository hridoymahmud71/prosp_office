<?php

/*project logs*/
$lang['some'] = 'some';


/*task_logs*/
$lang['some'] = 'some';

/*Groupname text*/
$lang['admin_text'] = 'Admin' ;
$lang['client_text'] = 'Client' ;
$lang['staff_text'] = 'Staff' ;

$lang['employer_text'] = 'Organization' ;
$lang['employee_text'] = 'Thrifter' ;
$lang['trustee_text'] = 'Trustee' ;
$lang['unidentified_group'] = 'Unidentified Group' ;

/*Main Categoris*/
$lang['project_text'] = 'Project';
$lang['task_text'] = 'Task';
$lang['user_text'] = 'User';
$lang['file_text'] = 'File(s)';
$lang['invoice_text'] = 'Invoice';
$lang['ticket_text'] = 'Ticket';
$lang['ticket_response_text'] = 'Ticket'; //as response is not a separate entity

$lang['product_text'] = 'Product';
$lang['thrift_group_text'] = 'Thrift';
$lang['thrift_payment_text'] = 'Thrift Payment';
$lang['thrift_disbursement'] = 'Thrift Disbursement';
$lang['user_text'] = 'User';

$lang['general_settings_text'] = 'General Settings';
$lang['contact_settings_text'] = 'Contact Settings';
$lang['currency_settings_text'] = 'Currency Settings';
$lang['datetime_settings_text'] = 'Date & Time Settings';
$lang['system_settings_text'] = 'System Settings';
$lang['payment_settings_text'] = 'Payment Settings';
// and so on . . . . . . . . .


/*---------------*/
$lang['normal_text'] = 'Normal';
$lang['high_text'] = 'High';
$lang['low_text'] = 'Low';

/*Activities text*/
$lang['registered_string_text'] = '%1$s registered for an account';

$lang['created_string_text'] = '%1$s created %2$s';
$lang['updated_string_text'] = '%1$s updated %2$s';
$lang['activated_string_text'] = '%1$s made %2$s active';
$lang['deactivated_string_text'] = '%1$s made %2$s deactive';
$lang['deleted_string_text'] = '%1$s deleted %2$s';

$lang['approved_string_text'] = '%1$s approved %2$s';
$lang['joined_string_text'] = '%1$s joined %2$s';


$lang['completed_string_text'] = '%1$s made %2$s complete';
$lang['priority_changed_string_text'] = '%1$s changed priority of %2$s';

$lang['marked_solved_string_text'] = '%1$s marked %2$s as solved';
$lang['marked_unsolved_string_text'] = '%1$s marked %2$s as unsolved';

$lang['created_response_for_ticket_string_text'] = '%1$s responded to  %2$s';


$lang['staffs_changed_announce_string_text'] = '%1$s changed staffs in %2$s';

$lang['assigned_staff_string_text'] = '%1$s assigned %2$s to %3$s';
$lang['deassigned_staff_string_text'] = '%1$s deassigned %2$s from %3$s';

$lang['allocated_client_string_text'] = '%1$s allocated %2$s to %3$s';
$lang['deallocated_client_string_text'] = '%1$s deallocated %2$s from %3$s';

$lang['file_uploaded_by_string_text'] = '%1$s Uploaded File(s)';

$lang['opened_string_text'] = '%1$s made %2$s Open';
$lang['closed_string_text'] = '%1$s made %2$s Close';
$lang['cleared_string_text'] = '%1$s made %2$s Clear';
$lang['uncleared_string_text'] = '%1$s made %2$s Unclear';

$lang['change_text'] = '%1$s changed';

$lang['logged_in_text'] = '%1$s logged in';
$lang['logged_out_text'] = '%1$s logged out';

$lang['thrift_started_string_text'] = '%1$s has started';
$lang['thrift_deleted_string_text'] = '%1$s has been deleted by the system';
$lang['thrift_ended_string_text'] = '%1$s has ended';
$lang['thrift_payment_paid_string_text'] = '%1$s has given a %2$s';
$lang['thrift_payment_declined_string_text'] = '%1$s has missed a %2$s';
$lang['thrift_disbursement_failed_string_text'] = '%1$s has a failed %2$s';


// and so on . . . . . . . . .


/*status lang*/
$lang['status_inactive_text'] = 'Inactive';
$lang['status_active_text'] = 'Active';

$lang['status_open_text'] = 'Open';
$lang['status_close_text'] = 'Close';

$lang['status_clear_text'] = 'Clear';
$lang['status_unclear_text'] = 'Unclear';

/*other important lang*/
$lang['none_text'] = 'None';
$lang['belongs_to_string_text'] = '%1$s belongs to %2$s';

/*change_title_message_text*/
$lang['change_title_message_text'] = '%1$s changed';
$lang['default_change_title_message_text'] = 'A change has occured';

/*change list:  fieldname-from-to*/
$lang['change_string_text'] = '%1$s changed from %2$s to %3$s';


/*------file related-------------------*/
$lang['uploaded_files_are_string_text'] = 'Uploaded Files are %1$s';
$lang['uploaded_file_is_string_text'] = 'Uploaded File is %1$s';
$lang['file_belongs_to_no_project_text'] = 'File Belongs To No Project';

$lang['task_belongs_to_project_string_text'] = '%1$s Belongs To %2$s';


?>