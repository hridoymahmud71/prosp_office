<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model
{
    private $current_timestamp = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->load->library('custom_datetime_library');
        $this->current_timestamp = $this->custom_datetime_library->getCurrentTimestamp();

    }


    public function countEmployees($common_filter_value = false, $specific_filters = false, $employer_id)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.mem_id_num as mem_id_num,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); // employee group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }


        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('u.email', $common_filter_value);
            $this->db->or_like('u.mem_id_num', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'email' || $column_name == 'mem_id_num') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getEmployees($common_filter_value = false, $specific_filters = false, $order, $limit, $employer_id)
    {

        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.mem_id_num as mem_id_num,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.user_employer_id as user_employer_id
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); // employee group_id 6

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        if ($employer_id) {
            $this->db->where('ud.user_employer_id', $employer_id);
        }


        if ($common_filter_value != false) {
            $this->db->group_start();

            $this->db->like('u.email', $common_filter_value);
            $this->db->or_like('u.mem_id_num', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'email' || $column_name == 'mem_id_num') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }


        $this->db->order_by('u.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }


    public function countEmployers($common_filter_value = false, $specific_filters = false)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); // employer group_id 6


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'company' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getEmployers($common_filter_value = false, $specific_filters = false, $order, $limit)
    {

        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); // employer group_id 6


        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->or_like('u.company', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'company' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }

        }


        $this->db->order_by('u.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    /*----------------------------------------------------------------------------------------------------------------*/


    public function getUserInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getAllPayments()
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_is_payment_paid', 1);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getAllPaymentRecieves()
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->where('tr.thrift_group_is_payment_recieved', 1);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }


    public function getEmployersRelatedPayments($employer_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_is_payment_paid', 1);

        $employee_query =
            'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';

        $this->db->where_in('tp.thrift_group_payer_member_id', $employee_query, false);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getEmployersRelatedPaymentRecieves($employer_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->where('tr.thrift_group_is_payment_recieved', 1);

        $employee_query =
            'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


        $this->db->where_in('tr.thrift_group_member_id', $employee_query, false);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getEmployeesRelatedPayments($employee_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->where('tp.thrift_group_is_payment_paid', 1);
        $this->db->where('tp.thrift_group_payer_member_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result();

        //echo $this->db->last_query();die();

        return $result;
    }

    public function getEmployeesRelatedPaymentRecieves($employee_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->where('tr.thrift_group_is_payment_recieved', 1);
        $this->db->where('tr.thrift_group_member_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result();

        //echo $this->db->last_query();die();

        return $result;
    }


    public function getThriftGroup($thrift_group_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');

        $this->db->where('tg.thrift_group_id', $thrift_group_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function countPaymentReport($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tp.thrift_group_combine_payment_number,
                       tp.thrift_group_payment_amount,
                       tp.thrift_group_payer_member_id,
                       tp.thrift_group_payment_number,
                       tp.thrift_group_payee_member_id,
                       tp.thrift_group_payment_date,
                        
        ', false);

        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id', 'left');


        if ($date_from && $date_to) {
            $this->db->where('tp.thrift_group_payment_date >=', $date_from);

            if ($date_to > $this->current_timestamp) {
                $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
            } else {

                $this->db->where('tp.thrift_group_payment_date <=', $date_to);
            }

        } else if(!$date_to) {
            $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
        }

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tp .thrift_group_payer_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tp.thrift_group_payment_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_number') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_amount') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payer_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payer_member_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_payee_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payee_member_id', $filter_value);
                    }

                }


            }

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


    public function getPaymentReport($common_filter_value = false, $specific_filters = false, $order, $limit, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tp.thrift_group_combine_payment_number,
                       tp.thrift_group_payment_amount,
                       tp.thrift_group_payer_member_id,
                       tp.thrift_group_payment_number,
                       tp.thrift_group_payee_member_id,
                       tp.thrift_group_payment_date,
                        
        ', false);

        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id', 'left');

        if ($date_from && $date_to) {
            $this->db->where('tp.thrift_group_payment_date >=', $date_from);
            if ($date_to > $this->current_timestamp) {
                $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('tp.thrift_group_payment_date <=', $date_to);
            }
        }else if(!$date_to) {
            $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
        }

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tp.thrift_group_payer_member_id', $employee_query, false);

        }

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tp.thrift_group_payment_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_number') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_amount') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payer_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payer_member_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_payee_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payee_member_id', $filter_value);
                    }

                }


            }

        }


        if ($order['column'] == 'thrift_group_number'
            ||
            $order['column'] == 'thrift_group_start_date'
            ||
            $order['column'] == 'thrift_group_end_date'
        ) {
            $this->db->order_by('tg.' . $order['column'], $order['by']);
        }

        if ($order['column'] == 'thrift_group_payment_amount'
            ||
            $order['column'] == 'thrift_group_payment_number'
            ||
            $order['column'] == 'thrift_group_payment_date'
        ) {
            $this->db->order_by('tp.' . $order['column'], $order['by']);
        }


        if ($limit) {
            $this->db->limit($limit['length'], $limit['start']);
        }


        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    public function getTotalPayment($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select(' SUM( tp.thrift_group_payment_amount) as total_payment_amount', false);

        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id', 'left');


        if ($date_from && $date_to) {
            $this->db->where('tp.thrift_group_payment_date >=', $date_from);
            if ($date_to > $this->current_timestamp) {
                $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('tp.thrift_group_payment_date <=', $date_to);
            }
        }else if(!$date_to) {
            $this->db->where('tp.thrift_group_payment_date <=', $this->current_timestamp);
        }

        //echo '$date_from'.$date_from.' '.'$date_to'.$date_to; die();

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tp .thrift_group_payer_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tp.thrift_group_payment_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_number') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_amount') {
                    $this->db->like('tp.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payer_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payer_member_id', $filter_value);
                    }

                }

                if ($column_name == 'thrift_group_payee_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tg.thrift_group_payee_member_id', $filter_value);
                    }

                }


            }

        }


        $query = $this->db->get();


        //echo $this->db->last_query();die();

        $row = $query->row();


        return $row;
    }


    /*----------------------------------------------------------------------------------------------------------------*/


    public function countPaymentRecieveReport($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tr.thrift_group_combine_payment_number,
                       tr.thrift_group_payment_recieve_amount,
                       tr.thrift_group_member_id,
                       tr.thrift_group_payment_recieve_number,                      
                       tr.thrift_group_payment_date,
                        
        ', false);

        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id', 'left');


        if ($date_from && $date_to) {
            $this->db->where('tr.thrift_group_payment_date >=', $date_from);
            if ($date_to > $this->current_timestamp) {
                $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('tr.thrift_group_payment_date <=', $date_to);
            }
        }else if(!$date_to) {
            $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
        }

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tr.thrift_group_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tr .thrift_group_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tr.thrift_group_payment_recieve_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_number') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_amount') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tr.thrift_group_member_id', $filter_value);
                    }

                }


            }

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


    public function getPaymentRecieveReport($common_filter_value = false, $specific_filters = false, $order, $limit, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,                                              
                        
                        tg.thrift_group_start_date,
                        tg.thrift_group_end_date,                        
                      
                       tr.thrift_group_combine_payment_number,
                       tr.thrift_group_payment_recieve_amount,
                       tr.thrift_group_member_id,
                       tr.thrift_group_payment_recieve_number,                      
                       tr.thrift_group_payment_date,
                        
        ', false);

        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id', 'left');


        if ($date_from && $date_to) {
            $this->db->where('tr.thrift_group_payment_date >=', $date_from);
            if ($date_to > $this->current_timestamp) {
                $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('tr.thrift_group_payment_date <=', $date_to);
            }
        }else if(!$date_to) {
            $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
        }

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tr.thrift_group_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tr .thrift_group_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tr.thrift_group_payment_recieve_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_number') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_amount') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tr.thrift_group_member_id', $filter_value);
                    }

                }


            }

        }


        if ($order['column'] == 'thrift_group_number'
            ||
            $order['column'] == 'thrift_group_start_date'
            ||
            $order['column'] == 'thrift_group_end_date'
        ) {
            $this->db->order_by('tg.' . $order['column'], $order['by']);
        }

        if ($order['column'] == 'thrift_group_payment_recieve_amount'
            ||
            $order['column'] == 'thrift_group_payment_recieve_number'
            ||
            $order['column'] == 'thrift_group_payment_date'
        ) {
            $this->db->order_by('tr.' . $order['column'], $order['by']);
        }


        if ($limit) {
            $this->db->limit($limit['length'], $limit['start']);
        }


        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }

    public function getTotalPaymentRecieve($common_filter_value = false, $specific_filters = false, $which_report, $employer_id, $employee_id, $date_from, $date_to)
    {
        $this->db->select(' SUM( tr.thrift_group_payment_recieve_amount) as total_payment_recieve_amount', false);

        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id', 'left');


        if ($date_from && $date_to) {
            $this->db->where('tr.thrift_group_payment_date >=', $date_from);
            if ($date_to > $this->current_timestamp) {
                $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('tr.thrift_group_payment_date <=', $date_to);
            }
        }else if(!$date_to) {
            $this->db->where('tr.thrift_group_payment_date <=', $this->current_timestamp);
        }

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('tr.thrift_group_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('tr .thrift_group_member_id', $employee_query, false);

        }


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('tg.thrift_group_number', $common_filter_value);
            $this->db->or_like('tr.thrift_group_payment_recieve_number', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'thrift_group_number') {
                    $this->db->like('tg.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_number') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_payment_recieve_amount') {
                    $this->db->like('tr.' . $column_name, $filter_value);
                }

                if ($column_name == 'thrift_group_member_id') {

                    if ($filter_value == 'all') {
                        //do nothing
                    } else {
                        $this->db->where('tr.thrift_group_member_id', $filter_value);
                    }

                }


            }

        }


        $query = $this->db->get();

        $row = $query->row();


        return $row;
    }


    /*----------------------------------------------------------------------------------------------------------------*/

    public function getPaymentMaxMinTimestamps($which_report, $employer_id, $employee_id, $curr_year_init_ts, $curr_year_final_ts)
    {

        $this->db->select("MAX(thrift_group_payment_date) AS max_pdate_ts,MIN(thrift_group_payment_date) AS min_pdate_ts");


        $this->db->from("pg_thrift_group_payments");


        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('thrift_group_payer_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('thrift_group_payer_member_id', $employee_query, false);

        }

        if ($curr_year_init_ts && $curr_year_final_ts) {
            $this->db->where("thrift_group_payment_date >=", $curr_year_init_ts);

            if ($curr_year_final_ts > $this->current_timestamp) {
                $this->db->where('thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('thrift_group_payment_date <=', $curr_year_final_ts);
            }
        }else if(!$curr_year_final_ts) {
            $this->db->where('thrift_group_payment_date <=', $this->current_timestamp);
        }

        $query = $this->db->get();

        $row = $query->row();
        return $row;

    }

    public function getPaymentRecieveMaxMinTimestamps($which_report, $employer_id, $employee_id, $curr_year_init_ts, $curr_year_final_ts)
    {

        $this->db->select("MAX(thrift_group_payment_date) AS max_pdate_ts,MIN(thrift_group_payment_date) AS min_pdate_ts");


        $this->db->from("pg_thrift_group_payment_recieves");

        if (($which_report == 'employee_report' || $which_report == 'employee_report_as_employee')
            && $employee_id
        ) {
            $this->db->where('thrift_group_member_id', $employee_id);
        }

        if (($which_report == 'employer_report' || $which_report == 'employer_report_as_employer')
            && $employer_id
        ) {

            $employee_query =
                'select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $employer_id . ')';


            $this->db->where_in('thrift_group_member_id', $employee_query, false);

        }

        if ($curr_year_init_ts && $curr_year_final_ts) {
            $this->db->where("thrift_group_payment_date >=", $curr_year_init_ts);

            if ($curr_year_final_ts > $this->current_timestamp) {
                $this->db->where('thrift_group_payment_date <=', $this->current_timestamp);
            } else {
                $this->db->where('thrift_group_payment_date <=', $curr_year_final_ts);
            }
        }else if(!$curr_year_final_ts) {
            $this->db->where('thrift_group_payment_date <=', $this->current_timestamp);
        }

        $query = $this->db->get();

        $row = $query->row();
        return $row;

    }

    public function countConcludedThrifts($user_id)
    {
        $this->db->select('
                        tg.thrift_group_id,                      
                        
        ', false);

        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);
        $this->db->where('tg.thrift_group_open', 0);
        $this->db->where('tg.thrift_group_activation_status', 0);
        $this->db->where('tg.thrift_group_term_duration', 'tg.thrift_group_current_cycle', false);

        /*extra starts*/
        $user_query = 'Select u.id from users as u where u.deletion_status!=1 and u.id=' . $user_id;

        $thrifts_query =
            'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $user_query . ')';


        $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);

        /*extra ends*/


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }


}
