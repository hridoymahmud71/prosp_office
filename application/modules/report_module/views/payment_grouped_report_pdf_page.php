<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Payment Report</title>


    <meta name="viewport" content="width=device-width">


    <style>
        /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        *:before, *:after {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            font-family: "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
            color: #5e5d52;
        }

        a {
            color: goldenrod;
        }

        a:hover, a:focus {
            color: goldenrod;
        }

        .container {
            margin: 5% 3%;
        }

        @media (min-width: 48em) {
            .container {
                margin: 2%;
            }
        }

        @media (min-width: 75em) {
            .container {
                margin: 2em auto;
                max-width: 75em;
            }
        }

        .responsive-table {
            width: 100%;
            margin-bottom: 1.5em;
        }

        @media (min-width: 48em) {
            .responsive-table {
                font-size: .9em;
            }
        }

        @media (min-width: 62em) {
            .responsive-table {
                font-size: 1em;
            }
        }

        .responsive-table thead {
            position: absolute;
            clip: rect(1px 1px 1px 1px);
            /* IE6, IE7 */
            clip: rect(1px, 1px, 1px, 1px);
            padding: 0;
            border: 0;
            height: 1px;
            width: 1px;
            overflow: hidden;
        }

        @media (min-width: 48em) {
            .responsive-table thead {
                position: relative;
                clip: auto;
                height: auto;
                width: auto;
                overflow: auto;
            }
        }

        .responsive-table thead th {
            background-color: goldenrod;
            border: 1px solid goldenrod;
            font-weight: normal;
            text-align: center;
            color: white;
        }

        .responsive-table thead th:first-of-type {
            text-align: left;
        }

        .responsive-table tbody,
        .responsive-table tr,
        .responsive-table th,
        .responsive-table td {
            display: block;
            padding: 0;
            text-align: left;
            white-space: normal;
        }

        @media (min-width: 48em) {
            .responsive-table tr {
                display: table-row;
            }
        }

        .responsive-table th,
        .responsive-table td {
            padding: .5em;
            vertical-align: middle;
        }

        @media (min-width: 30em) {
            .responsive-table th,
            .responsive-table td {
                padding: .75em .5em;
            }
        }

        @media (min-width: 48em) {
            .responsive-table th,
            .responsive-table td {
                display: table-cell;
                padding: .5em;
            }
        }

        @media (min-width: 62em) {
            .responsive-table th,
            .responsive-table td {
                padding: .75em .5em;
            }
        }

        @media (min-width: 75em) {
            .responsive-table th,
            .responsive-table td {
                padding: .75em;
            }
        }

        .responsive-table caption {
            margin-bottom: 1em;
            font-size: 1em;
            font-weight: bold;
            text-align: center;
        }

        @media (min-width: 48em) {
            .responsive-table caption {
                font-size: 1.5em;
            }
        }

        .responsive-table tfoot {
            font-size: .8em;
            font-style: italic;
        }

        @media (min-width: 62em) {
            .responsive-table tfoot {
                font-size: .9em;
            }
        }

        @media (min-width: 48em) {
            .responsive-table tbody {
                display: table-row-group;
            }
        }

        .responsive-table tbody tr {
            margin-bottom: 1em;
            border: 2px solid goldenrod;
        }

        @media (min-width: 48em) {
            .responsive-table tbody tr {
                display: table-row;
                border-width: 1px;
            }
        }

        .responsive-table tbody tr:last-of-type {
            margin-bottom: 0;
        }

        @media (min-width: 48em) {
            .responsive-table tbody tr:nth-of-type(even) {
                background-color: rgba(94, 93, 82, 0.1);
            }
        }

        .responsive-table tbody th[scope="row"] {
            background-color: goldenrod;
            color: white;
        }

        @media (min-width: 48em) {
            .responsive-table tbody th[scope="row"] {
                background-color: transparent;
                color: #5e5d52;
                text-align: left;
            }
        }

        .responsive-table tbody td {
            text-align: right;
        }

        @media (min-width: 30em) {
            .responsive-table tbody td {
                border-bottom: 1px solid goldenrod;
            }
        }

        @media (min-width: 48em) {
            .responsive-table tbody td {
                text-align: center;
            }
        }

        .responsive-table tbody td[data-type=currency] {
            text-align: right;
        }

        .responsive-table tbody td[data-title]:before {
            content: attr(data-title);
            float: left;
            font-size: .8em;
            color: rgba(94, 93, 82, 0.75);
        }

        @media (min-width: 30em) {
            .responsive-table tbody td[data-title]:before {
                font-size: .9em;
            }
        }

        @media (min-width: 48em) {
            .responsive-table tbody td[data-title]:before {
                content: none;
            }
        }

        /*i wrote*/

        .img_logo img {
            width: 50%;
        }

        .img_logo {
            text-align: center;
        }
    </style>
</head>

<body>
<div class="container">

    <?php if ($site_logo) { ?>
        <div class="img_logo">

            <img class=""
                 src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $site_logo ?>"
                 alt="Logo">

        </div>
    <?php } ?>
    <br><br>
    <table id="tbl" class="responsive-table">
        <caption><strong>Payment Report</strong></caption>
        <thead>
        <tr>
            <th scope="col"><?= lang('thrift_group_payer_member_text') ?></th>
            <th scope="col"><?= lang('thrift_group_payment_amount_text') ?></th>
            <th scope="col"><?= lang('thrift_group_number_text') ?></th>
            <th scope="col"><?= lang('thrift_group_thrifter_id_text') ?></th>
            <th scope="col"><?= lang('thrift_group_payment_number_text') ?></th>
            <th scope="col"><?= lang('thrift_group_payment_date_text') ?></th>
            <th scope="col"><?= lang('thrift_group_start_date_text') ?></th>
            <th scope="col"><?= lang('thrift_group_end_date_text') ?></th>
        </tr>
        </thead>
        <tfoot>
        <?php
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        ?>
        <tr>
            <td colspan="6"><?= $actual_link ?></td>
        </tr>
        </tfoot>
        <tbody>
        <?php if ($grps) { ?>
            <?php foreach ($grps as $g) { ?>
                <tr>
                    <td style="border-top: 1px solid black;border-bottom: 1px solid black"><?= $g->comp_title ?></td>
                    <td style="border-top: 1px solid black;border-bottom: 1px solid black" colspan="7"></td>
                </tr>

                <?php if ($g->payments) { ?>
                    <?php foreach ($g->payments as $p) { ?>

                        <tr>
                            <td><?= $p->thrift_group_payer_name ?></td>
                            <td data-type="currency"><?= $p->p_amt->display ?></td>
                            <td><?= $p->thrift_group_number ?></td>
                            <td><?= $p->thrift_group_payer_id_num ?></td>
                            <td><?= $p->thrift_group_payment_number ?></td>
                            <td><?= $p->tp_p_dt->display ?></td>
                            <td><?= $p->tg_en_dt->display ?></td>
                            <td><?= $p->tg_st_dt->display ?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>

            <?php } ?>

            <?php if ($total_payment_amount_display) { ?>
                <tr>
                    <td id="set_colspan" scope="col" colspan="6"></td>
                    <td> <?= lang('total_text') ?></td>
                    <td> <?= $total_payment_amount_display ?></td>
                </tr>
            <?php } ?>

        <?php } ?>

        </tbody>
    </table>
</div>


</body>
</html>


