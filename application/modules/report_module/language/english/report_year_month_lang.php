<?php

$lang['reports_by_time_text'] = 'Reports By time';
$lang['payment_report_text'] = 'Payments';
$lang['payment_recieve_report_text'] = 'Disbursements';

$lang['years_text'] = 'Years';
$lang['months_text'] = 'Months';

$lang['all_text'] = 'All';
$lang['all_years_text'] = 'All Years';
$lang['current_year_text'] = 'Current Year';

$lang['report_text'] = 'Report Text';

$lang['breadcrum_report_text'] = 'Report';
$lang['breadcrum_report_details_text'] = 'Time';