<?php

$lang['page_title_text'] = 'Outbox';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Send Message';


$lang['box_title_text'] = 'Messages sent';

$lang['write_message_text'] = 'Write a message';




//ok
$lang['successful_text'] = 'Successful!';
$lang['successful_message_send_text'] = 'Message Successfully Sent';
$lang['view_message_text'] = 'View Message';
$lang['delete_success_text'] = 'Succesfully deleted the message.';


$lang['add_button_text'] = 'Add A User';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_archived_text'] = 'Yes';
$lang['option_non_archived_text'] = 'No';


$lang['column_created_at_text'] = 'Time';
$lang['column_message_text'] = 'Message';
$lang['column_archived_text'] = 'Archived?';
$lang['column_actions_text'] = 'Actions';

$lang['status_archive_text'] = 'Archived';
$lang['status_non_archive_text'] = 'Not Archived';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This User ?';
$lang['swal_confirm_button_text'] = 'yes delete this User';
$lang['swal_cancel_button_text'] = 'No, keep this user';




$lang['archive_success_text'] = 'Message archived';
$lang['dearchive_success_text'] = 'Message removed from archive';

$lang['read_success_text'] = 'Message marked as read';
$lang['unread_success_text'] = 'Message marked as unread';

$lang['creation_time_unknown_text'] = 'Unknown';

/*tooltip text*/
$lang['tooltip_archive_text'] = 'Make Message Acrchived';
$lang['tooltip_dearchive_text'] = 'Make Message Non Archived';

$lang['tooltip_view_text'] = 'View Message ';
$lang['tooltip_edit_text'] = 'Edit Message ';
$lang['tooltip_delete_text'] = 'Delete Message ';



/*loading*/
$lang['loading_text'] = 'Loading . . .';


