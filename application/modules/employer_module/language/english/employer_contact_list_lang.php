<?php

/*page texts*/
$lang['page_title_text'] = 'Conatct List';

$lang['page_employer_subtitle_text'] = 'Manage Contacts';
$lang['page_admin_subtitle_text'] = 'Manage Contacts';

$lang['table_title_text'] = 'Manage Contacts';
$lang['no_user_found_text'] = 'No Contact Is Found !';
$lang['no_matching_user_found_text'] = 'No matching Contact Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Contact';
$lang['breadcrumb_page_text'] = 'Contact List';

$lang['add_button_text'] = 'Add Contact';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_all_text'] = 'All';
$lang['option_admin_text'] = 'Admin';
$lang['option_staff_text'] = 'Staff';
$lang['option_client_text'] = 'Client';

$lang['column_member_id_text'] = 'Member ID';
$lang['column_thrifter_id_text'] = 'Thrifters ID';
$lang['column_firstname_text'] = 'First Name';
$lang['column_lastname_text'] = 'Last Name';
$lang['column_email_text'] = 'Email';
$lang['column_group_text'] = 'Group';
$lang['column_organization_text']='Organization';
$lang['column_org_select_text']='Select Organizer';
$lang['column_status_text'] = 'Status';
$lang['column_created_on_text'] = 'Created ON';
$lang['column_last_login_text'] = 'Last Login';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This Contact ?';
$lang['swal_confirm_button_text'] = 'yes delete this Contact';
$lang['swal_cancel_button_text'] = 'No, keep this Contact';

$lang['swal_password_title_text'] = 'Generate and send a new password to this user?';
$lang['swal_password_confirm_button_text'] = 'Yes';
$lang['swal_password_cancel_button_text'] = 'No';

$lang['delete_success_text'] = 'Successfully deleted the Contact.';

$lang['successfull_text'] = 'Successful';
$lang['user_add_success_text'] = 'Successfully added user. Edit the created user to put him/her in a group.';
$lang['update_success_text'] = 'Successfully updated the user.';

$lang['activate_success_text'] = 'Contact Activated';
$lang['dectivate_success_text'] = 'Contact Deactivated';
$lang['deletion_success_text'] = 'Contact Deleted';

$lang['see_user_text'] = 'See Contact';
$lang['edit_user_text'] = 'Edit Contact';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['never_logged_in_text'] = 'Never Logged In';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Contact Active';
$lang['tooltip_deactivate_text'] = 'Make Contact Deactive';

$lang['tooltip_view_text'] = 'View Contact ';
$lang['tooltip_edit_text'] = 'Edit Contact ';
$lang['tooltip_delete_text'] = 'Delete Contact ';
$lang['tooltip_message_text'] = 'Mail/Message Contact';
$lang['tooltip_send_password_text'] = 'Send Password';



/*loading*/
$lang['loading_text'] = 'Loading Contacts . . .';

/*Employee*/

$lang['password_send_success_text']='Newly generated password is sent to user via mail';

/*swal*/
$lang['swal_delete_title_text']= 'Want to delete contact ?';
$lang['swal_delete_confirm_button_text']= 'Yes';
$lang['swal_delete_cancel_button_text']= 'No';





