<?php

/*page texts*/
$lang['page_title_text'] = 'Unapproved member List';

$lang['page_employer_subtitle_text'] = 'Manage Unapproved members';
$lang['page_admin_subtitle_text'] = 'Manage Unapproved members';

$lang['table_title_text'] = 'Manage Unapproved members';
$lang['no_user_found_text'] = 'No Unapproved Members Is Found !';
$lang['no_matching_user_found_text'] = 'No matching Unapproved Member Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Unapproved members';
$lang['breadcrumb_page_text'] = 'Admin';

$lang['add_button_text'] = 'Add Thrifter';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_all_text'] = 'All';
$lang['option_admin_text'] = 'Admin';
$lang['option_staff_text'] = 'Staff';
$lang['option_client_text'] = 'Client';

$lang['column_member_id_text'] = 'Members ID';
$lang['column_thrifter_id_text'] = 'Thrifters ID';
$lang['column_firstname_text'] = 'First Name';
$lang['column_lastname_text'] = 'Last Name';
$lang['column_email_text'] = 'Email';
$lang['column_group_text'] = 'Group';
$lang['column_organization_text']='Organization';
$lang['column_org_select_text']='Select Organizer';
$lang['column_status_text'] = 'Status';
$lang['column_created_on_text'] = 'Created ON';
$lang['column_last_login_text'] = 'Last Login';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This Thrifter?';
$lang['swal_confirm_button_text'] = 'yes delete this Thrifter';
$lang['swal_cancel_button_text'] = 'No, keep this Thrifter';

$lang['swal_password_title_text'] = 'Generate and send a new password to this user?';
$lang['swal_password_confirm_button_text'] = 'Yes';
$lang['swal_password_cancel_button_text'] = 'No';

$lang['swal_approve_title_text'] = 'Approve this user ?';
$lang['swal_approve_confirm_button_text'] = 'Yes';
$lang['swal_approve_cancel_button_text'] = 'No';

$lang['swal_disapprove_title_text'] = 'Disapprove this user ?';
$lang['swal_disapprove_confirm_button_text'] = 'Yes';
$lang['swal_disapprove_cancel_button_text'] = 'No';

$lang['delete_success_text'] = 'Successfully deleted the Thrifter.';

$lang['successfull_text'] = 'Successful';
$lang['user_add_success_text'] = 'Successfully added user. Edit the created user to put him/her in a group.';
$lang['update_success_text'] = 'Successfully updated the user.';

$lang['activate_success_text'] = 'Thrifter Activated';
$lang['dectivate_success_text'] = 'Thrifter Deactivated';

$lang['user_approve_success_text'] = 'User Approved';
$lang['user_disapprove_success_text'] = 'User Disapproved';

$lang['see_user_text'] = 'See Thrifter';
$lang['edit_user_text'] = 'Edit Thrifter';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['never_logged_in_text'] = 'Never Logged In';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Thrifter Active';
$lang['tooltip_deactivate_text'] = 'Make Thrifter Deactive';

$lang['tooltip_view_text'] = 'View Thrifter ';
$lang['tooltip_edit_text'] = 'Edit Thrifter ';
$lang['tooltip_delete_text'] = 'Delete Thrifter ';
$lang['tooltip_message_text'] = 'Mail/Message Thrifter';
$lang['tooltip_send_password_text'] = 'Send Password';



/*loading*/
$lang['loading_text'] = 'Loading Thrifter . . .';

/*Employee*/
$lang['add_employee_text']='Thrifter Successfully Inserted';

$lang['password_send_success_text']='Newly generated password is sent to user via mail';




