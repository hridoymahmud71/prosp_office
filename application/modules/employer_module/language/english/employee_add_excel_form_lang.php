<?php

/*page texts*/
$lang['page_title_text'] = 'Add Thrifter';
$lang['page_subtitle_add_text'] = 'Thrifter add';

$lang['breadcrum_home_text'] = 'Organization';
$lang['breadcrumb_page_add_text'] = 'New Thrifter';

/*page excel form*/

$lang['employee_add_form_header_text'] = 'Upload Thrifter Excel file';
$lang['file_input_text'] = 'Add Thrifter list (Excel file)';
$lang['employer_select_text'] = 'Select Organization';
$lang['file_submit_text'] = 'SUBMIT';

$lang['employee_add_modal_header_text'] = 'Upload Single Thrifter Details';
$lang['single_employee_text'] = 'Click here for Single Thrifter';
$lang['single_employee_button_text'] = 'Add Single Thrifter';



// modal
$lang['modal_title_text'] = 'Insert a New Thrifter';
$lang['modal_first_name_text'] = 'First Name';
$lang['modal_last_name_text'] = 'Last Name';
$lang['modal_email_text'] = 'Email';
$lang['modal_user_ofc_id_text'] = 'Employee ID Number';
$lang['modal_user_gender_text'] = 'Gender';
$lang['modal_user_salary_text'] = 'Thrifter\'s Salary';
$lang['modal_user_home_address_text'] = 'Address';
$lang['modal_user_gender_text'] = 'Gender';
$lang['modal_phone_text'] = 'Thrifter\'s Phone';
$lang['modal_user_gender_text'] = 'Gender';
$lang['modal_user_gender_male_text'] = 'MALE';
$lang['modal_user_gender_female_text'] = 'FEMALE';
$lang['modal_user_gender_text'] = 'Gender';
$lang['modal_age_text'] = 'Date of Birth';
$lang['modal_user_hire_date_text'] = 'Hire Date';
$lang['modal_user_bvn_text'] = 'BVN number';

$lang['bank_select_text'] = 'Select Bank ';
$lang['modal_user_bank_text'] = 'Bank';
$lang['modal_user_bank_name_text'] = 'Bank Name';
$lang['modal_user_bank_account_no_text'] = 'Bank A/C number';

$lang['modal_cancel_text'] = 'RESET';


// ERROR MESSAGES

$lang['success_number_employee_upload_text'] = ' thrifter(s) added successfully.';
$lang['error_number_employee_upload_text'] = ' thrifter(s) detail is not uploaded as no valid/unique email found.';
$lang['click_form_download_text'] = 'Click here for excel format';
$lang['banks_with_id_text'] = 'Banks with Id';
$lang['employee_email_importance_text'] = '** Thrifter\'s email section is mandatory. if no email found, specific thrifter(s) data will not be uploaded.';
$lang['employer_name_require_text'] = '** Organization\'s  is Required';
$lang['field_mandatory_text'] = '** This field is Required';
$lang['excel_file_format_text'] = '** The filename is not readable. Please upload as the given file format';
$lang['row_number_text'] = ' Row number ';
$lang['contains_text'] = ' contains a ';

$lang['employee_insert_err_text'] = '** Your given email ID already exists. Please choose a different email ID';


/*-------------------------------------------------------*/

$lang['label_user_street_1_text'] = 'Street 1';
$lang['label_user_street_2_text'] = 'Street 2';
$lang['label_user_country_text'] = 'Country';
$lang['label_user_state_text'] = 'State';
$lang['label_user_city_text'] = 'City';

$lang['placeholder_user_street_1_text'] = 'Enter Street 1';
$lang['placeholder_user_street_2_text'] = 'Enter Street 2';
$lang['placeholder_user_country_text'] = 'Select a Country';
$lang['placeholder_user_state_text'] = 'Select a State';
$lang['placeholder_user_city_text'] = 'Select a City';

$lang['not_found_text'] = 'Not Found';

$lang['country_not_found_text'] = 'Country Not Found';
$lang['state_not_found_text'] = 'State Not Found';
$lang['city_not_found_text'] = 'City Not Found';


?>