<?php

/*page texts*/
$lang['page_title_text'] = 'Partner\'s List';

$lang['page_employer_subtitle_text'] = 'Manage Thrifter\'s';
$lang['page_admin_subtitle_text'] = 'Manage Thrifter\'s';

$lang['page_admin_subtitle_text'] = 'Manage Partner\'s';

$lang['table_title_text'] = 'Manage Partner\'s';
$lang['no_user_found_text'] = 'No Organizaton Is Found !';
$lang['no_matching_user_found_text'] = 'No matching Organizaton Is Found !';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Thrifter';
$lang['breadcrumb_page_text'] = 'Admin';

$lang['add_button_text'] = 'Add Organization';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_all_text'] = 'All';
$lang['option_admin_text'] = 'Admin';
$lang['option_staff_text'] = 'Staff';
$lang['option_client_text'] = 'Client';

$lang['column_organization_name_text'] = 'Organization Name';
$lang['column_organization_phone_text'] = 'Phone';
$lang['column_organization_contact_person_text'] = 'Contact info';
$lang['column_website_text'] = 'Website';

$lang['column_thrifter_id_text'] = 'Thrifters ID';
$lang['column_firstname_text'] = 'First Name';
$lang['column_lastname_text'] = 'Last Name';
$lang['column_email_text'] = 'Email';
$lang['column_group_text'] = 'Group';
$lang['column_organization_text']='Organization';
$lang['column_org_select_text']='Select Organization';
$lang['column_status_text'] = 'Status';
$lang['column_created_on_text'] = 'Created ON';
$lang['column_last_login_text'] = 'Last Login';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This Organizaton?';
$lang['swal_confirm_button_text'] = 'yes delete this Organizaton';
$lang['swal_cancel_button_text'] = 'No, keep this Organizaton';

$lang['swal_password_title_text'] = 'Generate and send a new password to this user ?';
$lang['swal_password_confirm_button_text'] = 'Yes';
$lang['swal_password_cancel_button_text'] = 'No';

$lang['delete_success_text'] = 'Successfully deleted the Organizaton.';

$lang['successfull_text'] = 'Successful';
$lang['unsuccessfull_text'] = 'Unsuccessful';
$lang['user_add_success_text'] = 'Successfully added user. Edit the created user to put him/her in a group.';
$lang['update_success_text'] = 'Successfully updated the user.';

$lang['activate_success_text'] = 'Organizaton Activated';
$lang['dectivate_success_text'] = 'Organizaton Deactivated';

$lang['see_user_text'] = 'See Organizaton';
$lang['edit_user_text'] = 'Edit Organizaton';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['never_logged_in_text'] = 'Never Logged In';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make Organizaton Active';
$lang['tooltip_deactivate_text'] = 'Make Organizaton Deactive';

$lang['tooltip_view_text'] = 'View Organizaton ';
$lang['tooltip_edit_text'] = 'Edit Organizaton ';
$lang['tooltip_delete_text'] = 'Delete Organizaton ';
$lang['tooltip_message_text'] = 'Mail/Message Organizaton';
$lang['tooltip_send_password_text'] = 'Send Password';


/*loading*/
$lang['loading_text'] = 'Loading Organizaton . . .';

/*Employee*/
$lang['add_employer_text']='Organizaton Successfully Inserted';

$lang['password_send_success_text']='Link for password reset is sent to user via mail';
$lang['password_send_error_text']='Link for password reset could not be sent to user via mail';



