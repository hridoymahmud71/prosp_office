<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>

                    <?php if ($which_form == 'add') { ?>
                        <small><?php echo lang('page_subtitle_add_text') ?></small>
                    <?php } ?>
                    <?php if ($which_form == 'edit') { ?>
                        <small><?php echo lang('page_subtitle_edit_text') ?></small>
                    <?php } ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrum_home_text') ?></a></li>
                    <?php if ($which_form == 'add') { ?>
                        <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                    <?php } ?>
                    <?php if ($which_form == 'edit') { ?>
                        <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_edit_text') ?></li>
                    <?php } ?>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?php if ($this->session->flashdata('contact_create_success_text')) { ?>
                            <strong> <?= lang('contact_create_success_text') ?></strong>
                        <?php } ?>

                        <?php if ($this->session->flashdata('contact_update_success_text')) { ?>
                            <strong> <?= lang('contact_update_success_text') ?></strong>
                        <?php } ?>

                        <?php if ($this->session->flashdata('flash_user_id')) { ?>                            &nbsp;
                                <a href="employer_module/edit_organization_contact_person/<?= $this->session->flashdata('flash_user_id')?>">
                                    <?=lang('edit_contact_person_text')?>
                                </a>
                        <?php } ?>

                        <?php if($this->ion_auth->in_group('employer')) { ?>
                            &nbsp;
                            <a href="employer_module/contact_person_list/my">
                                <?=lang('my_contact_person_list_text')?>
                            </a>
                        <?php } ?>

                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?php if ($this->session->flashdata('validation_errors')) { ?>
                            <strong> <?= $this->session->flashdata('validation_errors') ?></strong>
                        <?php } ?>
                    </div>
                <?php } ?>

                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-8">
                        <form role="form" action="<?= base_url().$form_url ?>" method="post" data-parsley-validate novalidate>

                            <?php if ($which_form == 'edit') { ?>
                                <input type="hidden" name="id" value="<?= $id?>">
                            <?php } ?>
                            <div class="form-group row">
                                <label for="first_name"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_first_name_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="first_name" class="form-control"
                                           name="first_name" value="<?= $contact_person?$contact_person->first_name:'' ?>"
                                           id="first_name"
                                           placeholder="<?php echo lang('employer_first_name_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="last_name"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_last_name_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="last_name" value="<?= $contact_person?$contact_person->last_name:'' ?>"
                                           id="last_name" placeholder="<?php echo lang('employer_last_name_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_phone_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="phone" class="form-control" name="phone"
                                           value="<?= $contact_person?$contact_person->phone:'' ?>"
                                           id="phone" placeholder="<?php echo lang('employer_phone_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_email_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" <?php if($which_form == 'edit') { echo ' readonly '; }  ?> required
                                           parsley-type="email" class="form-control" name="email"
                                           value="<?= $contact_person?$contact_person->email:'' ?>"
                                           id="email" placeholder="<?php echo lang('employer_email_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="user_website"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_website_text'); ?>
                                    <span class="text-danger"></span></label>
                                <div class="col-sm-7">
                                    <input type="text" parsley-type="user_website" class="form-control"
                                           name="user_website" value="<?= $contact_person?$contact_person->user_website:'' ?>"
                                           id="user_website" placeholder="<?php echo lang('employer_website_text'); ?>">
                                </div>
                            </div>

                            <div class="form-group text-center m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('file_submit_text'); ?>
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                    <?php echo lang('modal_cancel_text'); ?>
                                </button>
                            </div>
                        </form>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>
<!-- </div> -->
<!-- </div> -->