<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->

<style>
    .select2-results__option {
        text-align: left !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                    <!-- <small><?php echo lang('page_subtitle_add_text') ?></small> -->
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <?php if ($this->session->flashdata('upload_message')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= $this->session->flashdata('upload_message'); ?><?php echo lang('success_number_employee_upload_text') ?></strong>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('employee_insert_with_err')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>** <?= $this->session->flashdata('employee_insert_with_err'); ?>  <?php echo lang('error_number_employee_upload_text') ?></strong>
            <?php foreach ($this->session->flashdata('employee_row_err_no') as $error_data) { ?>
                <?php echo '<br>'; ?>
                <?php print_r(lang('row_number_text')) . print_r($error_data['row']) . print_r(lang('contains_text')) . print_r($error_data['error']); ?>
            <?php } ?>
        </div>
    <?php } ?>
    <?php if (isset($_SESSION['file_ext_err'])) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?php echo lang('excel_file_format_text') ?></strong>
            <?php unset($_SESSION['file_ext_err']); ?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-6" <?php if ($this->ion_auth->in_group('analyst')) { ?> style="display: none" <?php } ?> >
            <div class="card-box">
                <div class="pull-right">
                    <a class="btn btn-danger btn-sm"
                       href="base_demo_excel/thrifter_details.xls"><?php echo lang('click_form_download_text') ?></a>
                    <!--<a class="btn btn-danger btn-sm"
                       href="employer_module/generate_banks_with_ids_pdf"><?php /*echo lang('banks_with_id_text') */?></a>-->
                </div>
                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <form action="employer_module/insert_employee_info" method="post" enctype="multipart/form-data">
                            <fieldset class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('file_input_text') ?></label>
                                <input type="file" required class="form-control" name="excel_file">
                                <label class="text-danger"
                                       for="confirm2"><?php echo lang('employee_email_importance_text') ?></label>
                            </fieldset>
                            <?php if ($this->ion_auth->is_admin()) { ?>
                                <fieldset class="form-group">
                                    <label for="user_employer_id"><?php echo lang('employer_select_text') ?></label>
                                    <select name="user_employer_id" required parsley-type="user_employer_id"
                                            class="form-control select2 employer_select" id="user_employer_id">
                                        <option value="">Select Organization</option>
                                        <?php foreach ($all_info as $row) { ?>
                                            <option value="<?= $row->id ?>"><?= $row->company ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if ($this->session->flashdata('employer_id_err')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"<?php echo lang('employer_name_require_text') ?>></label>
                                    <?php } ?>
                                </fieldset>
                            <?php } ?>
                            <button type="submit"
                                    class="btn btn-primary"><?php echo lang('file_submit_text') ?></button>
                        </form>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
        <div class="col-6">

            <div class="card-box">
                <?php if ($this->session->flashdata('employee_insert')) { ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo lang('success_number_employee_upload_text') ?></strong>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('employee_insert_err_text')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo lang('employee_insert_err_text') ?></strong>
                    </div>
                <?php } ?>
                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_modal_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                        <label for="exampleInputEmail1"><?php echo lang('single_employee_text') ?></label>
                        <a href="#custom-modal" id="auto_click"
                           class="btn btn-block btn-lg btn-primary waves-effect waves-light" data-animation="fadein"
                           data-plugin="custommodal"
                           data-overlaySpeed="200"
                           data-overlayColor="#36404a"><?php echo lang('single_employee_button_text') ?></a>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end col -->


            <!-- Modal -->
            <div id="custom-modal" class="modal-demo">
                <button type="button" class="close" onclick="Custombox.close();">
                    <span>&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="custom-modal-title"><?php echo lang('modal_title_text'); ?></h4>
                <div class="custom-modal-text">
                    <form role="form" action="employer_module/insert_single_employee" method="post"
                          data-parsley-validate novalidate>
                        <?php if ($this->ion_auth->is_admin()) { ?>
                            <div class="form-group row">
                                <label for="user_employer_id"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_select_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <select name="user_employer_id" required style="width: 100%;"
                                            class="form-control select2 myselect" id="user_employer_id">
                                        <option value="">Select Organization</option>
                                        <?php foreach ($all_info as $row) { ?>
                                            <option value="<?= $row->id ?>"><?= $row->company ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label for="first_name"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_first_name_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="first_name" class="form-control"
                                       name="first_name"
                                       id="first_name" placeholder="<?php echo lang('modal_first_name_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_last_name_text'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="last_name"
                                       id="last_name" placeholder="<?php echo lang('modal_last_name_text'); ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_email_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="email" required parsley-type="email" class="form-control" name="email"
                                       id="inputEmail3" placeholder="<?php echo lang('modal_email_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user_ofc_id"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_ofc_id_text'); ?>
                                <span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="user_ofc_id" class="form-control"
                                       name="user_ofc_id"
                                       id="user_ofc_id" placeholder="<?php echo lang('modal_user_ofc_id_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user_salary"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_salary_text'); ?>
                                <span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="user_salary" class="form-control"
                                       name="user_salary"
                                       id="user_salary" placeholder="<?php echo lang('modal_user_salary_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_phone_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="phone" class="form-control" name="phone"
                                       id="phone" placeholder="<?php echo lang('modal_phone_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_gender_text'); ?>
                                <span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <div>
                                        <div class="radio">
                                            <input type="radio" name="user_gender" id="radio1" value="male" required>
                                            <label for="radio1"><?php echo lang('modal_user_gender_male_text'); ?></label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="user_gender" id="radio2" value="female">
                                            <label for="radio2"><?php echo lang('modal_user_gender_female_text'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user_dob"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_age_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required readonly="readonly" parsley-type="user_dob"
                                       class="form-control date_chk" name="user_dob"
                                       id="user_dob" placeholder="<?php echo lang('modal_age_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>

                        <!--here-->

                        <div class="form-group row">
                            <label for="user_country"
                                   class="col-sm-4 form-control-label"><?php echo lang('label_user_country_text'); ?>
                                <span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="user_country" required style="width: 100%;"
                                        class="form-control select2  user_country" id="user_country">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_state"
                                   class="col-sm-4 form-control-label"><?php echo lang('label_user_state_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="user_state" required style="width: 100%;"
                                        class="form-control select2 user_state" id="user_state">

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_city"
                                   class="col-sm-4 form-control-label"><?php echo lang('label_user_city_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="user_city" required style="width: 100%;"
                                        class="form-control select2 user_city" id="user_city">

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_street_1"
                                   class="col-sm-4 form-control-label"><?php echo lang('label_user_street_1_text'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="user_street_1" class="form-control"
                                       name="user_street_1"
                                       id="user_street_1"
                                       placeholder="<?php echo lang('placeholder_user_street_1_text'); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_street_2"
                                   class="col-sm-4 form-control-label"><?php echo lang('label_user_street_2_text'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="user_street_2" class="form-control"
                                       name="user_street_2"
                                       id="user_street_2"
                                       placeholder="<?php echo lang('placeholder_user_street_2_text'); ?>">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="user_hire_date"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_hire_date_text'); ?>
                                <span class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required readonly="readonly" parsley-type="user_hire_date"
                                       class="form-control date_chk" name="user_hire_date"
                                       id="user_hire_date"
                                       placeholder="<?php echo lang('modal_user_hire_date_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="user_bvn"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_bvn_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" required parsley-type="user_bvn" class="form-control" name="user_bvn"
                                       id="user_bvn" placeholder="<?php echo lang('modal_user_bvn_text'); ?>">
                                <?php if ($this->session->flashdata('employer_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_employer_id"
                                   class="col-sm-4 form-control-label"><?php echo lang('bank_select_text'); ?><span
                                        class="text-danger">*</span></label>
                            <div class="col-sm-7">
                                <select name="user_bank" required style="width: 100%;"
                                        class="form-control select2 myselect user_bank" id="">
                                    <option value=""><?= lang('bank_select_text') ?></option>
                                    <?php foreach ($banks as $bank) { ?>
                                        <option value="<?= $bank->bank_id ?>"><?= $bank->bank_name ?></option>
                                    <?php } ?>
                                </select>
                                <?php if ($this->session->flashdata('bank_detail_err')) { ?>
                                    <label class="text-danger"
                                           for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_bank_account_no"
                                   class="col-sm-4 form-control-label"><?php echo lang('modal_user_bank_account_no_text'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" name="user_bank_account_no"
                                       id="user_bank_account_no"
                                       placeholder="<?php echo lang('modal_user_bank_account_no_text'); ?>">
                            </div>
                        </div>

                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <?php echo lang('file_submit_text'); ?>
                            </button>
                            <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                <?php echo lang('modal_cancel_text'); ?>
                            </button>
                        </div>


                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- </div> -->
    <!-- </div> -->

    <?php if ($this->session->flashdata('employer_detail_err')) { ?>
        <script>
            $(document).ready(function () {
                jQuery(function () {
                    jQuery('#auto_click').click();
                });
            });
        </script>
    <?php } ?>


    <script type="text/javascript">
        $(function () {
            $('#user_dob').datepicker({
                format: 'yyyy-mm-dd',
                endDate: '-18y',
                autoclose: true
            });

            $('#user_hire_date').datepicker({
                format: 'yyyy-mm-dd',
                endDate: '-1d',
                autoclose: true
            });

            // ajax: {
            // url: '<?php echo base_url() . 'employer_module/get_employer_list'?>',
            // type: 'GET',
            // success: function (data) {
            //     var get_data = JSON.parse(data)
            //     console.log(get_data);
            //     }
            // }

            $('.employer_select').select2();
            $('.user_bank').select2();
            $('.myselect').select2({
                dropdownParent: $("#custom-modal")
            });


            $('.user_country').select2({
                dropdownParent: $("#custom-modal"),
                allowClear: true,
                placeholder: '<?= lang('placeholder_user_country_text')?>',

                "language": {
                    "noResults": function () {
                        var btn_html = "<?= lang('country_not_found_text')?>";
                        var div = document.createElement("div");
                        div.innerHTML = btn_html;
                        return div;
                    }
                },

                minimumInputLength: 1,
                ajax: {
                    url: '<?= base_url() ?>employer_module/get_countries_by_select2',
                    dataType: 'json',
                    cache: true,
                    delay: 1000,
                    allowClear: true,


                    data: function (params) {

                        return {
                            keyword: params.term, // search term
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {

                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        //console.log(data.items);

                        return {
                            results: data.items,
                            pagination: {
                                more: data.more_pages
                            }
                        };
                    },

                    /*templateResult: formatRepo,
                     templateSelection: formatRepoSelection,*/

                    escapeMarkup: function (markup) {
                        return markup;
                    } // let our custom formatter work


                }

            });

            $('.user_state').select2({
                dropdownParent: $("#custom-modal"),
                allowClear: true,
                placeholder: '<?= lang('placeholder_user_state_text')?>',

                "language": {
                    "noResults": function () {
                        var btn_html = "<?= lang('state_not_found_text')?>";
                        var div = document.createElement("div");
                        div.innerHTML = btn_html;
                        return div;
                    }
                },

                minimumInputLength: 1,
                ajax: {
                    url: '<?= base_url() ?>employer_module/get_states_by_select2',
                    dataType: 'json',
                    cache: true,
                    delay: 1000,
                    allowClear: true,


                    data: function (params) {
                        return {
                            user_country: $('#user_country').val(),
                            keyword: params.term, // search term
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {

                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        //console.log(data.items);

                        return {
                            results: data.items,
                            pagination: {
                                more: data.more_pages
                            }
                        };
                    },

                    /*templateResult: formatRepo,
                     templateSelection: formatRepoSelection,*/

                    escapeMarkup: function (markup) {
                        return markup;
                    } // let our custom formatter work


                }

            });

            $('.user_city').select2({
                dropdownParent: $("#custom-modal"),
                allowClear: true,
                placeholder: '<?= lang('placeholder_user_city_text')?>',

                "language": {
                    "noResults": function () {
                        var btn_html = "<?= lang('city_not_found_text')?>";
                        var div = document.createElement("div");
                        div.innerHTML = btn_html;
                        return div;
                    }
                },

                minimumInputLength: 1,
                ajax: {
                    url: '<?= base_url() ?>employer_module/get_cities_by_select2',
                    dataType: 'json',
                    cache: true,
                    delay: 1000,
                    allowClear: true,


                    data: function (params) {

                        return {
                            user_state: $('#user_state').val(),
                            keyword: params.term, // search term
                            page: params.page,
                        };
                    },
                    processResults: function (data, params) {

                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        //console.log(data.items);

                        return {
                            results: data.items,
                            pagination: {
                                more: data.more_pages
                            }
                        };
                    },

                    /*templateResult: formatRepo,
                     templateSelection: formatRepoSelection,*/

                    escapeMarkup: function (markup) {
                        return markup;
                    } // let our custom formatter work


                }

            });


        });
    </script>

