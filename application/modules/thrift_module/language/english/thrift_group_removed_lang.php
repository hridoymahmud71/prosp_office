<?php

$lang['thrift_group_removed_text'] = 'Thrift group is removed';

$lang['thrift_group_removed_subject_text'] = 'A thrift group is removed';
$lang['thrift_group_id_text'] = 'Thrift Group Id';

$lang['reason_text'] = 'Reason';
$lang['unknown_text'] = 'Unknown';
$lang['creator_deleted_text'] = 'Creator deleted the group';
$lang['no_member_joined_text'] = 'No member joined in due time';
$lang['promised_loan_insufficient_text'] = 'Promised Loan was insufficient';


