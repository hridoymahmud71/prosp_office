<?php


$lang['page_title_add_text'] = 'New Loan Request';
$lang['page_title_edit_text'] = 'Edit Loan Request';
$lang['page_title_view_text'] = 'Loan Requests';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Loan Thrifting';


$lang['box_title_add_text'] = 'ENTER LOAN DETAILS';
$lang['box_title_edit_text'] = 'Edit Details';
$lang['box_title_view_text'] = 'Thrift Information';

/*invitation*/
$lang['invitation_card_title_text'] = 'Loan Thrift Invitation';
$lang['invitation_card_invited_by_text'] = 'Invited By';
$lang['invitation_card_status_text'] = 'Status';


$lang['invitation_card_description_text'] = 'You are invited to join this thrift';
$lang['invitation_card_accept_text'] = 'Accept';
$lang['invitation_card_decline_text'] = 'Decline';


/*member table*/
$lang['member_box_title_text'] = 'Invited Members';
$lang['members_text'] = 'Members';
$lang['status_text'] = 'Status';
$lang['order_text'] = 'Order';

$lang['title_move_up_order_text'] = 'Move Up';
$lang['title_move_down_order_text'] = 'Move Down';
$lang['title_remove_text'] = 'Remove';

//form
$lang['select_colleagues_text'] = 'Registered members only: Enter colleague names or email addresses';
$lang['no_colleaguse_found_text'] = 'No colleague/member is found';

//radio
$lang['include_external_members_radio_label_text'] = 'Include non-registered individuals?';
$lang['include_external_members_radio_yes_text'] = 'Yes';
$lang['include_external_members_radio_no_text'] = 'No';

$lang['select_external_members_text'] = 'Non-registered members only: enter email addresses';

$lang['thrift_amount_text'] = 'Loan Amount';


$lang['placeholder_interest_rate_text'] = 'Enter a proposed loan interest rate';
$lang['placeholder_repayments_terms_text'] = 'Enter your proposed number of monthly payments (months)';

$lang['set_start_date_text'] = 'Set Loan Start Date (this is also the payment deduction date)';
$lang['interest_rate_text'] = 'Interest rate';
$lang['repayment_terms_text'] = 'Loan Repayment Terms';



//chk
$lang['start_thrift_text'] = 'Start Thrift';
$lang['start_thrift_long_text'] = 'Start a thrift with this loan product';



$lang['submit_btn_add_text'] = 'Add';
$lang['submit_btn_create_thrift_text'] = 'Request Loan';
$lang['submit_btn_update_text'] = 'Update';
//validation

$lang['atleast_need_one_colleague_text'] = 'At least one colleague/member need to be selected';
$lang['could_not_be_added_text'] = '%s could not be added';


$lang['initiator_text'] = 'Initiator';
$lang['accepted_text'] = 'Accepted';
$lang['pending_text'] = 'Pending';
$lang['declined_text'] = 'Declined';

//flash
$lang['successful_text'] = 'Successful!';
$lang['unsuccessful_text'] = 'Unsuccessful!';

$lang['view_loan_product_thrift_text'] = 'See here';

$lang['loan_product_create_success'] = 'Successfully created';
$lang['loan_product_update_success'] = 'Successfully updated';

$lang['flash_thrift_percentage_error_text'] = 'Your monthly salary must be greater than %s%% of the monthly contribution amount to join this thrift';

$lang['thrift_error_no_employer_text'] = 'You do not belong to any organization';

/*promised amount*/
$lang['promise_amount_label_text'] = 'How much do you want to lend?';
$lang['promise_amount_placeholder_text'] = 'Enter amount';
$lang['promise_amount_submit_btn_text'] = 'Set';

$lang['loan_promise_add_success_text'] = 'Successfully set promised amount';
$lang['loan_promise_update_success_text'] = 'Successfully set promised amount';

//-------------------------------
$lang['promised_amount_text'] = 'Promised Amount';
$lang['unavailable_text'] = 'Unavailable';












