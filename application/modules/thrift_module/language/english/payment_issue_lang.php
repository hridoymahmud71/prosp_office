<?php


$lang['page_title_all_issue_details_page_text'] = 'All Issue Details page';
$lang['page_title_employer_issue_details_page_text'] = 'Organization\'s Issue Details page';
$lang['page_title_employee_issue_details_page_text'] = 'Thrifter\'s Issue Details page';

$lang['breadcrum_issue_text'] = 'Issue';
$lang['breadcrum_issue_details_text'] = 'Issue Details';

$lang['all_payment_text'] = 'All Payments';
$lang['my_payment_text'] = 'My Payments';

/*payments col*/
$lang['email_text'] = 'Email';
$lang['status_actions_text'] = 'Status/Actions';

$lang['thrift_group_number_text'] = 'Thrift Group ID';
$lang['thrift_group_employer_text'] = 'Org';
$lang['thrift_group_payment_amount_text'] = 'Amount';
$lang['thrift_group_payer_member_text'] = 'Payer';
$lang['thrift_group_payment_number_text'] = 'Payment ID';
$lang['thrift_group_payee_member_text'] = 'Payee';
$lang['thrift_group_payment_date_text'] = 'Date';
$lang['thrift_group_start_date_text'] = 'Thrift Start';
$lang['thrift_group_end_date_text'] = 'Thrift End';

$lang['thrift_group_thrifter_id_text'] = 'Thrifter ID';
$lang['thrift_group_payer_or_payee_text'] = 'Payer/Payee';
$lang['total_text'] = 'Total';

$lang['pay_text'] = 'Pay';

$lang['thrift_details_text'] = 'Thrift Details';
$lang['member_details_text'] = 'Member Details';
$lang['payment_details_text'] = 'Payment Details';
$lang['payment_recieve_details_text'] = 'Payment Disbursement';

$lang['yes_text'] = 'Yes';
$lang['no_text'] = 'No';

$lang['unavailable_text'] = 'Unavailable';
$lang['choose_range_text'] = 'Choose Range';

$lang['payment_pdf_text'] = 'PAYMENT PDF';
$lang['payment_with_disbursement_pdf_text'] = 'PAYMENT/DISBURSMENT PDF';

$lang['payment_excel_text'] = 'PAYMENT EXCEL';
$lang['payment_with_disbursement_excel_text'] = 'PAYMENT/DISBURSMENT EXCEL';

$lang['other_issues_text'] = 'OTHER ISSUES';

$lang['payment_grouped_pdf_text'] = 'PAYMENT BY GROUP PDF';
$lang['payment_grouped_excel_text'] = 'PAYMENT BY GROUP EXCEL';

/*status*/
$lang['column_status_text'] = 'Status';
$lang['all_text'] = 'All';
$lang['scheduled_text'] = 'Scheduled';
$lang['waiting_text'] = 'Waiting';
$lang['charge_unsuccessful_text'] = 'Charge Unsuccessful';
$lang['solved_text'] = 'Solved';
$lang['charge_successful_text'] = 'Charge Successful';
$lang['complete_text'] = 'Complete';

/*tooltip*/

$lang['tooltip_view_text'] = 'View Thrift';
$lang['tooltip_solve_text'] = 'Solve';
$lang['tooltip_unsolve_text'] = 'Unsolve';
$lang['tooltip_pay_text'] = 'Pay';

/*flash*/
$lang['solve_success_text'] = 'Payment Successful';
$lang['unsolve_success_text'] = 'Payment Unsuccessful';

/**/
$lang['loading_text'] = 'Loding...';
$lang['not_found_text'] = 'Not found';
$lang['no_matching_found_text'] = 'No Matching Result Found';




