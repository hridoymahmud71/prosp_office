<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

//test
$route['thrift_module/send_test_email'] = 'ThriftController/testSendEmail';

//by cron job
$route['thrift_module/auto_assign_bot_admins'] = 'ThriftController/autoAssignBotAdmins';

$route['thrift_module/auto_thrift_group_payment'] = 'ThriftController/autoThriftGroupPayment';
$route['thrift_module/auto_thrift_group_payment_recieve'] = 'ThriftController/autoThriftGroupPaymentRecieve';

$route['thrift_module/auto_thrift_group_payment_retry'] = 'ThriftController/autoThriftGroupPaymentRetry';
$route['thrift_module/auto_thrift_group_payment_recieve_retry'] = 'ThriftController/autoThriftGroupPaymentRecieveRetry';

$route['thrift_module/auto_start_custom_thrift_group'] = 'ThriftController/autoStartCustomThriftGroup';

//sequentially
$route['thrift_module/auto_start_loan_thrift_group'] = 'ThriftController/autoStartLoanThriftGroup';
$route['thrift_module/auto_charge_lenders_from_loan_thrift_group'] = 'ThriftController/autoChargeLendersFromLoanThriftGroup';
$route['thrift_module/auto_start_loan_thrifting'] = 'ThriftController/autoStartLoanThrifting';

$route['thrift_module/send_reminder_to_thrifters'] = 'ThriftController/sendReminderToThrifters';
$route['thrift_module/notify_payment_declined'] = 'ThriftController/notifyPaymentDeclined';
$route['thrift_module/notify_payment_recieve_declined'] = 'ThriftController/notifyPaymentRecieveDeclined';
$route['thrift_module/auto_solve_paystack_payments'] = 'ThriftController/autoSolvePaystackPayments';

//webhook
$route['thrift_module/get_paystack_events_by_webhook'] = 'ThriftController/getPaystackEventsByWebhook';
$route['thrift_module/submit_payment_method_choice'] = 'ThriftController/submitPaymentMethodChoice';

$route['thrift_module/start_thrift/(:any)'] = 'ThriftController/startThrift';
$route['thrift_module/show_thrifts/all'] = 'ThriftController/showThrifts';   // for admin
$route['thrift_module/show_thrifts/my'] = 'ThriftController/showThrifts';    // employees see their own thrifts
$route['thrift_module/show_thrifts/my_employees'] = 'ThriftController/showThrifts';    // employers see their employees thrifts

$route['thrift_module/get_thrifts_by_ajax'] = 'ThriftController/getThriftsByAjax';
$route['thrift_module/view_thrift/(:any)'] = 'ThriftController/viewThrift';

// custom product
$route['thrift_module/add_custom_product'] = 'ThriftController/addCustomProduct';
$route['thrift_module/get_colleagues_by_select2'] = 'ThriftController/getColleaguesBySelect2';

$route['thrift_module/custom_product_list/all_created'] = 'ThriftController/getCustomProductCreationList';
$route['thrift_module/custom_product_list/all_recieved'] = 'ThriftController/getCustomProductRecievedList';

$route['thrift_module/custom_product_list/created'] = 'ThriftController/getCustomProductCreationList';
$route['thrift_module/custom_product_list/recieved'] = 'ThriftController/getCustomProductRecievedList';

$route['thrift_module/custom_product_creation_list_by_ajax'] = 'ThriftController/getCustomProductCreationListByAjax';
$route['thrift_module/custom_product_recieved_list_by_ajax'] = 'ThriftController/getCustomProductRecievedListByAjax';

$route['thrift_module/custom_product_thrift/edit/(:any)'] = 'ThriftController/EditOrViewCustomProduct';
$route['thrift_module/custom_product_thrift/view/(:any)'] = 'ThriftController/EditOrViewCustomProduct';

$route['thrift_module/accept_custom_product/(:any)'] = 'ThriftController/AcceptCustomProduct';
$route['thrift_module/decline_custom_product/(:any)'] = 'ThriftController/DeclineCustomProduct';
$route['thrift_module/delete_custom_product/(:any)'] = 'ThriftController/DeleteCustomProduct';

$route['thrift_module/custom_thrift_member_remove/(:any)'] = 'ThriftController/customThriftMemberRemove';
$route['thrift_module/custom_thrift_member_order_up/(:any)'] = 'ThriftController/customThriftMemberOrderUp';
$route['thrift_module/custom_thrift_member_order_down/(:any)'] = 'ThriftController/customThriftMemberOrderDown';
$route['thrift_module/super_edit_custom_thrift'] = 'ThriftController/superEditCustomThrift';
$route['thrift_module/super_edit_custom_thrift_details'] = 'ThriftController/superEditCustomThriftDetails';

//individual product
$route['thrift_module/add_individual_product'] = 'ThriftController/addIndividualProduct';

//loan product
$route['thrift_module/add_loan_product'] = 'ThriftController/addLoanProduct';

$route['thrift_module/loan_product_list/all_created'] = 'ThriftController/getLoanProductCreationList';
$route['thrift_module/loan_product_list/all_recieved'] = 'ThriftController/getLoanProductRecievedList';

$route['thrift_module/loan_product_list/created'] = 'ThriftController/getLoanProductCreationList';
$route['thrift_module/loan_product_list/recieved'] = 'ThriftController/getLoanProductRecievedList';

$route['thrift_module/loan_product_creation_list_by_ajax'] = 'ThriftController/getLoanProductCreationListByAjax';
$route['thrift_module/loan_product_recieved_list_by_ajax'] = 'ThriftController/getLoanProductRecievedListByAjax';

$route['thrift_module/loan_product_thrift/edit/(:any)'] = 'ThriftController/EditOrViewLoanProduct';
$route['thrift_module/loan_product_thrift/view/(:any)'] = 'ThriftController/EditOrViewLoanProduct';

$route['thrift_module/accept_loan_product/(:any)'] = 'ThriftController/AcceptLoanProduct';
$route['thrift_module/decline_loan_product/(:any)'] = 'ThriftController/DeclineLoanProduct';
$route['thrift_module/delete_loan_product/(:any)'] = 'ThriftController/DeleteLoanProduct';

$route['thrift_module/loan_thrift_member_remove/(:any)'] = 'ThriftController/loanThriftMemberRemove';

$route['thrift_module/super_edit_loan_thrift'] = 'ThriftController/superEditLoanThrift';
$route['thrift_module/super_edit_loan_thrift_details'] = 'ThriftController/superEditLoanThriftDetails';

$route['thrift_module/set_loan_promise'] = 'ThriftController/setLoanPromise';


//payment issues
$route['thrift_module/show_issue/all_issue/payment_issue/all'] = 'ThriftController/showIssue';
$route['thrift_module/show_issue/employee_issue_as_employee/payment_issue/all'] = 'ThriftController/showIssue';
$route['thrift_module/show_issue/employee_issue/payment_issue/all/(:any)'] = 'ThriftController/showIssue';

$route['thrift_module/show_issue/all_issue/payment_recieve_issue/all'] = 'ThriftController/showIssue';
$route['thrift_module/show_issue/employee_issue_as_employee/payment_recieve_issue/all'] = 'ThriftController/showIssue';
$route['thrift_module/show_issue/employee_issue/payment_recieve_issue/all/(:any)'] = 'ThriftController/showIssue';

/*ajax*/
$route['thrift_module/get_payment_issue_by_ajax'] = 'ThriftController/getPaymentIssueByAjax';
$route['thrift_module/get_payment_recieve_issue_by_ajax'] = 'ThriftController/getPaymentRecieveIssueByAjax';

/*solution*/
//any = payment_id
$route['thrift_module/solve_payment/(:any)'] = 'ThriftController/solvePayment';
$route['thrift_module/unsolve_payment/(:any)'] = 'ThriftController/unsolvePayment';

$route['thrift_module/solve_payment_recieve/(:any)'] = 'ThriftController/solvePaymentRecieve';
$route['thrift_module/unsolve_payment_recieve/(:any)'] = 'ThriftController/unsolvePaymentRecieve';

$route['thrift_module/pay/by_paystack/(:any)'] = 'ThriftController/makePayment';
$route['thrift_module/transfer/by_paystack/(:any)'] = 'ThriftController/makeTransfer';
