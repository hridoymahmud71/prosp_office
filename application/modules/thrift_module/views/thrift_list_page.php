
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php if ($which_list == 'all') { ?>
                                <small><?php echo lang('page_title_all_thrift_text') ?></small>
                            <?php } ?>

                            <?php if ($which_list == 'my') { ?>
                                <small><?php echo lang('page_title_my_thrift_text') ?></small>
                            <?php } ?>

                            <?php if ($which_list == 'my_employees') { ?>
                                <small><?php echo lang('page_title_my_employees_thrift_text') ?></small>
                            <?php } ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a
                                        href="#"><?php echo lang('breadcrumb_home_text') ?></a>
                            </li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">

                            </h4>

                            <?php if ($this->session->flashdata('thrift_success')) { ?>
                                <section class="content mt-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="alert-heading"><?php echo lang('successfull_text') ?></h4>
                                                <p>
                                                    <?php
                                                    if ($this->session->flashdata('thrift_success_text')) {
                                                        echo lang('thrift_success_text');
                                                    }

                                                    if ($this->session->flashdata('thrift_create_success_text')) {
                                                        $this->session->flashdata('thrift_create_success_text');
                                                    }

                                                    if ($this->session->flashdata('loan_create_success_text')) {
                                                        echo lang('loan_create_success_text');
                                                    }
                                                    ?>
                                                </p>
                                                <?php if ($this->session->flashdata('flash_thrift_group_id')) { ?>
                                                    <p class="mb-0"> &nbsp;
                                                        <a href="<?php echo base_url() . 'thrift_module/view_thrift/' . $this->session->flashdata('flash_thrift_group_id') ?>">
                                                            <?php echo lang('see_thrift_group_text'); ?>
                                                        </a>
                                                    </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            <?php } ?>

                            <?php if ($this->session->flashdata('thrift_error')) { ?>
                                <section class="content mt-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="alert-heading"><?php echo lang('thrifting_error_text') ?></h4>

                                                <?php /*if ($this->session->flashdata('thrift_error_exceed_limit_text')) { */?><!--
                                                    <p>
                                                        <?php /*echo lang('thrift_error_exceed_limit_text') */?>
                                                    </p>
                                                --><?php /*} */?>

                                                <?php if ($this->session->flashdata('flash_thrift_percentage_error')) { ?>
                                                    <p>
                                                        <?php echo sprintf($this->lang->line('flash_thrift_percentage_error_text'),$this->session->flashdata('flash_thrift_percentage_error'));  ?>
                                                    </p>
                                                <?php } ?>

                                                <?php if ($this->session->flashdata('thrift_error_no_employer_text')) { ?>
                                                    <p>
                                                        <?php echo lang('thrift_error_no_employer_text') ?>
                                                    </p>
                                                <?php } ?>

                                                <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                                                    <p>
                                                        <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                                                    </p>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            <?php } ?>

                            <!-- Main content -->
                            <section class="">
                                <div class="">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <h3 class="m-t-0 header-title"><?php echo lang('table_title_text') ?></h3>

                                                <div>
                                                    <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1"
                                                           cellpadding="3"
                                                           border="0">
                                                        <tbody>
                                                        <tr id="filter_col0" data-column="0">
                                                            <td align="center">
                                                                <label><?php echo lang('column_group_number_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col0_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col1" data-column="1">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_product_name_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col1_filter" type="hidden">
                                                                <select id="custom_product_filter"
                                                                        class="form-control select2 custom_product_filter">
                                                                    <option value="all"><?php echo lang('option_all_product_text') ?></option>

                                                                    <?php if ($products) { ?>
                                                                        <?php foreach ($products as $a_product) { ?>
                                                                            <option value="<?= $a_product->product_id ?>"><?= $a_product->product_name ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>

                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col3" data-column="3">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_completion_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col3_filter" type="hidden">
                                                                <select id="custom_completion_filter"
                                                                        class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_complete_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_incomplete_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col4" data-column="4">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_member_status_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col4_filter" type="hidden">
                                                                <select id="custom_member_status_filter"
                                                                        class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_member_full_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_member_not_full_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col5" data-column="5">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_status_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col5_filter" type="hidden">
                                                                <select id="custom_status_filter" class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_active_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_inactive_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col6" data-column="6">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_open_staus_text') ?></label>
                                                            </td>
                                                            <td align="center">
                                                                <input class="column_filter form-control"
                                                                       id="col6_filter" type="hidden">
                                                                <select id="custom_open_status_filter"
                                                                        class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="yes"><?php echo lang('option_open_text') ?></option>
                                                                    <option value="no"><?php echo lang('option_close_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="thrift-table"
                                                       class="table table-bordered table-hover table-responsive ">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_group_number_text') ?></th>
                                                        <th><?php echo lang('column_product_name_text') ?></th>
                                                        <th><?php echo lang('column_contribution_amount_text') ?></th>
                                                        <th><?php echo lang('column_completion_text') ?></th>
                                                        <th><?php echo lang('column_member_status_text') ?></th>
                                                        <th><?php echo lang('column_status_text') ?></th>
                                                        <th><?php echo lang('column_open_staus_text') ?></th>
                                                        <th><?php echo lang('column_creation_date_text') ?></th>
                                                        <th><?php echo lang('column_start_date_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->



<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<!--this css style is holding datatable inside the box-->
<style>
    #thrift-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #thrift-table td,
    #thrift-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(document).ready(function () {

        var loading_image_src = '<?php echo base_url() ?>' + 'base_demo_images/loading.gif';
        var loading_image = '<img src="' + loading_image_src + ' ">';
        var loading_span = '<span><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></span> ';
        var loading_text = "<div style='font-size:larger' ><?php echo lang('loading_text')?></div>";


        $('#thrift-table').DataTable({

            processing: true,
            serverSide: true,
            paging: true,
            pagingType: "full_numbers",
            lengthChange: true,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            searchDelay: 3000,
            infoEmpty: '<?php echo lang("no_thrift_found_text")?>',
            zeroRecords: '<?php echo lang("no_matching_thrift_found_text")?>',
            language: {
                processing: loading_image + '<br>' + loading_text
            },

            columns: [
                {data: "thrift_group_number"},          //0
                {
                    data: {
                        _: "prod.html",
                        sort: "prod.int"
                    }
                },                                      //1
                {
                    data: {
                        _: "cont.html",
                        sort: "cont.dec"
                    }
                },                                      //2
                {
                    data: {
                        _: "comp.html",
                        sort: "comp.int"
                    }
                },                                      //3
                {
                    data: {
                        _: "mem.html",
                        sort: "mem.int"
                    }
                },                                      //4
                {
                    data: {
                        _: "act.html",
                        sort: "act.int"
                    }
                },                                      //5
                {
                    data: {
                        _: "opn.html",
                        sort: "opn.int"
                    }
                },                                      //6
                {
                    data: {
                        _: "cr_dt.display",
                        sort: "cr_dt.timestamp"
                    }
                },                                      //7
                {
                    data: {
                        _: "st_dt.display",
                        sort: "st_dt.timestamp"
                    }
                },                                      //8
                {data: "action"}                        //9

            ],

            columnDefs: [

                {
                    'targets': 0,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },
                {
                    'targets': 1,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {
                    'targets': 2,
                    'createdCell': function (td, cellData, rowData, row, col) {
                        $(td).attr('title', cellData);
                    }
                },

                {orderable: false, targets: [1, 9]} , { visible: false, targets: [7] }
            ],

            aaSorting: [[7, 'desc']],

            ajax: {
                url: "<?php echo base_url() . 'thrift_module/get_thrifts_by_ajax' ?>",                   // json datasource
                data: {
                    which_list: '<?= $which_list ?>'
                },
                type: "post",
                complete: function (res) {
                    getConfirm();
                }

                //open succes only for test purpuses . remember when success is uncommented datble doesn't diplay data
                /*success: function (res) {

                 console.log(res.last_query);
                 console.log(res.common_filter_value);
                 console.log(res.specific_filters);
                 console.log(res.order_column);
                 console.log(res.order_by);
                 console.log(res.limit_start);
                 console.log(res.limit_length);
                 }*/
            }

        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        //customized delay_func starts
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        //customized delay_func ends

        $('input.column_filter').on('keyup', function () {
            var var_this = $(this);
            delay(function () {
                filterColumn($(var_this).parents('tr').attr('data-column'));
            }, 3000);
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#thrift-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {

        /*$('.custom_product_filter').select2({placeholder: "prod"});


         $('.custom_product_filter').on("select2:select", function (e) {
         e.preventDefault();

         var prod_id = $(e.currentTarget).find("option:selected").val();

         $('#col3_filter').val(prod_id);
         filterColumn(1);

         });*/


        //select2 is not working


        $('#custom_product_filter').on('change', function () {

            $('#col1_filter').val($('#custom_product_filter').val());
            filterColumn(1);

        });


        /*-----------------------------*/


        $('#custom_completion_filter').on('change', function () {

            if ($('#custom_completion_filter').val() == 'all') {
                $('#col3_filter').val('');
                filterColumn(3);
            } else {
                $('#col3_filter').val($('#custom_completion_filter').val());
                filterColumn(3);
            }

        });

        $('#custom_member_status_filter').on('change', function () {

            if ($('#custom_member_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_member_status_filter').val());
                filterColumn(4);
            }

        });

        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col5_filter').val('');
                filterColumn(5);
            } else {
                $('#col5_filter').val($('#custom_status_filter').val());
                filterColumn(5);
            }

        });

        $('#custom_open_status_filter').on('change', function () {

            if ($('#custom_open_status_filter').val() == 'all') {
                $('#col6_filter').val('');
                filterColumn(6);
            } else {
                $('#col6_filter').val($('#custom_open_status_filter').val());
                filterColumn(6);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    function getConfirm() {
        $('.confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });
    }

</script>


