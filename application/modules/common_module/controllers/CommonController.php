<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        // $this->load->library('form_validation');
        // $this->form_validation->CI =& $this;


        $this->load->model('Common_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');

        $this->load->library('custom_datetime_library');

        // applicationlibrary/custom_image_library*
        $this->load->library('custom_image_library');

        //$user_data = $this->ion_auth->user()->row_array();

        $user_data = $this->getDetailedUserData_asArray();
        $this->session->set_userdata($user_data);

        $image_directory = $this->getImageDirectory();
        $this->session->set_userdata('image_directory', $image_directory);

    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');

        $this->lang->load('dashboard');

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }


        if ($this->ion_auth->in_group('trustee')) {
            $data['is_trustee'] = 'trustee';
        } else {
            $data['is_trustee'] = 'not_trustee';
        }

        if ($this->ion_auth->in_group('employer')) {
            $data['is_employer'] = 'employer';
        } else {
            $data['is_employer'] = 'not_employer';
        }

        if ($this->ion_auth->in_group('employee')) {
            $data['is_employee'] = 'employee';
        } else {
            $data['is_employee'] = 'not_employee';
        }

        /*------------------------------------------------------------------------------------------------------------*/
        $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();
        $curr_year = $this->custom_datetime_library->convert_and_return_TimestampToYearGivenFormat($curr_ts, 'Y');
        $only_product_name_for_json = array();

        $months_this_year = array();

        for ($i = 1; $i <= 12; $i++) {

            $leading_zero = '';
            if ($i < 10) {
                $leading_zero = '0';
            }
            $arr['m'] = $curr_year . '-' . $leading_zero . $i;
            $months_this_year[] = $arr['m'];

        }

        $mp_arr = array();

        foreach ($months_this_year as $mty) {
            $ts = $this->custom_datetime_library->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($mty,'Y-m');

            $cnp_max_min_ts_and_strings = $this->cnp_max_min_ts_and_strings($ts);
            $one_month_product_count = $this->Common_model->getMonthWiseProductCount($cnp_max_min_ts_and_strings['curr_ym_min_ts'], $cnp_max_min_ts_and_strings['curr_ym_max_ts']);

            $mp_arr[$mty] = $one_month_product_count;
        }

        $product_arr = $this->Common_model->getProductsArray();
        $data['randomcolor'] = json_encode(['#ff664f','#274744','#4bb091','#b2ae85','#f02f53','#fdd6ff','#9fd5e5','#80e5c4','#49cad4','#6e9cd0']);
        if($product_arr){
            foreach ($product_arr as $p) {

                $colors = array();
                for ($ci = 1000000; $ci <= 16777215; $ci += 100000) {

                    if (count($colors) < count($product_arr)) {
                        $gen_color = (string)dechex($ci);
                        $gap = 6 - (int)strlen($gen_color);

                        $zeroes = '';
                        if ($gap > 0) {
                            for ($g = 1; $g <= $gap; $g++) {
                                $zeroes .= '0';
                            }
                        }

                        $gen_color = '#' . $zeroes . $gen_color;
                        $colors[] = $gen_color;
                    }

                }
                //shuffle($colors);
                $only_product_name_for_json[] = $p['product_name'];

            }

            $randomcolor = json_encode($colors);
            $data['randomcolor'] = $randomcolor;
        }

        $data['productsgraph'] = json_encode(['nothing']);
        $data['product_names'] = json_encode(['nothing']);

        $all_m_data = array();
        foreach ($months_this_year as $month) {
            $m_data = array();
            $m_data['m'] = $month;

            if($mp_arr[$month]){
                foreach ($mp_arr[$month] as $products_in_a_month) {
                    $m_data[$products_in_a_month['product_name']] =  $products_in_a_month['total_product'];
                }
            }

            $all_m_data[] =  $m_data;

        }

        $data['productsgraph'] = json_encode($all_m_data);
        $data['product_names'] = json_encode($only_product_name_for_json);
        /*-------------------------------------------------------------------------------------------------------------*/

        $org_value['all_org_data'] = $this->Common_model->get_all_org_data();

        $query_date = date('Y-m-d');
        $start_date = date('Y-m-01', strtotime($query_date));
        $last_date = date('Y-m-t', strtotime($query_date));

        $emp_id = array();
        $new_emp_name = array();
        $get_emp_name = array();
        $match_employee_id = array();
        $specific_id_name = array();
        $final_data = array();

        foreach ($org_value['all_org_data'] as $key => $row) {
            $get_emp_name[] = $this->Common_model->get_name($row['id']);
            $specific_id_name[] = $get_emp_name[$key];
            $emp_id[] = $this->Common_model->countThrifts($row['id'], $start_date, $last_date);
        }

        foreach ($specific_id_name as $key => $value) {
            $match_employee_id[] = $value[0];
        }
        foreach ($match_employee_id as $key => $n_value) {
            $new_emp_name[] = $n_value['company'];
        }

        $data['org_name'] = $new_emp_name;


        $i = 0;
        $final_pie_chart_val = array();
        foreach ($emp_id as $value) {
            $final_pie_chart_val[] = array(
                'label' => $new_emp_name[$i],
                'value' => $value,
            );
            $i++;
        }
        $data['final_pie_chart_val'] = json_encode($final_pie_chart_val);

        /*------------------------------------------------------------------------------------------------------------*/


        $curr_month = date('Y-m-d') . ' first day of last month';

        $dt = date_create($curr_month);
        $last_month = $dt->format('Y-m');
        $data['last_month_payment'] = $this->Common_model->get_last_month_payment($last_month);

        $data['this_month_payment'] = $this->Common_model->get_this_month_payment();

        $curr_month = date('Y-m-d') . ' first day of next month';
        $dt = date_create($curr_month);
        $next_month = $dt->format('Y-m');
        $data['next_month_payment'] = $this->Common_model->get_next_month_payment($next_month);

        $data['total_paid'] = $this->Common_model->get_paid_amount();

        /*-------------------------*/

        /*------------------------------------------------------------------------------------------------------------*/


        $data['ACTIVE_RUNNING_THRIFTS'] = $this->Common_model->get_all_ACTIVE_RUNNING_THRIFTS($min_ts = false, $max_ts = false);
        $data['ACTIVE_THRIFTS_VOLUME'] = $this->Common_model->get_all_ACTIVE_THRIFTS_VOLUME($min_ts = false, $max_ts = false);
        $data['AVERAGE_THRIFTS_VOLUME'] = $this->Common_model->get_all_AVERAGE_THRIFTS_VOLUME($min_ts = false, $max_ts = false);
        $data['MEMBERS_IN_ACTIVE_THRIFT'] = $this->Common_model->get_all_MEMBERS_IN_ACTIVE_THRIFT($min_ts = false, $max_ts = false);

        //$only_active = true, $include-deleted = false
        $data['non_deleted_active_thrifter_count'] = $this->Common_model->countThrifters(true,false);

        $data['currency'] = $this->custom_settings_library->getASettingsValue('currency_settings', 'currency_sign');

        /*increment decrement calculation starts*/
        $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();

        $cnp_max_min_ts_and_strings = $this->cnp_max_min_ts_and_strings($curr_ts);

        /*-------------------------*/

        $active_running_thrifts_cm =
            $this
                ->Common_model
                ->get_all_ACTIVE_RUNNING_THRIFTS($cnp_max_min_ts_and_strings['curr_ym_min_ts'], $cnp_max_min_ts_and_strings['curr_ym_max_ts']);

        $active_running_thrifts_lm =
            $this
                ->Common_model
                ->get_all_ACTIVE_RUNNING_THRIFTS($cnp_max_min_ts_and_strings['prev_ym_min_ts'], $cnp_max_min_ts_and_strings['prev_ym_max_ts']);

        if (!$active_running_thrifts_cm) {
            $active_running_thrifts_cm = 0;
        }

        if (!$active_running_thrifts_lm) {
            $active_running_thrifts_lm = 0;
        }

        if ($active_running_thrifts_lm == 0) {
            $active_running_thrift_change = 100;
        } else {
            $active_running_thrift_change = (($active_running_thrifts_cm - $active_running_thrifts_lm) / $active_running_thrifts_lm) * 100;

        }


        $data['active_running_thrift_change'] = $active_running_thrift_change;

        /*-------------------------*/

        $active_running_thrifts_vol_cm =
            $this
                ->Common_model
                ->get_all_ACTIVE_THRIFTS_VOLUME($cnp_max_min_ts_and_strings['curr_ym_min_ts'], $cnp_max_min_ts_and_strings['curr_ym_max_ts']);

        $active_running_thrifts_vol_lm =
            $this
                ->Common_model
                ->get_all_ACTIVE_THRIFTS_VOLUME($cnp_max_min_ts_and_strings['prev_ym_min_ts'], $cnp_max_min_ts_and_strings['prev_ym_max_ts']);


        if (!$active_running_thrifts_vol_cm) {
            $active_running_thrifts_vol_cm = 0.00;
        }

        if (!$active_running_thrifts_vol_lm) {
            $active_running_thrifts_vol_lm = 0.00;
        }

        if ($active_running_thrifts_vol_lm == 0) {
            $active_running_thrift_vol_change = 100;
        } else {
            $active_running_thrift_vol_change = (($active_running_thrifts_vol_cm - $active_running_thrifts_vol_lm) / $active_running_thrifts_vol_lm) * 100;
        }

        $data['active_running_thrift_vol_change'] = $active_running_thrift_vol_change;

        /*-------------------------*/

        $average_running_thrifts_vol_cm =
            $this
                ->Common_model
                ->get_all_AVERAGE_THRIFTS_VOLUME($cnp_max_min_ts_and_strings['curr_ym_min_ts'], $cnp_max_min_ts_and_strings['curr_ym_max_ts']);

        $average_running_thrifts_vol_lm =
            $this
                ->Common_model
                ->get_all_AVERAGE_THRIFTS_VOLUME($cnp_max_min_ts_and_strings['prev_ym_min_ts'], $cnp_max_min_ts_and_strings['prev_ym_max_ts']);


        if (!$average_running_thrifts_vol_cm) {
            $average_running_thrifts_vol_cm = 0.00;
        }

        if (!$average_running_thrifts_vol_lm) {
            $average_running_thrifts_vol_lm = 0.00;
        }

        if ($average_running_thrifts_vol_lm == 0) {
            $average_running_thrift_vol_change = 100;
        } else {
            $average_running_thrift_vol_change = (($average_running_thrifts_vol_cm - $average_running_thrifts_vol_lm) / $average_running_thrifts_vol_lm) * 100;
        }


        $data['average_running_thrift_vol_change'] = $average_running_thrift_vol_change;

        /*-------------------------*/

        $members_in_running_thrifts_cm =
            $this
                ->Common_model
                ->get_all_MEMBERS_IN_ACTIVE_THRIFT($cnp_max_min_ts_and_strings['curr_ym_min_ts'], $cnp_max_min_ts_and_strings['curr_ym_max_ts']);

        $members_in_running_thrifts_lm =
            $this
                ->Common_model
                ->get_all_MEMBERS_IN_ACTIVE_THRIFT($cnp_max_min_ts_and_strings['prev_ym_min_ts'], $cnp_max_min_ts_and_strings['prev_ym_max_ts']);

        if (!$members_in_running_thrifts_cm) {
            $members_in_running_thrifts_cm = 0;
        }

        if (!$members_in_running_thrifts_lm) {
            $members_in_running_thrifts_lm = 0;
        }

        if ($members_in_running_thrifts_lm == 0) {
            $members_in_running_thrift_change = 100;
        } else {
            $members_in_running_thrift_change = (($members_in_running_thrifts_cm - $members_in_running_thrifts_lm) / $members_in_running_thrifts_lm) * 100;
        }

        $data['members_in_running_thrift_change'] = $members_in_running_thrift_change;

        /*increment decrement calculation ends*/

        /*----------------------------------------------------------------------------------------------------------*/

        $data['count_messages'] = $this->Common_model->countInboxMessages($this->session->userdata('user_id'));
        $data['count_comments'] = $this->Common_model->countInboxComments($this->session->userdata('user_id'));
        $data['messages_and_comments'] = $this->getMessagesAndComments(5); //5 x 2


        $this->load->view("header");
        //$this->load->view("common_left");
        $this->load->view("dashboard_page", $data);
        $this->load->view("footer");
    }

    private function getMessages($limit)
    {
        $messages = $this->Common_model->getInboxMessages($this->session->userdata('user_id'),$limit);

        if ($messages) {
            $i = 0;
            foreach ($messages as $msg) {

                if ($msg->message_created_at == 0) {
                    $messages[$i]->message_created_at_datestring = $this->lang->line('unvailable_text');
                } else {
                    $messages[$i]->message_created_at_datestring =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($msg->message_created_at);
                }

                $text = $msg->message;
                $text = substr(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", "'", "\"", "&quot;"), '', strip_tags($msg->message)), 0, 40) . '...';
                if (str_word_count($text, 0) > 10) {
                    $words = str_word_count($text, 2);
                    $pos = array_keys($words);
                    $text = substr($text, 0, $pos[10]) . '...';
                }
                $messages[$i]->message = $text;

                $messages[$i]->sender = new stdClass();
                $messages[$i]->sender->int = $msg->message_sender_id;

                $sender = $this->Common_model->getUser($msg->message_sender_id);

                if ($this->ion_auth->is_admin($msg->message_sender_id)) {
                    $messages[$i]->sender = 'Prosperis';
                } else if ($this->ion_auth->in_group('employer', $msg->message_sender_id)) {
                    $messages[$i]->sender = $sender->company;
                } else {
                    $messages[$i]->sender = $sender->first_name . ' ' . $sender->last_name;
                }

                $i++;
            }
        }

        return $messages;


    }

    private function getComments($limit)
    {
        $comments = $this->Common_model->getInboxComments($this->session->userdata('user_id'),$limit);

        if ($comments) {
            $i = 0;
            foreach ($comments as $cmt) {

                if ($cmt->commented_at == 0) {
                    $comments[$i]->commented_at_datestring = $this->lang->line('unvailable_text');
                } else {
                    $comments[$i]->commented_at_datestring =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($cmt->commented_at);
                }

                $text = $cmt->comment;
                $text = substr(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", "'", "\"", "&quot;"), '', strip_tags($cmt->comment)), 0, 40) . '...';
                if (str_word_count($text, 0) > 10) {
                    $words = str_word_count($text, 2);
                    $pos = array_keys($words);
                    $text = substr($text, 0, $pos[10]) . '...';
                }
                $comments[$i]->comment = $text;

                $comments[$i]->sender = new stdClass();
                $comments[$i]->sender->int = $cmt->comment_by;

                $sender = $this->Common_model->getUser($cmt->comment_by);

                if ($this->ion_auth->is_admin($cmt->comment_by)) {
                    $comments[$i]->sender = 'Prosperis';
                } else if ($this->ion_auth->in_group('employer', $cmt->comment_by)) {
                    $comments[$i]->sender = $sender->company;
                } else {
                    $comments[$i]->sender = $sender->first_name . ' ' . $sender->last_name;
                }

                $i++;
            }
        }

        return $comments;


    }

    private function getMessagesAndComments($limit)
    {
        $messages = $this->getMessages($limit);
        $comments = $this->getComments($limit);

        $messages_and_comments = array();

        if($messages){
            foreach ($messages as $msg) {

                $messages_and_comment = new stdClass();
                $messages_and_comment->url =  "message_module/view_message/".$msg->message_id;
                $messages_and_comment->sender = $msg->sender;
                $messages_and_comment->date = $msg->message_created_at;
                $messages_and_comment->datestring = $msg->message_created_at_datestring;
                $messages_and_comment->text = $msg->message;

                $messages_and_comments[] = $messages_and_comment;
            }
        }

        if($comments){
            foreach ($comments as $cmt) {

                $messages_and_comment = new stdClass();
                $messages_and_comment->url =  base_url()."message_module/view_message/". $cmt->message_id ."#comment-". $cmt->comment_id;
                $messages_and_comment->sender = $cmt->sender;
                $messages_and_comment->date = $cmt->commented_at;
                $messages_and_comment->datestring = $cmt->commented_at_datestring;
                $messages_and_comment->text = $cmt->comment;

                $messages_and_comments[] = $messages_and_comment;
            }
        }

        function cmp($a, $b) {
           return $a->date < $b->date;
        }

        $messages_and_comment = usort($messages_and_comments,'cmp');

        return $messages_and_comments;

    }

    private function cnp_max_min_ts_and_strings($curr_ts)
    {
        $curr_ym = $max_ts_ym_key =
            $this
                ->custom_datetime_library
                ->convert_and_return_TimestampToYearMonthGivenFormat($curr_ts, 'Y-n');


        $curr_ym_exploded = explode('-', $curr_ym);
        $curr_y = (int)$curr_ym_exploded[0];
        $curr_m = (int)$curr_ym_exploded[1];

        $nxt_y = $curr_y + 1;
        $prev_y = $curr_y - 1;

        if ($curr_m == 12) {
            $nxt_m = 1;
            $yr_for_nxt_m = $curr_y + 1;
        } else {
            $nxt_m = $curr_m + 1;
            $yr_for_nxt_m = $curr_y;
        }

        if ($curr_m == 1) {
            $prev_m = 12;
            $yr_for_prev_m = $curr_y - 1;
        } else {
            $prev_m = $curr_m - 1;
            $yr_for_prev_m = $curr_y;
        }


        $curr_m_last_d = $this->getNumDaysOfMonth($curr_y, $curr_m);
        $nxt_m_last_d = $this->getNumDaysOfMonth($yr_for_nxt_m, $nxt_m);
        $prev_m_last_d = $this->getNumDaysOfMonth($yr_for_prev_m, $prev_m);


        $curr_ym_min_string = $curr_y . '-' . $curr_m . '-' . '01' . '-' . '00-00-00';
        $curr_ym_max_string = $curr_y . '-' . $curr_m . '-' . $curr_m_last_d . '-' . '23-59-59';


        $nxt_ym_min_string = $yr_for_nxt_m . '-' . $nxt_m . '-' . '01' . '-' . '00-00-00';
        $nxt_ym_max_string = $yr_for_nxt_m . '-' . $nxt_m . '-' . $nxt_m_last_d . '-' . '23-59-59';


        $prev_ym_min_string = $yr_for_prev_m . '-' . $prev_m . '-' . '01' . '-' . '00-00-00';
        $prev_ym_max_string = $yr_for_prev_m . '-' . $prev_m . '-' . $prev_m_last_d . '-' . '23-59-59';


        $curr_ym_min_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($curr_ym_min_string, 'Y-n-d-H-i-s');

        $curr_ym_max_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($curr_ym_max_string, 'Y-n-d-H-i-s');


        $nxt_ym_min_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($nxt_ym_min_string, 'Y-n-d-H-i-s');

        $nxt_ym_max_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($nxt_ym_max_string, 'Y-n-d-H-i-s');

        $prev_ym_min_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($prev_ym_min_string, 'Y-n-d-H-i-s');

        $prev_ym_max_ts =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($prev_ym_max_string, 'Y-n-d-H-i-s');


        $arr = array();

        $arr['curr_ym'] = $curr_ym;

        $arr['curr_ym_min_string'] = $curr_ym_min_string;
        $arr['curr_ym_max_string'] = $curr_ym_max_string;

        $arr['nxt_ym_min_string'] = $nxt_ym_min_string;
        $arr['nxt_ym_max_string'] = $nxt_ym_max_string;

        $arr['prev_ym_min_string'] = $prev_ym_min_string;
        $arr['prev_ym_max_string'] = $prev_ym_max_string;


        $arr['curr_ym_min_ts'] = $curr_ym_min_ts;
        $arr['curr_ym_max_ts'] = $curr_ym_max_ts;

        $arr['nxt_ym_min_ts'] = $nxt_ym_min_ts;
        $arr['nxt_ym_max_ts'] = $nxt_ym_max_ts;

        $arr['prev_ym_min_ts'] = $prev_ym_min_ts;
        $arr['prev_ym_max_ts'] = $prev_ym_max_ts;

        return $arr;

    }

    private function getNumDaysOfMonth($y, $m)
    {
        $d = 30;
        for ($i = 1; $i <= 12; $i++) {


            $arr_31 = [1, 3, 5, 7, 8, 10, 12];
            $arr_30 = [4, 6, 9, 11];

            //how many days in a month ?
            if (in_array($m, $arr_31)) {
                $d = 31;
            } else if (in_array($m, $arr_30)) {
                $d = 30;
            } else if ($m == 2) {

                //leap year
                if ($y % 4 == 0 && ($y % 100 != 0)) {
                    $d = 29;
                } else {
                    $d = 28;
                }

            }


        }

        return $d;
    }


    /*----------------------------------------------------------------------------------------------------------------*/
    /*
     * using to get profile image
     * gets called by ajax
     *
     * */
    public function getDetailedUserData()
    {
        $user_data = $this->Common_model->getDetailedUserData($this->session->userdata('user_id'));

        return $user_data;
    }

    public function getDetailedUserData_asArray()
    {
        $user_data = $this->Common_model->getDetailedUserData_asArray($this->session->userdata('user_id'));

        if ($this->session->userdata('org_contact_user_id')) {
            $contact = $this->Common_model->getDetailedUserData($this->session->userdata('org_contact_user_id'));

            if ($contact) {
                $user_data['first_name'] = $contact->first_name;
                $user_data['last_name'] = $contact->last_name;
            }
        }
        return $user_data;
    }

    public function getDetailedUserData_withAjax()
    {
        $user_data = $this->getDetailedUserData();

        $encoded_user_data = json_encode($user_data);
        print_r($encoded_user_data);
    }

    public function getImageDirectory()
    {
        $image_directory = $this->custom_image_library->getMainImageDirectory();
        return $image_directory;
    }

    public function get_UserProfileImage_and_Directory_withAjax()
    {
        $user_data = $this->getDetailedUserData();
        $data['user_profile_image'] = $user_data->user_profile_image;
        $data['image_directory'] = $this->getImageDirectory();

        $encoded_data = json_encode($data);
        print_r($encoded_data);
    }

    public function get_site_detail()
    {
        $settings_code = 'general_settings';

        $office_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'office_site_logo');

        if ($office_site_logo == '' || $office_site_logo == null || $office_site_logo == false) {
            $site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $site_logo = $office_site_logo;
        }


        $site_name = $this->custom_settings_library->getASettingsValue($settings_code, 'site_name');


        $user_id = $this->session->userdata('user_id');
        $data['user_image'] = $this->Common_model->get_user_image($user_id);

        if ($this->session->userdata('org_contact_user_id')) {
            $contact = $this->Common_model->getDetailedUserData($this->session->userdata('org_contact_user_id'));

            if ($contact) {
                $data['user_image'] = $this->Common_model->get_user_image($this->session->userdata('org_contact_user_id'));
            }
        }

        $data['site_logo'] = '';
        $data['site_name'] = '';

        if ($site_logo) {
            $data['site_logo'] = $site_logo;
        }
        if ($site_name) {
            $data['site_name'] = $site_name;
        }


        $all_data = json_encode($data);
        echo $all_data;
    }

    public function getNotificationsByAjax()
    {
        $n_data['messages'] = $this->getMessages(3);
        $n_data['count_messages'] = $this->Common_model->countInboxMessages($this->session->userdata('user_id'));

        $n_data['comments'] = $this->getComments(3);
        $n_data['count_comments'] = $this->Common_model->countInboxComments($this->session->userdata('user_id'));

        $n_data['messages_and_comments'] = $this->getMessagesAndComments(3); //limit X 2

        $noti = $this->load->view('common_module/notification_list_html', $n_data, false);
        echo $noti;
    }

    public function checkIfUserIsActive_withAjax()
    {
        $user_data = $this->getDetailedUserData();

        if ($user_data->active == 0) {

            $this->ion_auth->logout();
        }

        if ($this->session->userdata('org_contact_user_id')) {
            $contact = $this->Common_model->getDetailedUserData($this->session->userdata('org_contact_user_id'));
            if ($contact) {
                if ($contact->active == 0) {
                    $this->ion_auth->logout();
                }
            }
        }
    }

    public function checkTourCompleted()
    {
        $user_data = $this->getDetailedUserData();
        echo $user_data->tour_completed;
    }

    public function tourCompleted()
    {
        $user_id = $this->session->userdata('user_id');
        $data['tour_completed'] = 1;
        $this->Common_model->updateTourCompleted($data,$user_id );
    }

    /*----------------------------------------------------------------------------------------------------------------*/


    public function update_employee_overall_percentage()
    {
        $user_id = $this->session->userdata('user_id');
        $data['user_employer_thrift_percentage'] = $this->input->post('user_employer_thrift_percentage');
        $this->Common_model->update_overall_percentage($data, $user_id);
        redirect('/');
    }


}
