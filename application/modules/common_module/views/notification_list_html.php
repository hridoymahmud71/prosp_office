<a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
   href="#" role="button"
   aria-haspopup="false" aria-expanded="false">
    <i class="zmdi zmdi-notifications-none noti-icon"></i>
    <?php if ((int)$count_messages+ (int)$count_comments > 0) { ?>
        <span class="noti-icon-badge"></span>
    <?php } ?>
</a>
<div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg"
     aria-labelledby="Preview">
    <!-- item-->
    <div class="dropdown-item noti-title">
        <h5>
            <small>
                <span class="label label-danger float-right">
                    <?= (int)$count_messages+ (int)$count_comments ?>
                </span>
                Notification
            </small>

        </h5>
    </div>

    <?php if ($messages_and_comments) { ?>
        <?php foreach ($messages_and_comments as $messages_and_comment) { ?>
            <a href="<?=$messages_and_comment->url?>"
               class="dropdown-item notify-item">
                <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                <p class="notify-details"><?= $messages_and_comment->sender ?>
                    <small class="text-muted"><?= $messages_and_comment->datestring ?></small>
                </p>
                <p class="notify-details"><?= $messages_and_comment->text ?></p>
            </a>
        <?php } ?>
    <?php } ?>

    <?php if(!$messages_and_comments || empty($messages_and_comments)) { ?>
        <p class="text-center mt-10 notify-details">No New Notifications</p>
    <?php } ?>

    <?php if ((int)$count_messages+ (int)$count_comments > 0) { ?>
        <!-- All-->
        <a href="message_module/inbox" class="dropdown-item notify-item notify-all">
            View All
        </a>
    <?php } ?>
</div>
