<!--<div class="content-page">-->


<?php if ($this->session->flashdata('thrift_error')) { ?>
    <section class="content mt-0">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="alert-heading"><?php echo lang('unsuccessful_text') ?></h4>

                    <?php if ($this->session->flashdata('thrift_error_exceed_limit_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_exceed_limit_text') ?>
                        </p>
                    <?php } ?>

                    <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                        </p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
<?php } ?>

<!-- Start content -->
<!--    <div class="content">-->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php if ($is_admin == 'admin') echo lang('page_subtitle_admin_dashboard_text') ?>
                    <?php if ($is_trustee == 'trustee') echo lang('page_subtitle_trustee_dashboard_text') ?>
                    <?php if ($is_employer == 'employer') echo lang('page_subtitle_employer_dashboard_text') ?>
                    <?php if ($is_employee == 'employee') echo lang('page_subtitle_employee_dashboard_text') ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($is_admin == 'admin') { ?>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="ion-social-buffer float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_running_thifts_text') ?></h6>
                    <h4 class="m-b-20"
                        data-plugin=""><?php echo ($ACTIVE_RUNNING_THRIFTS == '') ? '0' : $ACTIVE_RUNNING_THRIFTS; ?></h4>
                    <span class="label <?= $active_running_thrift_change > 0 ? ' label-success ' : ' label-danger '; ?>">
                        <?= $active_running_thrift_change ? number_format($active_running_thrift_change,2) : 0 ?>
                        % </span> <span class="text-muted"><?= lang('from_previous_month_text') ?></span></div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="zmdi zmdi-paypal-alt float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_active_thrifts_volume_text'); ?></h6>
                    <h4 class="m-b-20"><?php echo $currency; ?><?php $amn = ($ACTIVE_THRIFTS_VOLUME == '') ? '0' : $ACTIVE_THRIFTS_VOLUME;
                        echo number_format($amn, 2);
                        ?></h4>
                    <span class="label <?= $active_running_thrift_vol_change > 0 ? ' label-success ' : ' label-danger '; ?>"> <?= $active_running_thrift_vol_change?number_format($members_in_running_thrift_change,2):0 ?>
                        % </span> <span class="text-muted"><?= lang('from_previous_month_text') ?></span></div>

            </div>

            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="ion-stats-bars float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_total_thrifts_volume_text') ?></h6>
                    <h4 class="m-b-20"><?php echo $currency; ?><?php $amnt = ($AVERAGE_THRIFTS_VOLUME == '') ? '0' : $AVERAGE_THRIFTS_VOLUME;
                        echo number_format($amnt, 2);
                        ?></h4>
                    <span class="label <?= $average_running_thrift_vol_change > 0 ? ' label-success ' : ' label-danger '; ?>">
                        <?= $average_running_thrift_vol_change? number_format($average_running_thrift_vol_change,2):0 ?>
                        % </span> <span class="text-muted"><?= lang('from_previous_month_text') ?></span></div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="fa fa-paper-plane float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('number_of_member_active_thrift_text') ?></h6>
                    <h4 class="m-b-20" data-plugin="">
                        <?php echo ($MEMBERS_IN_ACTIVE_THRIFT == '') ? '0' : $MEMBERS_IN_ACTIVE_THRIFT; ?>
                        /
                        <?=  $non_deleted_active_thrifter_count ? $non_deleted_active_thrifter_count: 0 ?>
                    </h4>
                    <span class="label <?= $members_in_running_thrift_change > 0 ? ' label-success ' : ' label-danger '; ?>">
                        <?= $members_in_running_thrift_change?number_format($members_in_running_thrift_change,2):0 ?>
                        % </span> <span class="text-muted"><?= lang('from_previous_month_text') ?></span></div>
            </div>
        </div>

    <?php } ?>

    <hr>
    <div class="row" <?php if ($is_admin != 'admin') { ?> style="display: none" <?php } ?> >
        <div class="col-xs-12 col-lg-12 col-xl-8">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-20"><?= lang('performance_stat_text') ?>s</h4>
                <!--<div class="text-center">
                    <ul class="list-inline chart-detail-list m-b-0">
                        <li class="list-inline-item">
                            <h6 style="color: #3db9dc;"><i class="zmdi zmdi-circle-o m-r-5"></i>Series A</h6>
                        </li>
                        <li class="list-inline-item">
                            <h6 style="color: #1bb99a;"><i class="zmdi zmdi-triangle-up m-r-5"></i>Series B</h6>
                        </li>
                        <li class="list-inline-item">
                            <h6 style="color: #818a91;"><i class="zmdi zmdi-square-o m-r-5"></i>Series C</h6>
                        </li>
                    </ul>
                </div>-->
                <div id="products-per-month" style="height: 320px;"></div>
            </div>
        </div><!-- end col-->

        <div class="col-xs-12 col-lg-12 col-xl-4">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30"><?= lang('organization_trends_monthly_text') ?></h4>

                <!-- <div class="text-center m-b-20">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-sm btn-secondary">Today</button>
                        <button type="button" class="btn btn-sm btn-secondary">This Week</button>
                        <button type="button" class="btn btn-sm btn-secondary">Last Week</button>
                    </div>
                </div> -->
                <div id="organization-trending" style="height: 269px;"></div>
                <div class="text-center">
                    <ul class="list-inline chart-detail-list mb-0 m-t-10">
                        <?php foreach ($org_name as $row) { ?>
                            <li class="list-inline-item">
                                <h6><i class="zmdi zmdi-circle-o m-r-5"></i><?= $row ?></h6>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <?php if ($is_admin == 'admin') { ?>
        <hr>
        <div class="row">

            <div class="col-xs-12 col-md-6">
                <div class="card-box">
                    <h4 class="header-title m-t-0 m-b-20"><?= lang('inbox_text') ?>
                        (<?= (int)$count_messages + (int)$count_comments ?>)</h4>
                    <a href="message_module/inbox" type="button"
                       class="btn btn-info btn-rounded waves-effect waves-light"><?= lang('view_all_text') ?></a>

                    <?php if ($messages_and_comments) { ?>
                        <div class="inbox-widget nicescroll"
                             style="height: 250px;
                                 tabindex=" 5000>

                            <?php foreach ($messages_and_comments as $messages_and_comment) { ?>
                                <a href="<?= $messages_and_comment->url ?>">
                                    <div class="inbox-item">
                                        <!--<div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg"
                                                                         class="rounded-circle" alt=""></div>-->
                                        <p class="inbox-item-author"><?= $messages_and_comment->sender ?></p>
                                        <p class="inbox-item-text"><?= $messages_and_comment->text ?></p>
                                        <p class="inbox-item-date"><?= $messages_and_comment->datestring ?></p>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if (!$messages_and_comments || empty($messages_and_comments)) { ?>
                        <p><?= lang('no_unread_messages_text') ?></p>
                    <?php } ?>

                </div>
            </div><!-- end col-->

        </div>
    <?php } ?>


    <!--    </div> <!-- container -->
    <!--</div> <!-- content -->
</div>


<!-- Chart JS -->
<script src="assets/backend_assets/plugins/chart.js/chart.min.js"></script>

<!--Morris Chart-->
<script src="assets/backend_assets/plugins/morris/morris.min.js"></script>
<script src="assets/backend_assets/plugins/raphael/raphael-min.js"></script>


<script type="text/javascript">
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    Morris.Bar({
        element: 'products-per-month',
        stacked: true,
        data: <?php echo $productsgraph;?>,
        barColors: <?php echo $randomcolor;?>,
        barSizeRatio: 0.3,
        gridTextSize: '10px',
        hideHover: "true",
        xkey: 'm',
        ykeys: <?= $product_names?>,
        labels: <?= $product_names?>,

        xLabelFormat: function (x) {
            var month = months[x.x];
            //console.log(new Date(x.src.m).getFullYear());
            var year = new Date(x.src.m).getFullYear();
            // var year = x.getFullYear();
            return month + ' ' + year;
        },
    });

    Morris.Donut({
        element: 'organization-trending',
        data: <?php echo $final_pie_chart_val;?>
    });

</script>