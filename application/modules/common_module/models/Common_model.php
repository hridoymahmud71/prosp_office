<?php

class Common_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function getDetailedUserData($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getDetailedUserData_asArray($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row_array = $query->row_array();
        return $row_array;
    }


    /*----------------------------------------------------------------------------------------------------------------*/


    public function countActiveUsers()
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveGroupMembers($group_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);
        $this->db->where('u.active=', 1);

        $this->db->join('users_groups as ug', 'u.id = ug.user_id');
        $this->db->where('ug.group_id=', $group_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countActiveProducts()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_deletion_status!=', 1);
        $this->db->where('product_is_active', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countThriftGroups()
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group');
        $this->db->where('thrift_group_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }


    /* ----- count files starts ------*/
    public function countAllFiles()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_file as f');
        $this->db->where('f.file_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    /* ----- count files ends -------*/

    public function countMyEmployees($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('rspm_tbl_user_details as ud', 'u.id=ud.user_id');

        $my_employees_query =
            'SELECT id from users as u JOIN rspm_tbl_user_details as ud on u.id=ud.user_id WHERE ud.user_employer_id=' . $user_id;
        $this->db->where_in('u.id', $my_employees_query, false);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;

    }

    public function countMyEmployeesRunningThriftGroups($user_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);
        $this->db->where('tg.thrift_group_current_cycle < tg.thrift_group_term_duration');

        $my_employees_thrifts_query =
            'Select u.id from users as u 
              
              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $user_id . ')';

        $thrifts_query =
            'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $my_employees_thrifts_query . ')';

        $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);


        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function countEmployerApprovals($employer_id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users.approve',0);
        $this->db->where('users.verification',1);
        $this->db->join('rspm_tbl_user_details','users.id=rspm_tbl_user_details.user_id');
        $this->db->join('users_groups','users.id=users_groups.user_id');
        if($employer_id)
        {
            $this->db->where('rspm_tbl_user_details.user_employer_id',$employer_id);
        }
        $this->db->where('users_groups.group_id',7);

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function countMyRunningThriftGroups($user_id)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);
        $this->db->where('tg.thrift_group_current_cycle < tg.thrift_group_term_duration');

        $my_thrifts_query = 'Select u.id from users as u where u.deletion_status!=1 and u.id=' . $user_id;

        $thrifts_query =
            'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
                  WHERE tgm.thrift_group_member_id IN(' . $my_thrifts_query . ')';

        $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);


        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getEmployeeRating($user_id)
    {
        $this->db->select('ud.user_rating');
        $this->db->from('rspm_tbl_user_details as ud');
        $this->db->where('ud.user_id', $user_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    // Thrift all product call
    public function get_employee_range_product()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_deletion_status', 0);
        $this->db->where('product_is_active', 1);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_all_product_stat()
    {
        $this->db->select('pg_p.product_name, pg_t.thrift_group_creation_date, COUNT(pg_t.thrift_group_product_id) as total_product');
        $this->db->from('pg_product pg_p');
        $this->db->join('pg_thrift_group pg_t', 'pg_t.thrift_group_product_id=pg_p.product_id');
        $this->db->group_by('pg_t.thrift_group_product_id');
        //$this->db->where('pg_p.product_deletion_status', 0);


        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    public function getProductsArray()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    public function getMonthWiseProductCount($min_ts,$max_ts)
    {
        $this->db->select('pg_p.product_id,pg_p.product_name, pg_t.thrift_group_creation_date, COUNT(pg_t.thrift_group_product_id) as total_product');
        $this->db->from('pg_product pg_p');
        $this->db->join('pg_thrift_group pg_t', 'pg_t.thrift_group_product_id=pg_p.product_id');
        $this->db->group_by('pg_t.thrift_group_product_id');
        $this->db->where('pg_t.thrift_group_creation_date>=', $min_ts);
        $this->db->where('pg_t.thrift_group_creation_date<=', $max_ts);

        $query = $this->db->get();

        /*echo "<pre>";
        print_r($query);
        echo "</pre>";
        echo $this->db->last_query();die();*/
        $result = $query->result_array();
        return $result;
    }

    /* rsnew info */
    public function get_all_ACTIVE_RUNNING_THRIFTS($min_ts = false, $max_ts = false)
    {
        $this->db->select('*');
        $this->db->from('pg_thrift_group');

        if (!$min_ts && !$max_ts) {
            $this->db->where('thrift_group_activation_status', 1);
            $this->db->where('thrift_group_open', 0);
        }


        if ($min_ts && $max_ts) {

            $this->db->where(
                ' (thrift_group_start_date<=' . $min_ts
                . ' and '
                . 'thrift_group_end_date>=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_start_date>=' . $min_ts
                . ' and '
                . 'thrift_group_start_date<=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_end_date>=' . $min_ts
                . ' and '
                . 'thrift_group_end_date<=' . $max_ts . ' )'

            );

        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function get_all_ACTIVE_THRIFTS_VOLUME($min_ts = false, $max_ts = false)
    {

        if ($min_ts && $max_ts) {

            $extra_where =
                '(  (thrift_group_start_date<=' . $min_ts
                . ' and '
                . 'thrift_group_end_date>=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_start_date>=' . $min_ts
                . ' and '
                . 'thrift_group_start_date<=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_end_date>=' . $min_ts
                . ' and '
                . 'thrift_group_end_date<=' . $max_ts . ' )  )';

            $result = $this->db->query(
                'SELECT SUM(thrift_group_member_count*thrift_group_product_price) as thrift_volume 
            FROM pg_thrift_group WHERE (1) AND ' . $extra_where
            )->row()->thrift_volume;

        } else {
            $result = $this->db->query(
                'SELECT SUM(thrift_group_member_count*thrift_group_product_price) as thrift_volume 
            FROM pg_thrift_group WHERE (thrift_group_activation_status=1 AND thrift_group_open=0 )'
            )->row()->thrift_volume;
        }


        return $result;
    }

    public function get_all_AVERAGE_THRIFTS_VOLUME($min_ts = false, $max_ts = false)
    {
        if ($min_ts && $max_ts) {

            $extra_where =
                '(  (thrift_group_start_date<=' . $min_ts
                . ' and '
                . 'thrift_group_end_date>=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_start_date>=' . $min_ts
                . ' and '
                . 'thrift_group_start_date<=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_end_date>=' . $min_ts
                . ' and '
                . 'thrift_group_end_date<=' . $max_ts . ' )  )';

            $result = $this->db->query(
                'SELECT AVG(thrift_group_member_count*thrift_group_product_price) as thrift_volume 
            FROM pg_thrift_group WHERE (1 ) AND ' . $extra_where
            )->row()->thrift_volume;

        } else {
            $result = $this->db->query(
                'SELECT AVG(thrift_group_member_count*thrift_group_product_price) as thrift_volume 
            FROM pg_thrift_group WHERE (thrift_group_activation_status=1 AND thrift_group_open=0 )'
            )->row()->thrift_volume;

        }


        return $result;
    }

    public function get_all_MEMBERS_IN_ACTIVE_THRIFT($min_ts = false, $max_ts = false)
    {
        $this->db->select_sum('thrift_group_member_count');
        $this->db->from('pg_thrift_group');
        $this->db->where('thrift_group_activation_status', 1);
        $this->db->where('thrift_group_open', 0);

        if ($min_ts && $max_ts) {

            $this->db->where(
                ' (thrift_group_start_date<=' . $min_ts
                . ' and '
                . 'thrift_group_end_date>=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_start_date>=' . $min_ts
                . ' and '
                . 'thrift_group_start_date<=' . $max_ts . ' )'
                . ' or '
                . ' (thrift_group_end_date>=' . $min_ts
                . ' and '
                . 'thrift_group_end_date<=' . $max_ts . ' )'

            );

        }

        $query = $this->db->get();
        $result = $query->row()->thrift_group_member_count;
        return $result;
    }

    public function get_all_trift_grp_membrs($empId)
    {
        $year = date('Y'); // Get current year and subtract 1
        $start = $year . '-01-01';
        $end = $year . '-12-31';

        //	echo strtotime('2017-01-01');
        $res = $this->db->query("select count(pg_thrift_group_members.thrift_group_member_id) as total_join, from_unixtime(pg_thrift_group_members.thrift_group_member_join_date, '%Y-%m-%d') AS Month from rspm_tbl_user_details left join pg_thrift_group_members on rspm_tbl_user_details.user_id = pg_thrift_group_members.thrift_group_member_id where rspm_tbl_user_details.user_employer_id='" . $empId . "' and (from_unixtime(pg_thrift_group_members.thrift_group_member_join_date, '%Y-%m-%d')>='" . $start . "' and from_unixtime(pg_thrift_group_members.thrift_group_member_join_date, '%Y-%m-%d')<='" . $end . "') group by from_unixtime(pg_thrift_group_members.thrift_group_member_join_date, '%Y-%m-%d') order by from_unixtime(pg_thrift_group_members.thrift_group_member_join_date, '%Y-%m-%d') ASC")->result_array();
        $newarr = array();
        if ($res) {

            foreach ($res as $data) {
                $get_mnthnme = date('n', strtotime($data['Month']));
                $newarr[$get_mnthnme] = (int)$data['total_join'];
            }

        }
        for ($i = 1; $i <= 12; $i++) {
            if (!array_key_exists($i, $newarr)) {
                $newarr[$i] = 0;
            }
        }
        ksort($newarr);
        $r = json_encode(array_values($newarr));
        return $r;
    }
    /* rsnew info */


    /*kam*/
    public function get_exact_employee_id($employee_id)
    {
        $this->db->select('user_id');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_employer_id', $employee_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    }

    public function get_all_org_data()
    {
        $this->db->select('users.id');
        $this->db->from('users');
        $this->db->join('users_groups', 'users_groups.user_id=users.id');
        $this->db->where('users_groups.group_id', 6);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_name($emp_id)
    {
        $this->db->select('first_name,last_name,company');
        $this->db->from('users');
        $this->db->where('id', $emp_id);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function countThrifts($user_id, $start_date, $last_date)
    {
        $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,
                        tg.thrift_group_product_id,
                        tg.thrift_group_product_price,
                        
                        tg.thrift_group_member_count,
                        tg.thrift_group_member_limit,
                        
                        tg.thrift_group_current_cycle,
                        tg.thrift_group_term_duration,
                        
                        tg.thrift_group_creation_date,
                        tg.thrift_group_start_date,
                        
                        tg.thrift_group_open,
                        tg.thrift_group_activation_status,
                        tg.thrift_group_deletion_status,
                        
        ', false);

        $this->db->from('pg_thrift_group as tg');
        $this->db->where('tg.thrift_group_deletion_status!=', 1);

        $this->db->where('from_unixtime(tg.thrift_group_start_date, "%Y-%m-%d")>=', $start_date);
        $this->db->where('from_unixtime(tg.thrift_group_start_date, "%Y-%m-%d")<=', $last_date);

        $my_employees_thrifts_query =
            'Select u.id from users as u

              join rspm_tbl_user_details as ud
              on u.id=ud.user_id
              
              where( u.deletion_status!=1 
              and ud.user_employer_id=' . $user_id . ')';

        $thrifts_query =
            'SELECT tgm.thrift_group_id from pg_thrift_group_members as tgm
              WHERE tgm.thrift_group_member_id IN(' . $my_employees_thrifts_query . ')';

        $this->db->where_in('tg.thrift_group_id', $thrifts_query, false);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }
    /*kam*/


    /*------------------------------------------*/


    public function get_last_month_payment($last_month)
    {
        $start = $last_month . '-01';
        $end = $last_month . '-31';

        $this->db->select('count(thrift_group_payment_amount) as total_payment_count, SUM(thrift_group_payment_amount) as total_payment_amount');
        $this->db->from('pg_thrift_group_payments');

        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")>=', $start);
        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")<=', $end);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_this_month_payment()
    {
        $get_month_wise = date('Y-m'); // Get current year and subtract 1
        $start = $get_month_wise . '-01';
        $end = $get_month_wise . '-31';

        $this->db->select('count(thrift_group_payment_amount) as total_payment_count, SUM(thrift_group_payment_amount) as total_payment_amount');
        $this->db->from('pg_thrift_group_payments');

        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")>=', $start);
        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")<=', $end);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_next_month_payment($next_month)
    {
        $start = $next_month . '-01';
        $end = $next_month . '-31';

        $this->db->select('count(thrift_group_payment_amount) as total_payment_count, SUM(thrift_group_payment_amount) as total_payment_amount');
        $this->db->from('pg_thrift_group_payments');

        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")>=', $start);
        $this->db->where('from_unixtime(pg_thrift_group_payments.thrift_group_payment_date, "%Y-%m-%d")<=', $end);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_paid_amount()
    {
        $this->db->select('count(thrift_group_payment_amount) as total_payment_count, SUM(thrift_group_payment_amount) as total_payment_amount');
        $this->db->from('pg_thrift_group_payments');
        $this->db->where('pg_thrift_group_payments.thrift_group_is_payment_paid', 1);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_collection_information()
    {
        $this->db->select('thrift_group_payment_amount, thrift_group_payment_date as pay_date');
        $this->db->from('pg_thrift_group_payments');

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_disbursment_information()
    {
        $this->db->select('thrift_group_payment_recieve_amount, thrift_group_payment_date as rec_date');
        $this->db->from('pg_thrift_group_payment_recieves');

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_user_image($user_id)
    {
        $this->db->select('user_profile_image');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    /*-------------------------------------------*/

    public function countInboxMessages($reciever_id)
    {
        $adm = false;
        if($this->ion_auth->is_admin()){
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message as m');


        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id', 'left');

        $this->db->where('mr.message_deleted_by_reciever!=', 1);
        $this->db->where('mr.reciever_read!=', 1);

        if ($adm) {
            $this->db->where('mr.message_reciever_id', 1);
        } else {
            $this->db->where('mr.message_reciever_id', $reciever_id);
        }

        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }
    public function getInboxMessages($reciever_id,$limit)
    {
        $adm = false;
        if($this->ion_auth->is_admin()){
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message as m');
        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id', 'left');

        $this->db->where('mr.message_deleted_by_reciever!=', 1);
        $this->db->where('mr.reciever_read!=', 1);

        if ($adm) {
            $this->db->where('mr.message_reciever_id', 1);
        } else {
            $this->db->where('mr.message_reciever_id', $reciever_id);
        }

        $this->db->order_by('m.message_created_at', 'desc');
        $this->db->limit($limit);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    public function countInboxComments($user_id)
    {
        $adm = false;
        if($this->ion_auth->is_admin()){
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message_comment as mc');
        $this->db->join('pg_message as m' , 'mc.message_id=m.message_id');
        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id');

        $this->db->where('mr.message_deleted_by_reciever!=', 1);


        $this->db->group_start();
        $this->db->where('mc.reciever_read_comment!=', 1);
        $this->db->or_where('mc.sender_read_comment!=', 1);
        $this->db->group_end();

        $this->db->where('mc.comment_by!=', $user_id);

        if ($adm) {
            $this->db->group_start();
            $this->db->where('mr.message_reciever_id', 1);
            $this->db->or_where('m.message_sender_id', 1);
            $this->db->group_end();
        } else {
            $this->db->group_start();
            $this->db->where('mr.message_reciever_id', $user_id);
            $this->db->or_where('m.message_sender_id', $user_id);
            $this->db->group_end();
        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();
        return $num_rows;
    }
    public function getInboxComments($user_id,$limit)
    {
        $adm = false;
        if($this->ion_auth->is_admin()){
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message_comment as mc');
        $this->db->join('pg_message as m' , 'mc.message_id=m.message_id');
        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id');

        $this->db->where('mr.message_deleted_by_reciever!=', 1);

        $this->db->group_start();
        $this->db->where('mc.reciever_read_comment!=', 1);
        $this->db->or_where('mc.sender_read_comment!=', 1);
        $this->db->group_end();

        $this->db->where('mc.comment_by!=', $user_id);

        if ($adm) {
            $this->db->group_start();
            $this->db->where('mr.message_reciever_id', 1);
            $this->db->or_where('m.message_sender_id', 1);
            $this->db->group_end();
        } else {
            $this->db->group_start();
            $this->db->where('mr.message_reciever_id', $user_id);
            $this->db->or_where('m.message_sender_id', $user_id);
            $this->db->group_end();
        }

        $this->db->order_by('mc.commented_at', 'desc');
        $this->db->limit($limit);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function update_overall_percentage($data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $data);
    }

    public function get_my_employee_percentage($user_id)
    {
        $this->db->select('user_employer_thrift_percentage');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }


    public function get_total_disbursment($user_id)
    {
        $this->db->select('SUM(thrift_group_payment_recieve_amount) as total_disburs_amount');
        $this->db->from('pg_thrift_group_payment_recieves');
        $this->db->where('thrift_group_member_id', $user_id);
        $this->db->where('thrift_group_is_payment_recieved', 1);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_count_thrift($user_id)
    {
        $this->db->select('grp.thrift_group_term_duration, grp.thrift_group_current_cycle, grp.thrift_group_id');
        $this->db->from('pg_thrift_group grp');
        $this->db->join('pg_thrift_group_members grp_mem', 'grp.thrift_group_id=grp_mem.thrift_group_id');
        $this->db->where('grp_mem.thrift_group_member_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }

    public function get_user_rating($user_id)
    {
        $this->db->select('user_rating');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->result_array();
        return $num_rows;
    }


    public function get_thrift_completion_rate($user_id)
    {
        $this->db->select('grp.thrift_group_term_duration, grp.thrift_group_current_cycle, grp.thrift_group_id,grp.thrift_group_is_individual_product');
        $this->db->from('pg_thrift_group grp');
        $this->db->join('pg_thrift_group_members grp_mem', 'grp.thrift_group_id=grp_mem.thrift_group_id');
        $this->db->where('grp_mem.thrift_group_member_id', $user_id);
        $this->db->order_by('grp.thrift_group_id', 'DESC');

        $this->db->limit(5);

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    public function get_all_product()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_deletion_status', 0);
        $this->db->where('product_is_active', 1);
        $this->db->where('product_id>', 0);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getNextPayment($employee_id)
    {
        $this->db->select('tp.thrift_group_payment_id,
                           tp.thrift_group_payment_number,
                           tp.thrift_group_payment_amount,
                           tp.thrift_group_payment_date,
                           tg.thrift_group_number,
                           tg.thrift_group_id');
        $this->db->from('pg_thrift_group_payments as tp');
        $this->db->join('pg_thrift_group as tg', 'tp.thrift_group_id=tg.thrift_group_id');

        $this->db->where('tp.thrift_group_is_payment_paid', 0);
        $this->db->where('tp.thrift_group_payer_member_id', $employee_id);
        $this->db->where('tg.thrift_group_activation_status', 1);
        $this->db->where('tg.thrift_group_open', 0);

        $this->db->limit(1);
        $this->db->order_by('tp.thrift_group_payment_date','asc');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getNextDisburseMent($employee_id)
    {
        $this->db->select('tr.thrift_group_payment_recieve_id,
                           tr.thrift_group_payment_recieve_number,
                           tr.thrift_group_payment_recieve_amount,
                           tr.thrift_group_payment_date,
                           tg.thrift_group_number,
                           tg.thrift_group_id');
        $this->db->from('pg_thrift_group_payment_recieves as tr');
        $this->db->join('pg_thrift_group as tg', 'tr.thrift_group_id=tg.thrift_group_id');

        $this->db->where('tr.thrift_group_is_payment_recieved', 0);
        $this->db->where('tr.thrift_group_member_id', $employee_id);
        $this->db->where('tg.thrift_group_activation_status', 1);
        $this->db->where('tg.thrift_group_open', 0);

        $this->db->limit(1);
        $this->db->order_by('tr.thrift_group_payment_date','asc');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getPendingInvitationRecieved($reciever_id,$limit)
    {

        $this->db->select('*');
        $this->db->from('pg_custom_product_invitation as pi');
        $this->db->join('pg_custom_product_invited_members as pim', 'pi.cpi_id=pim.cpi_id');

        $this->db->where('pim.cpi_inv_to=', $reciever_id);
        $this->db->where('pim.cpi_inv_accepted', 0);
        $this->db->where('pim.cpi_is_invitor !=', 1);

        $this->db->order_by('pi.cpi_created_at', 'desc');
        $this->db->limit($limit, 0);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function updateTourCompleted($data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $data);
    }

    public function countThrifters($only_active,$include_deleted)
    {
        $this->db->select('u.id as id,');
        $this->db->from('users as u');

        if(!$include_deleted){
            $this->db->where('u.deletion_status!=', 1);
        }


        if($only_active){
            $this->db->where('u.active', 1);
        }

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); //employee is 7

        $query = $this->db->get();
        return $query->num_rows();
    }


}
