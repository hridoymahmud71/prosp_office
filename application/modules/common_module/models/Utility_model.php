<?php

class Utility_model extends CI_Model
{

    public function __construct()
    {
       // parent::__construct();
        $this->load->helper('cookie');
    }

    public function setTargetInACookie()
    {
        //called form third party mx_loader

        $items = array(
            'login',
            'logout',
            'forgot_password',
            'reset_password',
            'load_style',
            'maintenance',
            'need_permission',
            'does_not_exist',
            'thrifter_registration',
            'insert_thrifter_registration',
            'do_verifiy_user',
            'get_employers_by_select2',
            'ajax_check_tf_auth_is_needed',
        );
	   
        $segs = $this->uri->segment_array();

        $cookie_name = 'redirect_path_after_login';

        if ($this->session->userdata('user_id') == null) {

            if (!$this->in_array_any($items, $segs)) {

                $route = implode('/', $segs);
                setcookie($cookie_name, $route, time() + 3600,'/');
            }
        }

        if (in_array('logout', $segs)) {
            $this->deleteCookie($cookie_name);
        }

    }

    function in_array_any($needles, $haystack)
    {
        return count(array_intersect($needles, $haystack));
    }


    public function deleteCookie($cookie_name)
    {
        if (isset($_COOKIE[$cookie_name])) {
            unset($_COOKIE[$cookie_name]);
            setcookie($cookie_name, '', time() - 3600,'/');
        }

    }

    public function checkMaintenanceMode()
    {

        //unavailable for office portal
    }

    //only works when session has data
    public function checkTFSetup()
    {

        $user_id =  $this->session->userdata('user_id');

        $user = $this->getUser($user_id);
        $segs = $this->uri->segment_array();
        $items = array(
            'login',
            'logout',
            'forgot_password',
            'reset_password',
            'load_style',
            'maintenance',
            'need_permission',
            'does_not_exist',
            'thrifter_registration',
            'insert_thrifter_registration',
            'do_verifiy_user',
            'get_employers_by_select2',
            'get_site_detail',
            'get_notifications_by_ajax',
            'check_if_user_is_active_with_ajax'
        );

        if($user){
            $tf_cond_1 = !$this->input->post();
            $tf_cond_2 = $user->google_tf_auth_forced == 1 && empty($user->google_tf_secret_code);
            $tf_cond_3 = !$this->in_array_any($items, $segs);

            if($tf_cond_1 && $tf_cond_2 && $tf_cond_3) {
                redirect("user_profile_module/google_tf_auth/$user->id");
            }
        }


    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id',$user_id);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getUserIdBySubdmain($subdomain)
    {

        $user_id = 0;

        $this->db->select('user_id');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('subdomain', $subdomain);

        $row = $this->db->get()->row();

        if ($row) {
            $user_id = $row->user_id;
        }

        return $user_id;
    }

    //----------------------------------------------lll
    public function getSubdomainByUserId($user_id)
    {
        $subdomain = "";

        $this->db->select('subdomain');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);
        $row = $this->db->get()->row();

        if ($row) {
            $subdomain = $row->subdomain;
        }

        return $subdomain;
    }

    public function getEmployerSubdomain($user_id)
    {
        $employer_subdomain = "";

        $employer_id = $this->getUserEmployerId($user_id);

        if ($employer_id > 0) {
            $employer_subdomain = $this->getSubdomainByUserId($employer_id);
        }

        return $employer_subdomain;

    }

    public function makeThriftUrlWithEmployerSubdomain($url, $user_id)
    {
        $employer_subdomain = $this->getEmployerSubdomain($user_id);

        return $this->makeUrlWithSubdomain($url, $employer_subdomain);
    }

    public function makeUrlWithSubdomain($url, $subdomain)
    {
        $exploded_url = explode('//', $url);

        $join = $subdomain != "" ? "{$subdomain}." : "";

        return "{$exploded_url[0]}//{$join}{$exploded_url[1]}";
    }



    public function getUserEmployerId($user_id)
    {
        $user_employer_id = 0;

        $this->db->select('user_employer_id');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);

        $row = $this->db->get()->row();

        if ($row) {
            $user_employer_id = $row->user_employer_id;
        }

        return $user_employer_id;
    }

    //----------------------------------------------lll

    public function getIndividualLogo($subdomain, $user_id)
    {
        if (!$user_id) {
            $user_id = $this->getUserIdBySubdmain($subdomain);
        }

        $individual_logo = $this->custom_settings_library->getAUserSettingsValue('individual_settings', 'individual_logo', $user_id);

        if ($individual_logo == null || $individual_logo == '') {
            $individual_logo = false;
        }

        return $individual_logo;
    }

    public function getSubdomain()
    {
        $url = current_url();
        $subdomain = "";
        $parsedUrl = parse_url($url);

        $host = explode('.', $parsedUrl['host']);

        $subdomains = array_slice($host, 0, count($host) - 2);

        if (!empty($subdomains)) {
            if (isset($subdomains[0])) {
                $subdomain = $subdomains[0];
            }
        }

        return $subdomain;
    }

}