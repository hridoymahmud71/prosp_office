<?php

/*page texts*/
$lang['page_all_title_text'] = 'All Product';
$lang['page_all_subtitle_text'] = 'Manage Products';

$lang['breadcrumb_home_text'] = 'Product All';
$lang['breadcrumb_section_text'] = 'Product all';



/*page all product*/

$lang['add_button_text'] = 'Add Product';

$lang['column_serial_number_text'] = 'Serial';
$lang['column_product_name_text'] = 'Product Name';
$lang['column_product_term_duration_text'] = 'Product Term';
$lang['column_product_price_text'] = 'Product Amount';
$lang['column_product_status_text'] = 'Status';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

$lang['tooltip_deactivate_text'] = 'Deactivate product';
$lang['tooltip_activate_text'] = 'Activate product';

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Deactive';

$lang['product_activate_success_text'] = 'Product is successfully activated';
$lang['product_deactivate_success_text'] = 'Product is successfully Deactivated';

$lang['tooltip_view_text'] = 'View Product';
$lang['tooltip_edit_text'] = 'Edit Product';
$lang['tooltip_delete_text'] = 'Delete Product';

$lang['swal_title'] = 'Are you sure to delete this Product?'

?>