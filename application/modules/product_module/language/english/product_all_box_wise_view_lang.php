<?php

$lang['page_title_text'] = 'Products Show';

$lang['page_subtitle_text'] = 'Products';


$lang['breadcrum_home_text'] = 'Products';
$lang['breadcrum_page_text'] = 'All Products';



/*product*/

$lang['months_text'] = 'month(s)';
$lang['start_thrift_text'] = 'Start Thrifting';
$lang['product_details_text'] = 'Product Details';


/*flash*/
$lang['unsuccessful_text'] = 'Unsuccessful';
$lang['thrift_error_only_employee_allowed_text'] = 'Only Employees are allowed to join thrift';
$lang['thrift_error_exceed_limit_text'] = 'Exceeds thrift limit';


?>