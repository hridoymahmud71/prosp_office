<?php

$lang['product_title_text'] = 'Product Detail';
$lang['breadcrum_all_product_text'] = 'All Product';
$lang['breadcrum_product_home_text'] = 'Product Detail';
$lang['breadcrum_product_page_text'] = 'Product View';

$lang['product_name_text']='Product Name';
$lang['product_thrift_duration_text']='Duration';
$lang['product_thrift_amount_text']='Amount';
$lang['product_thrift_currency_text']='Naira';
$lang['product_description_text']='Product Description';
$lang['product_detail_title_text']='Full details of Product';
$lang['product_thrift_time_style_text']='Months';
?>