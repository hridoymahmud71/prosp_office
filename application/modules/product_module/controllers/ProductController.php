<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends MX_Controller
{
    public $CI;

    function __construct()
    {
        parent::__construct();

        $this->CI = &get_instance();
        $this->load->library('custom_log_library');
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('payment_module/custom_payment_library');
        $this->load->model('product_module/Product_model');
    }

    public function all_product_info()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('product_all_view');
        $data['all_product'] = $this->Product_model->get_all_product();

        $this->load->view("common_module/header");

        $this->load->view('all_product_view_page', $data);
        $this->load->view("common_module/footer");
    }

    public function all_product_box_wise_info()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('product_all_box_wise_view');
        $data['all_product'] = $this->Product_model->get_all_product();
        $this->lang->load('product_all_view');

        $this->load->view("common_module/header");

        $this->load->view('all_box_wise_product_view_page', $data);
        $this->load->view("common_module/footer");
    }

    public function view_product_details()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('product_detail_view');

        $product_id = $this->uri->segment(3);
        $data['all_info'] = $this->Product_model->get_product_data($product_id);
        $this->load->view("common_module/header");

        $this->load->view('view_product_details_page', $data);
        $this->load->view("common_module/footer");
    }

    public function add_product_info()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        if (!($this->ion_auth->in_group('superadmin') || $this->ion_auth->in_group('admin') )) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('product_add_form');

        $data = array();
        $data['title'] = 'page_title_add_text';
        $data['subtitle'] = 'page_subtitle_add_text';
        $data['breadcrumb'] = 'breadcrumb_page_add_text';

        $data['url'] = base_url() . 'product_module/insert_product';
        $data['product_name'] = '';
        $data['product_image'] = '';
        $data['product_term_duration'] = '';
        $data['product_price'] = '';
        $data['product_description'] = '';

        $this->load->view("common_module/header");

        $this->load->view('add_product_form_page', $data);
        $this->load->view("common_module/footer");
    }

    public function insert_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $data['product_name'] = trim($this->input->post('product_name'));
        $data['product_image'] = date('Y_m_d_H_i_s_') . $_FILES['product_image']['name'];
        $data['product_term_duration'] = trim($this->input->post('product_term_duration'));
        $data['product_price'] = trim($this->input->post('product_price'));
        $data['product_description'] = trim($this->input->post('product_description'));

        $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|is_unique[pg_product.product_name]');
        $this->form_validation->set_rules('product_term_duration', 'Product Term Duration', 'trim|required|greater_than[1]|less_than[36]|integer');
        $this->form_validation->set_rules('product_price', 'Product Amount', 'trim|required|numeric');
        $this->form_validation->set_rules('product_description', 'Product Description', 'trim|required');

        if ($this->form_validation->run() == true) {
            $uploaddir = $this->CI->config->item('pg_upload_path') . '/product/';
            $uploadfile = $uploaddir . date('Y_m_d_H_i_s_') . basename($_FILES['product_image']['name']);
            $imageFileType = pathinfo($uploadfile, PATHINFO_EXTENSION);

            if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif") {
                move_uploaded_file($_FILES['product_image']['tmp_name'], $uploadfile);
                $created_product_id = $this->Product_model->insert_product_data($data);

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'product',                                                              //3.    $type
                    $created_product_id,                                                    //4.    $type_id
                    'created',                                                              //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'admin',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                $this->session->set_flashdata('product_insert_sucess', 'Product Successfully Inserted');
                redirect('product_module/add_product_info');
            } else {
                $this->session->set_flashdata('error', 'error');
                $this->session->set_flashdata('product_insert_error', 'Image file type do not matched !');
                redirect('product_module/add_product_info');
            }
        } else {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('product_insert_error', validation_errors());
            redirect('product_module/add_product_info');
        }
    }


    public function activate_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        if (!($this->ion_auth->in_group('superadmin') || $this->ion_auth->in_group('admin') )) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('product_all_view');
        $product_id = $this->uri->segment(3);
        $this->Product_model->activate_product($product_id);
        $this->session->set_flashdata('product_activated', $this->lang->line('product_activate_success_text'));
        redirect('product_module/all_product_info');
    }

    public function deactivate_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        if (!($this->ion_auth->in_group('superadmin') || $this->ion_auth->in_group('admin') )) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('product_all_view');
        $product_id = $this->uri->segment(3);
        $this->Product_model->deactivate_product($product_id);
        $this->session->set_flashdata('product_deactivated', $this->lang->line('product_deactivate_success_text'));
        redirect('product_module/all_product_info');
    }

    public function edit_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        if (!($this->ion_auth->in_group('superadmin') || $this->ion_auth->in_group('admin') )) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('product_add_form');

        $product_id = $this->uri->segment(3);
        $data = $this->Product_model->get_product_data($product_id);

        $data['title'] = 'page_title_edit_text';
        $data['subtitle'] = 'page_subtitle_edit_text';
        $data['breadcrumb'] = 'breadcrumb_page_edit_text';

        $data['url'] = base_url() . 'product_module/update_product/' . $product_id;
        $data['product_name'] = $data[0]->product_name;
        $data['product_image'] = $data[0]->product_image;
        $data['product_term_duration'] = $data[0]->product_term_duration;
        $data['product_price'] = $data[0]->product_price;
        $data['product_description'] = $data[0]->product_description;

        $this->load->view("common_module/header");

        $this->load->view('add_product_form_page', $data);
        $this->load->view("common_module/footer");
    }

    public function update_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $product_id = $this->uri->segment(3);
        $data['product_name'] = $this->input->post('product_name');
        if ($_FILES['product_image']['name'] != '' || $_FILES['product_image']['name'] != null) {
            $data['product_image'] = date('Y_m_d_H_i_s_') . $_FILES['product_image']['name'];
        }
        $data['product_term_duration'] = trim($this->input->post('product_term_duration'));
        $data['product_price'] = trim($this->input->post('product_price'));
        $data['product_description'] = trim($this->input->post('product_description'));

        $old_product_term_duration = trim($this->input->post('old_product_term_duration'));
        $old_product_price = trim($this->input->post('old_product_price'));

        $term_duration_changed = false;
        $product_price_changed = true;
        if ($data['product_term_duration'] != $old_product_term_duration) {
            $term_duration_changed = true;
        }

        if ($data['product_description'] != $old_product_price) {
            $product_price_changed = true;
        }

        $this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
        $this->form_validation->set_rules('product_term_duration', 'Product Term Duration', 'trim|required|greater_than[1]|less_than[36]|integer');
        $this->form_validation->set_rules('product_price', 'Product Amount', 'trim|required|numeric');

        if ($this->form_validation->run() == true) {
            if ($_FILES['product_image']['name'] != '' || $_FILES['product_image']['name'] != null) {
                $uploaddir = $this->CI->config->item('pg_upload_path') . '/product/';
                $uploadfile = $uploaddir . date('Y_m_d_H_i_s_') . basename($_FILES['product_image']['name']);
                $imageFileType = pathinfo($uploadfile, PATHINFO_EXTENSION);
                if ($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png" || $imageFileType == "gif") {
                    move_uploaded_file($_FILES['product_image']['tmp_name'], $uploadfile);
                    $this->Product_model->update_product_data($data, $product_id);

                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        '',                                                                     //2.    $created_for
                        'product',                                                              //3.    $type
                        $product_id,                                                            //4.    $type_id
                        'updated',                                                              //5.    $activity
                        'admin',                                                                //6.    $activity_by
                        'admin',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        '',                                                                     //10.   $super_type
                        '',                                                                     //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                    /*creating log ends*/

                    if ($term_duration_changed || $product_price_changed) {
                        //do nothing
                    }

                    $this->session->set_flashdata('product_update_success', 'Product Successfully Updated');
                    redirect('product_module/all_product_info');
                } else {
                    // $this->Product_model->update_product_data($data,$product_id);
                    // $this->session->set_flashdata('product_update_success','Product Successfully Updated except Image');
                    // redirect('product_module/all_product_info');
                    $this->session->set_flashdata('error', 'error');
                    $this->session->set_flashdata('product_insert_error', 'Image file type do not matched !');
                    redirect('product_module/edit_product/' . $product_id);
                }
            } else {
                $this->Product_model->update_product_data($data, $product_id);

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'product',                                                              //3.    $type
                    $product_id,                                                            //4.    $type_id
                    'updated',                                                              //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'admin',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                if ($product_price_changed) {
                    //do nothing
                }

                $this->session->set_flashdata('product_update_success', 'Product Successfully Updated');
                redirect('product_module/all_product_info');
            }

        } else {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('product_insert_error', validation_errors());
            redirect('product_module/edit_product/' . $product_id);
        }
    }

    public function delete_product()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        $product_id = $this->uri->segment(3);
        $this->Product_model->delete_product($product_id);
        $this->session->set_flashdata('product_delete_success', 'Product Successfully Deleted');
        redirect('product_module/all_product_info');

    }

    private function alphaNum($cap = false, $length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }


        if ($cap == true) {
            $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } else {
            $alphabets = 'abcdefghijklmnopqrstuvwxyx';
            $characters = '0123456789abcdefghijklmnopqrstuvwxyx';
        }


        $integers = '0123456789';


        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

}