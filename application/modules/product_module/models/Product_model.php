<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function insert_product_data($data)
    {
        $this->db->insert('pg_product', $data);
        return $this->db->insert_id();
    }

    public function activate_product($product_id)
    {
    	$this->db->set('product_is_active', 1);
    	$this->db->where('product_id',$product_id);
    	$this->db->update('pg_product');
    }

    public function deactivate_product($product_id)
    {
    	$this->db->set('product_is_active', 0);
    	$this->db->where('product_id',$product_id);
    	$this->db->update('pg_product');
    }

    public function get_all_product()
    {
    	$this->db->select('*');
    	$this->db->from('pg_product');
    	$this->db->where('product_deletion_status',0);
        $this->db->where('product_id!=',0); //do not shw custom product in the list
    	
    	$query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_all_product_without_plan()
    {
        $this->db->select('*');
        $this->db->from('pg_product');
        $this->db->where('product_deletion_status',0);
        $this->db->where('product_id!=',0); //do not shw custom product in the list

        $this->db->group_start();
        $this->db->where('paystack_plan_code', null);
        $this->db->or_where('paystack_plan_id', 0);
        $this->db->group_end();

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function get_product_data($product_id)
    {
    	$this->db->select('*');
    	$this->db->from('pg_product');
    	$this->db->where('product_id',$product_id);
    	
    	$query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function update_product_data($data,$product_id)
    {
    	$this->db->where('product_id',$product_id);
    	$this->db->update('pg_product',$data);

    }

    public function delete_product($product_id)
    {
    	$this->db->set('product_deletion_status',1);
    	$this->db->where('product_id',$product_id);
    	$this->db->update('pg_product');
    }

}