<!-- <div class="content-page"> -->
    <!-- Start content -->
    <!-- <div class="content"> -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_all_title_text') ?>
                            <!-- <small><?php echo lang('page_all_subtitle_text') ?></small> -->
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_info"><?php echo lang('breadcrumb_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">
                                <?php echo lang('page_all_subtitle_text') ?>
                            </h4>
                            <?php if($this->ion_auth->is_admin()){?>
                            <ol class="breadcrumb float-right">
                                <a class="btn btn-primary"
                                   href="product_module/add_product_info"><?php echo lang('add_button_text') ?>
                                    &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                                </a>
                            </ol>
                            <?php }?>
                            <section>
                                <div class="" style="margin-top: 10%;">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <?php if ($this->session->flashdata('product_activated')) { ?>
                                                <div class="col-md-12 col-md-offset-3">
                                                    <div class="text-center alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                        <?php echo $this->session->flashdata('product_activated');?>
                                                    </div>
                                                </div>
                                            <?php }?>

                                            <?php if ($this->session->flashdata('product_update_success')) { ?>
                                                <div class="col-md-12 col-md-offset-3">
                                                    <div class="text-center alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button><?php echo $this->session->flashdata('product_update_success');?>
                                                    </div>
                                                </div>
                                            <?php }?>

                                            <?php if ($this->session->flashdata('product_deactivated')) { ?>
                                                <div class="col-md-12 col-md-offset-3">
                                                    <div class="text-center alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button><?php echo $this->session->flashdata('product_deactivated');?>
                                                    </div>
                                                </div>
                                            <?php }?>

                                            <?php if ($this->session->flashdata('product_delete_success')) { ?>
                                                <div class="col-md-12 col-md-offset-3">
                                                    <div class="text-center alert alert-success">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button><?php echo $this->session->flashdata('product_delete_success');?>
                                                    </div>
                                                </div>
                                            <?php }?>

                                            <!--<div class="box-header col-md-12">
                                                <div>
                                                    <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                                           border="0">
                                                        <tbody>
                                                        <tr id="filter_col0" data-column="0">
                                                            <td align="center"><label><?php /*echo lang('column_serial_number_text') */?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col0_filter" type="text">
                                                            </td>
                                                        </tr>
                                                        <tr id="filter_col1" data-column="1">
                                                            <td align="center">
                                                                <label for=""><?php /*echo lang('column_product_name_text') */?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col1_filter" type="text">
                                                            </td>
                                                        </tr>

                                                        <tr id="filter_col4" data-column="4">
                                                            <td align="center"><label
                                                                        for=""><?php /*echo lang('column_product_status_text') */?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col4_filter" type="hidden">
                                                                <select id="custom_status_filter" class="form-control">
                                                                    <option value="all"><?php /*echo lang('option_all_text') */?></option>
                                                                    <option value="1"><?php /*echo lang('option_active_text') */?></option>
                                                                    <option value="0"><?php /*echo lang('option_inactive_text') */?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>-->
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="product-table" class="table table-bordered table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <!--<th><?php /*echo lang('column_serial_number_text') */?></th>-->
                                                        <th><?php echo lang('column_product_name_text') ?></th>
                                                        <th><?php echo lang('column_product_term_duration_text') ?></th>
                                                        <th><?php echo lang('column_product_price_text') ?></th>
                                                        <th><?php echo lang('column_product_status_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($all_product) {
                                                        foreach ($all_product as $key=>$row) { ?>
                                                            <tr>
                                                                <!--<td><?php /*echo $key+1;*/?></td>-->
                                                                <td><?php echo $row->product_name;?></td>
                                                                <td><?php echo $row->product_term_duration;?></td>
                                                                <td><?php echo $this->custom_settings_library->getASettingsValue('currency_settings','currency_sign').' '.number_format($row->product_price,3);?></td>
                                                                <td data-search="<?php echo $row->product_is_active ?>">
                                                                    <?php if ($row->product_is_active == 1) { ?>
                                                                        <span class="label label-primary"><?php echo lang('status_active_text') ?></span>
                                                                        &nbsp;
                                                                        <a title="<?php echo lang('tooltip_deactivate_text') ?>"
                                                                           href="<?php echo base_url() . 'product_module/deactivate_product/'
                                                                               . $row->product_id ?>">
                                                                            <span class="label label-danger"><i class="fa fa-times"
                                                                                                                aria-hidden="true"></i></span>
                                                                        </a>

                                                                    <?php } else { ?>
                                                                        <span class="label label-default"><?php echo lang('status_inactive_text') ?></span>
                                                                        &nbsp;
                                                                        <a title="<?php echo lang('tooltip_activate_text') ?>"
                                                                           href="<?php echo base_url() . 'product_module/activate_product/'
                                                                               . $row->product_id ?>">
                                                                        <span class="label label-success"><i class="fa fa-check"
                                                                                                             aria-hidden="true"></i></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>

                                                                    &nbsp;
                                                                    <a title="<?php echo lang('tooltip_view_text') ?>" style="color: #2b2b2b"
                                                                       href="<?php echo base_url() . 'product_module/view_product_details/'
                                                                           . $row->product_id;?>"
                                                                       class=""><i class="fa fa-eye fa-lg"
                                                                                   aria-hidden="true"></i>
                                                                    </a>

                                                                    &nbsp;
                                                                    <a title="<?php echo lang('tooltip_edit_text') ?>" style="color: #2b2b2b"
                                                                       href="<?php echo base_url() . 'product_module/edit_product/'
                                                                           . $row->product_id;?>"
                                                                       class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                                   aria-hidden="true"></i>
                                                                    </a>
                                                                    &nbsp;

                                                                    <a title="<?php echo lang('tooltip_delete_text') ?>" id="" style="color: #2b2b2b"
                                                                       href="<?php echo base_url() . 'product_module/delete_product/'
                                                                           . $row->product_id;?>"
                                                                       class="confirmation" ">
                                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                                       aria-hidden="true">
                                                                    </i>
                                                                    </a>


                                                                </td>

                                                            </tr>
                                                        <?php }

                                                    } else { ?>
                                                        <div style="color: darkred;font-size: larger"><?php echo lang('no_currency_found_text') ?></div>
                                                    <?php } ?>

                                                    </tbody>
                                                    <!-- <tfoot>
                                                    <tr>
                                                    <tr>
                                                        <th><?php echo lang('column_serial_number_text') ?></th>
                                                        <th><?php echo lang('column_product_name_text') ?></th>
                                                        <th><?php echo lang('column_product_term_duration_text') ?></th>
                                                        <th><?php echo lang('column_product_price_text') ?></th>
                                                        <th><?php echo lang('column_product_status_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </tr>
                                                    </tfoot> -->
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->

                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>

                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    <!-- </div> -->
<!-- </div> -->






<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->

<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<!--this css style is holding datatable inside the box-->
<style>
    #product-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #product-table td,
    #product-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        /*$('#product-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });*/
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        //var table = $('#product-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        //$('#product-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        /*$('#product-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();*/
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>