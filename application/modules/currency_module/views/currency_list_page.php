<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_info"><?php echo lang('breadcrumb_home_text') ?></a></li>
                            <li class="breadcrumb-item"><a href="users/auth/show_user_groups"><?php echo lang('breadcrumb_section_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <h4 class="page-title float-left">
                                <small><?php echo lang('page_subtitle_text') ?></small>
                            </h4>
                            <div class="pull-right">
                                <a class="btn btn-primary"
                                   href="<?php echo base_url() . 'currency_module/add_currency' ?>"><?php echo lang('add_button_text') ?>
                                    &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                                </a>
                            </div>
                            <!--messages starts-->
                            <?php if ($this->session->flashdata('currency_create_success')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="add-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#add-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo lang('add_successfull_text') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div></div>
                            <?php if ($this->session->flashdata('currency_update_success')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#update-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo lang('update_successfull_text') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div></div>
                            <?php if ($this->session->flashdata('currency_activated')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#update-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo $this->session->flashdata('currency_name') . lang('currency_activated') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div></div>
                            <?php if ($this->session->flashdata('currency_deactivated')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#update-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo $this->session->flashdata('currency_name') . lang('currency_deactivated') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div></div>
                            <?php if ($this->session->flashdata('currency_deleted')) { ?>
                                <br>
                                <div class="col-md-6">
                                    <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                        <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                            <button type="button" class="close" data-target="#update-success-panel" data-dismiss="alert"><span
                                                        aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                            </button>
                                        </div>
                                        <div class="panel-body"><?php echo $this->session->flashdata('currency_name') . lang('currency_deleted') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <section class="content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-header">
                                                <h3 class="box-title"><?php echo lang('table_title_text') ?></h3>
                                                <div style="padding-top: 1%;padding-bottom: 1%">
                                                    <?php echo lang('toggle_column_text') ?>
                                                    <a class="toggle-vis" data-column="0"><?php echo lang('column_currency_name_text') ?></a>
                                                    -
                                                    <a class="toggle-vis" data-column="1"><?php echo lang('column_currency_short_name_text') ?></a>
                                                    -
                                                    <a class="toggle-vis" data-column="2"><?php echo lang('column_currency_sign_text') ?></a>
                                                    -
                                                    <a class="toggle-vis" data-column="3"><?php echo lang('column_currency_conversion_rate_text') ?></a>
                                                    -
                                                    <a class="toggle-vis" data-column="4"><?php echo lang('column_currency_status_text') ?></a>
                                                </div>
                                                <div>
                                                    <table style="width: 67%; margin: 0 auto 2em auto;" cellspacing="1" cellpadding="3"
                                                           border="0">
                                                        <tbody>
                                                        <tr id="filter_col0" data-column="0">
                                                            <td align="center"><label><?php echo lang('column_currency_name_text') ?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col0_filter" type="text">
                                                            </td>

                                                            <td align="center"><label>regex</label></td>
                                                            <td align="center"><input class="column_filter" id="col0_regex" type="checkbox">
                                                            </td>

                                                            <td align="center"><label>smart</label></td>
                                                            <td align="center"><input class="column_filter" id="col0_smart" checked="checked"
                                                                                      type="checkbox"></td>
                                                        </tr>
                                                        <tr id="filter_col1" data-column="1">
                                                            <td align="center">
                                                                <label for=""><?php echo lang('column_currency_short_name_text') ?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col1_filter" type="text">
                                                            </td>

                                                            <td align="center"><label>regex</label></td>
                                                            <td align="center"><input class="column_filter" id="col1_regex" type="checkbox">
                                                            </td>

                                                            <td align="center"><label>smart</label></td>
                                                            <td align="center"><input class="column_filter" id="col1_smart" checked="checked"
                                                                                      type="checkbox"></td>
                                                        </tr>

                                                        <tr id="filter_col4" data-column="4">
                                                            <td align="center"><label
                                                                        for=""><?php echo lang('column_currency_status_text') ?></label></td>
                                                            <td align="center">
                                                                <input class="column_filter form-control" id="col4_filter" type="hidden">
                                                                <select id="custom_status_filter" class="form-control">
                                                                    <option value="all"><?php echo lang('option_all_text') ?></option>
                                                                    <option value="1"><?php echo lang('option_active_text') ?></option>
                                                                    <option value="0"><?php echo lang('option_inactive_text') ?></option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="currency-table" class="table table-bordered table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_currency_name_text') ?></th>
                                                        <th><?php echo lang('column_currency_short_name_text') ?></th>
                                                        <th><?php echo lang('column_currency_sign_text') ?></th>
                                                        <th><?php echo lang('column_currency_conversion_rate_text') ?></th>
                                                        <th><?php echo lang('column_currency_status_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($all_currencies) {
                                                        foreach ($all_currencies as $a_currency) { ?>
                                                            <tr>
                                                                <td><?php echo $a_currency->currency_name ?></td>
                                                                <td><?php echo $a_currency->currency_short_name ?></td>
                                                                <td><?php echo $a_currency->currency_sign ?></td>
                                                                <td><?php echo $a_currency->conversion_rate ?></td>
                                                                <td data-search="<?php echo $a_currency->currency_status ?>">
                                                                    <?php if ($a_currency->currency_status == 1) { ?>
                                                                        <span class="label label-primary"><?php echo lang('status_active_text') ?></span>
                                                                        &nbsp;
                                                                        <a title="<?php echo lang('tooltip_deactivate_text') ?>"
                                                                           href="<?php echo base_url() . 'currency_module/deactivate_currency/'
                                                                               . $a_currency->currency_id ?>">
                                                                            <span class="label label-danger"><i class="fa fa-times"
                                                                                                                aria-hidden="true"></i></span>
                                                                        </a>

                                                                    <?php } else { ?>
                                                                        <span class="label label-default"><?php echo lang('status_inactive_text') ?></span>
                                                                        &nbsp;
                                                                        <a title="<?php echo lang('tooltip_activate_text') ?>"
                                                                           href="<?php echo base_url() . 'currency_module/activate_currency/'
                                                                               . $a_currency->currency_id ?>">
                                                                        <span class="label label-success"><i class="fa fa-check"
                                                                                                             aria-hidden="true"></i></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                </td>
                                                                <td>

                                                                    &nbsp;
                                                                    <a title="<?php echo lang('tooltip_edit_text') ?>" style="color: #2b2b2b"
                                                                       href="<?php echo base_url() . 'currency_module/edit_currency/'
                                                                           . $a_currency->currency_id ?>"
                                                                       class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                                   aria-hidden="true"></i>
                                                                    </a>
                                                                    &nbsp;

                                                                    <a title="<?php echo lang('tooltip_delete_text') ?>" id="" style="color: #2b2b2b"
                                                                       href="<?php echo base_url() . 'currency_module/delete_currency/'
                                                                           . $a_currency->currency_id ?>"
                                                                       class="confirmation" ">
                                                                    <i id="remove" class="fa fa-trash-o fa-lg"
                                                                       aria-hidden="true">
                                                                    </i>
                                                                    </a>


                                                                </td>

                                                            </tr>
                                                        <?php }

                                                    } else { ?>
                                                        <div style="color: darkred;font-size: larger"><?php echo lang('no_currency_found_text') ?></div>
                                                    <?php } ?>

                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                    <tr>
                                                        <th><?php echo lang('column_currency_name_text') ?></th>
                                                        <th><?php echo lang('column_currency_short_name_text') ?></th>
                                                        <th><?php echo lang('column_currency_sign_text') ?></th>
                                                        <th><?php echo lang('column_currency_conversion_rate_text') ?></th>
                                                        <th><?php echo lang('column_currency_status_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->

                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
</div>


<!-- <script>
    $(function () {
        $(document).tooltip();
    })
</script> -->

<!--this css style is holding datatable inside the box-->
<style>
    #currency-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #currency-table td,
    #currency-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>

<script>
    $(function () {
        $('#currency-table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>


<script>
    /*column toggle*/
    $(function () {

        var table = $('#currency-table').DataTable();

        $('a.toggle-vis').on('click', function (e) {
            e.preventDefault();

            // Get the column API object
            var column = table.column($(this).attr('data-column'));

            // Toggle the visibility
            column.visible(!column.visible());
        });

    });
</script>


<script>
    /*input searches*/
    $(document).ready(function () {
        $('#currency-table').DataTable();

        $('input.column_filter').on('keyup click', function () {
            filterColumn($(this).parents('tr').attr('data-column'));
        });
    });
</script>

<script>
    function filterColumn(i) {

        $('#currency-table').DataTable().column(i).search(
            $('#col' + i + '_filter').val(),
            $('#col' + i + '_regex').prop('checked'),
            $('#col' + i + '_smart').prop('checked')
        ).draw();
    }
</script>

<script>
    /*cutom select searches through input searches*/
    $(function () {
        /*-----------------------------*/
        $('#custom_status_filter').on('change', function () {

            if ($('#custom_status_filter').val() == 'all') {
                $('#col4_filter').val('');
                filterColumn(4);
            } else {
                $('#col4_filter').val($('#custom_status_filter').val());
                filterColumn(4);
            }

        });
        /*-----------------------------*/
    })
</script>

<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>