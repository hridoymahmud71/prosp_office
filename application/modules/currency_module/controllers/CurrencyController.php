<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CurrencyController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('currency_module/Currency_model');

        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->getCurrencyList();
        }
    }

    public function getCurrencyList()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $this->lang->load('currency_list');

            $data['all_currencies'] = $this->Currency_model->getCurrencies();

            $this->load->view("common_module/header");
            $this->load->view("common_module/common_left");
            $this->load->view("currency_module/currency_list_page", $data);
            $this->load->view("common_module/footer");

        }
    }

    public function addCurrency()
    {
        $this->getCurrencyForm('add_currency_form', $currency_id = 0);
    }

    public function editCurrency()
    {
        $currency_id = $this->uri->segment(3);
        $this->getCurrencyForm('edit_currency_form', $currency_id);
    }

    public function getCurrencyForm($form_name, $currency_id)
    {
        $this->lang->load('currency_form');
        $data['currency_id'] = $currency_id;
        $data['form_name'] = $form_name;

        if ($form_name == 'add_currency_form') {
            $data['form_action'] = 'currency_module/create_currency';
        }

        if ($form_name == 'edit_currency_form') {

            $data['form_action'] = 'currency_module/update_currency';

            if ($currency_id > 0) {
                $data['a_currency'] = $this->Currency_model->getACurrency($currency_id);
            }

        }

        $this->load->view("common_module/header");
        $this->load->view("common_module/common_left");
        $this->load->view("currency_module/currency_form_page", $data);
        $this->load->view("common_module/footer");
    }

    public function createCurrency()
    {
        $data['currency_name'] = $this->input->post('currency_name');
        $data['currency_short_name'] = $this->input->post('currency_short_name');
        $data['currency_sign'] = $this->input->post('currency_sign');
        $data['conversion_rate'] = $this->input->post('conversion_rate');
        $data['currency_status'] = $this->input->post('currency_status');


        $this->form_validation->set_rules('currency_name', 'currency_name',
            'required|is_unique[rspm_tbl_currency.currency_name]',
            array(
                'required' => $this->lang->line('currency_name_required_text'),
                'is_unique' => $this->lang->line('currency_name_unique_text')
            )
        );

        $this->form_validation->set_rules('currency_short_name', 'currency_short_name',
            'required|is_unique[rspm_tbl_currency.currency_short_name]',
            array(
                'required' => $this->lang->line('currency_short_name_required_text'),
                'is_unique' => $this->lang->line('currency_Shory_name_unique_text')

            )
        );

        $this->form_validation->set_rules('currency_sign', 'currency_sign', 'required',
            array(
                'required' => $this->lang->line('currency_sign_required_text')
            )
        );

        $this->form_validation->set_rules('conversion_rate', 'conversion_rate', 'decimal',
            array(
                'decimal' => $this->lang->line('conversion_rate_decimal_text')
            )
        );

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('validation_errors', validation_errors());

            $this->session->set_flashdata('currency_name', $this->input->post('currency_name'));
            $this->session->set_flashdata('currency_short_name', $this->input->post('currency_short_name'));
            $this->session->set_flashdata('currency_sign', $this->input->post('currency_sign'));
            $this->session->set_flashdata('conversion_rate', $this->input->post('conversion_rate'));
            $this->session->set_flashdata('currency_status', $this->input->post('currency_status'));

            redirect('currency_module/add_currency');
        } else {
            $currency_added = $this->Currency_model->addCurrency($data);

            if ($currency_added == true) {
                $this->session->set_flashdata('currency_create_success', 'currency_create_success');
                redirect('currency_module');
            }

        }

    }

    public function updateCurrency()
    {
        $this->input->post('currency_id');
        $data['currency_name'] = $this->input->post('currency_name');
        $data['currency_short_name'] = $this->input->post('currency_short_name');
        $data['currency_sign'] = $this->input->post('currency_sign');
        $data['conversion_rate'] = $this->input->post('conversion_rate');
        $data['currency_status'] = $this->input->post('currency_status');

        $this->session->set_flashdata('currency_name', $this->input->post('currency_name'));
        $this->session->set_flashdata('currency_short_name', $this->input->post('currency_short_name'));
        $this->session->set_flashdata('currency_sign', $this->input->post('currency_sign'));
        $this->session->set_flashdata('conversion_rate', $this->input->post('conversion_rate'));
        $this->session->set_flashdata('currency_status', $this->input->post('currency_status'));

        if ($this->Currency_model->
            checkIfExists('currency_name',
                $this->input->post('currency_name'),
                $this->input->post('currency_id')) == true
        ) {
            $this->session->set_flashdata('currency_name_exists','currency_name_exists');

            redirect('currency_module/edit_currency/'.$this->input->post('currency_id'));
        }

        if ($this->Currency_model->
            checkIfExists('currency_short_name',
                $this->input->post('currency_short_name'),
                $this->input->post('currency_id')) == true
        ) {
            $this->session->set_flashdata('currency_short_name_exists','currency_short_name_exists');

            redirect('currency_module/edit_currency/'.$this->input->post('currency_id'));
        }



        $this->form_validation->set_rules('currency_name', 'currency_name', 'required',
            array(
                'required' => $this->lang->line('currency_name_required_text')
            )
        );

        $this->form_validation->set_rules('currency_short_name', 'currency_short_name', 'required',
            array(
                'required' => $this->lang->line('currency_short_name_required_text')
            )
        );
        $this->form_validation->set_rules('currency_sign', 'currency_sign', 'required',
            array(
                'required' => $this->lang->line('currency_sign_required_text')
            )
        );
        $this->form_validation->set_rules('conversion_rate', 'conversion_rate', 'decimal',
            array(
                'decimal' => $this->lang->line('conversion_rate_decimal_text')
            )
        );

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());

            redirect('currency_module/edit_currency/' . $this->input->post('currency_id'));
        } else {
            $currency_updated = $this->Currency_model->updateCurrency($data, $this->input->post('currency_id'));

            if ($currency_updated == true) {
                $this->session->set_flashdata('currency_update_success', 'currency_create_success');
                redirect('currency_module');
            }

        }

    }

    public function activateCurrency()
    {
        $currency_id = $this->uri->segment(3);

        $activated = $this->Currency_model->activateCurrency($currency_id);
        if ($activated == true) {
            $a_currency = $this->Currency_model->getACurrency($currency_id);

            $this->session->set_flashdata('currency_name', $a_currency->currency_name);
            $this->session->set_flashdata('currency_activated', 'currency_activated');
            redirect('currency_module');
        }

    }

    public function deactivateCurrency()
    {
        $currency_id = $this->uri->segment(3);

        $deactivated = $this->Currency_model->deactivateCurrency($currency_id);
        if ($deactivated == true) {
            $a_currency = $this->Currency_model->getACurrency($currency_id);

            $this->session->set_flashdata('currency_name', $a_currency->currency_name);
            $this->session->set_flashdata('currency_deactivated', 'currency_deactivated');
            redirect('currency_module');
        }

    }

    public function deleteCurrency()
    {
        $currency_id = $this->uri->segment(3);

        $deactivated = $this->Currency_model->deactivateCurrency($currency_id);
        $deleted = $this->Currency_model->deleteCurrency($currency_id);

        if ($deactivated && $deleted) {
            $a_currency = $this->Currency_model->getACurrency($currency_id);

            $this->session->set_flashdata('currency_name', $a_currency->currency_name);
            $this->session->set_flashdata('currency_deleted', 'currency_deleted');
            redirect('currency_module');
        }

    }


}

