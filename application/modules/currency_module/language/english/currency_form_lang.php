<?php
/*page texts*/
$lang['page_title_add_text'] = 'Add Currency';
$lang['page_title_edit_text'] = 'Edit Currency';

$lang['page_subtitle_text'] = 'Currency\'s information';
$lang['box_title_text'] = 'Currency Information Form';

$lang['breadcrumb_home_text'] = 'Currency Settings';
$lang['breadcrumb_section_text'] = 'Currency';
$lang['breadcrumb_add_page_text'] = 'Create Currency';
$lang['breadcrumb_edit_page_text'] = 'Edit Currency';


/*Add group form texts*/
$lang['label_currency_name_text'] = 'Currency Name';
$lang['label_currency_short_name_text'] = 'Currency Short Name/Currency Code';
$lang['label_currency_sign_text'] = 'Currency Sign/Symbol';
$lang['label_currency_sign_help_text'] = 'help me to get codes for symbols';

$lang['label_conversion_rate_text'] = 'Conversion Rate (against usd)';
$lang['label_currency_status_text'] = 'Currency Status';

$lang['placeholder_currency_name_text'] = 'Enter Currency Name';
$lang['placeholder_currency_short_name_text'] = 'Enter Currency Short Name (example: BDT)';
$lang['placeholder_currency_sign_text'] = 'For \'$\' sign use  \'& # 3 6 \' (html enity decimal)';
$lang['placeholder_conversion_rate_text'] = 'Enter Conversion Rate (example:\'78.80\' means 1 usd=78.80 bdt)';

$lang['option_currency_status_enable_text'] = 'Enable Currency';
$lang['option_currency_status_endisable_text'] = 'Disable Currency';

$lang['button_submit_create_text'] = 'Create Currency';
$lang['button_submit_update_text'] = 'Update Currency';

/*validation error texts*/
$lang['currency_name_required_text'] = 'Currency Name is Required';
$lang['currency_name_unique_text'] = 'Currency Name Already Exists';

$lang['currency_short_name_required_text'] = 'Currency Short Name/Currency Code is Required';
$lang['currency_short_name_unique_text'] = 'Currency Name Already Exists';

$lang['already_exists'] = ' Already Exists';

$lang['currency_sign_required_text'] = 'Currency Name is Required';
$lang['conversion_rate_decimal_text'] = 'Currency Name is Required';

/*success messages*/
