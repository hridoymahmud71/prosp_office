<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Userprofile_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }


    //gets called from users/controllers/Auth/create_user()
    public function checkUserIn_UserDetailsTBL($user_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_user_details as ud');
        $this->db->where('ud.user_id', $user_id);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return false;
        } else {
            return true;
        }
    }

    //gets called from users/controllers/Auth/create_user()
    public function insertUserIdIn_UserDetailsTBL($user_id)
    {
        $this->db->set('user_id', $user_id);
        $this->db->insert('rspm_tbl_user_details');
    }

    /*------------------------------------------------------*/


    public function getRunningProjects_ofClient($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project as p');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningProjects_ofStaff($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_project_assigned_staff as pas');
        $this->db->where('pas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_project as p', 'pas.project_id=p.project_id');

        $this->db->where('p.status', 1);
        $this->db->where('p.progress <', 100);
        $this->db->where('p.deletion_status!=', 1);


        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningTasks_ofClient($client_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task as t');
        $this->db->join('rspm_tbl_project as p', 't.project_id=p.project_id');
        $this->db->where('p.client_id', $client_id);

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getRunningTasks_ofStaff($staff_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_task_assigned_staff as tas');
        $this->db->where('tas.staff_id', $staff_id);
        $this->db->join('rspm_tbl_task as t', 't.task_id=tas.task_id');

        $this->db->where('t.task_status', 1);
        $this->db->where('t.task_progress <', 100);
        $this->db->where('t.task_deletion_status!=', 1);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }


    /*----------------------------------------------------------------------------------------------------------------*/


    public function getGroupsByUser($user_id)
    {
        $this->db->select('g.id as group_id');
        $this->db->select('g.name as user_group_name');
        $this->db->from('users_groups as ug');
        $this->db->where('ug.user_id', $user_id);
        $this->db->join('groups as g', 'ug.group_id = g.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getUserInfo($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updateUserInfo($user_data, $user_id)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $user_data);

        return true;
    }

    public function updateUserDetailsInfo($user_details_data, $user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('rspm_tbl_user_details', $user_details_data);

        return true;
    }

    /*----------------------------------------------------------------------------------------------------------------*/

    public function getTimelineElements()
    {
        $data = new stdClass();
        return $data;
    }

    public function countTotalCountries($keyword)
    {
        $this->db->select('*');
        $this->db->from('countries');

        $this->db->like('country_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalCountries($keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('countries');

        $this->db->like('country_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function countTotalStates($keyword, $country_id)
    {
        $this->db->select('*');
        $this->db->from('states');

        if ($country_id) {
            $this->db->where('country_id', $country_id);
        }

        $this->db->like('state_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalStates($country_id, $keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('states');

        if ($country_id) {
            $this->db->where('country_id', $country_id);
        }

        $this->db->like('state_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function countTotalCities($keyword, $state_id)
    {
        $this->db->select('*');
        $this->db->from('cities');

        if ($state_id) {
            $this->db->where('state_id', $state_id);
        }

        $this->db->like('city_name', $keyword);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalCities($state_id, $keyword, $limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('cities');

        if ($state_id) {
            $this->db->where('state_id', $state_id);
        }


        $this->db->like('city_name', $keyword);

        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getCountry($country_id)
    {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('country_id', $country_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getState($state_id)
    {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('state_id', $state_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getCity($city_id)
    {
        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('city_id', $city_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getBanks($only_undeleted_banks = false)
    {
        $this->db->select('*');
        $this->db->from('pg_bank');

        if ($only_undeleted_banks) {
            $this->db->where('bank_deletion_status!=', 1);
        }

        $this->db->order_by('bank_name');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getActiveBots()
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');
        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->group_start();
        $this->db->where('u.is_user_bot', 1);
        $this->db->group_end();
        $this->db->where('u.deletion_status !=', 1);
        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 1);

        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    //------------------------------------------------------------------------------------------------------------------
    public function getAllThriftIdsByUser($user_id)
    {
        $ids = array();

        $this->db->select('tgm.thrift_group_id');
        $this->db->from('pg_thrift_group_members as tgm');
        $this->db->where('tgm.thrift_group_member_id', $user_id);

        $rows = $this->db->get()->result_array();

        if(!empty($rows)){
            $ids = array_column($rows,'thrift_group_id');
        }

        return $ids;

    }

    public function getThriftsByUser($user_id, $limit)
    {
        $thrift_group_ids = $this->getAllThriftIdsByUser($user_id);

        $result = null;

        if(!empty($thrift_group_ids)){

            $this->db->select('
                        tg.thrift_group_id,
                        tg.thrift_group_number,
                        tg.thrift_group_product_id,
                        tg.thrift_group_product_price,
                        
                        tg.thrift_group_member_count,
                        tg.thrift_group_member_limit,
                        
                        tg.thrift_group_current_cycle,
                        tg.thrift_group_term_duration,
                        
                        tg.thrift_group_creation_date,
                        tg.thrift_group_start_date,
                        
                        tg.thrift_group_open,
                        tg.thrift_group_activation_status,
                        tg.thrift_group_deletion_status,
                        
        ', false);

            $this->db->from('pg_thrift_group as tg');
            $this->db->where('tg.thrift_group_deletion_status!=', 1);
            $this->db->where_in("tg.thrift_group_id",$thrift_group_ids);

            //echo $thrift_group_query;die();

            $this->db->limit($limit);
            $query = $this->db->get();

            //echo $this->db->last_query();
            $result = $query->result();

        }
        
        return $result;
    }


}