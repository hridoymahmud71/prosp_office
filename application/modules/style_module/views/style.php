
a{
color:<?= $chosen_color?> !important;
}
a:hover{
color:<?= $chosen_color_deeper?> !important;
}
a.btn{
color:white !important;
background-color:<?= $chosen_color?> !important;
}
a.btn:hover{
color:white !important;
background-color:<?= $chosen_color_deeper?> !important;
}

table thead tr {
background-color: goldenrod;
color: white;
}

#topnav .topbar-main {
background-color: <?= $chosen_color ?> !important;
}

.btn-primary {
background-color: <?= $chosen_color ?> !important;
border-color: <?= $chosen_color ?> !important;
}

.btn-primary:hover {
background-color: <?= $chosen_color_deeper ?> !important;
border-color: <?= $chosen_color_deeper ?> !important;
}

.btn-custom {
background-color: <?= $chosen_color_deeper ?> !important;
border-color: <?= $chosen_color_deeper ?> !important;
}

.btn-custom:hover {
background-color: <?= $chosen_color_deeper ?> !important;
border-color: <?= $chosen_color_deeper ?> !important;
}

#topnav .navigation-menu > li > a {
color: <?= $chosen_color ?> !important;
}

#topnav .navigation-menu > li:hover a {
color: <?= $chosen_color_deeper ?> !important;
}

.label-default, .label-primary {
background-color: <?= $chosen_color ?> !important;
}

.notification-list .noti-title {
background-color: <?= $chosen_color ?> !important;
}

.account-pages {
background: <?= $chosen_color ?> !important;
}

.wrapper-page .card-box {
border: 5px solid <?= $chosen_color ?> !important;
}

.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
background-color: <?= $chosen_color ?> !important;
border-color: <?= $chosen_color ?> !important;
color: white !important;
}



#topnav .navigation-menu > li .submenu li a {
color: <?= $chosen_color ?> !important;
}
#topnav .navigation-menu > li .submenu li:hover a {
color: <?= $chosen_color_deeper ?> !important;
}

#topnav .navigation-menu > li .submenu li a {
display: block;
padding: 8px 25px;
clear: both;
white-space: nowrap;
color: #2b3d51;
}

select.form-control:not([size]):not([multiple]) {
height: auto;
}

.dropdown-item.active, .dropdown-item:active {
background-color: <?= $chosen_color_deeper ?> !important;
color: white !important;
}

div.hopscotch-bubble {
border: 3px solid <?= $chosen_color_deeper ?> !important;
}


div.hopscotch-bubble .hopscotch-bubble-arrow-container.up .hopscotch-bubble-arrow {
border-bottom: 19px solid <?= $chosen_color_deeper ?> !important;
}
div.hopscotch-bubble .hopscotch-bubble-arrow-container.down .hopscotch-bubble-arrow {
border-top: 19px solid <?= $chosen_color_deeper ?> !important;
}
div.hopscotch-bubble .hopscotch-bubble-arrow-container.left .hopscotch-bubble-arrow {
border-right: 19px solid <?= $chosen_color_deeper ?> !important;
}
div.hopscotch-bubble .hopscotch-bubble-arrow-container.right .hopscotch-bubble-arrow {
border-left: 19px solid <?= $chosen_color_deeper ?> !important;
}
div.hopscotch-bubble .hopscotch-bubble-number {
background: <?= $chosen_color_deeper ?> !important;
}

div.hopscotch-bubble .hopscotch-next, div.hopscotch-bubble .hopscotch-prev {
background-color: <?= $chosen_color_deeper ?> !important;
border-color: <?= $chosen_color_deeper ?> !important;
}

.hopscotch-nav-button{
background-color: <?= $chosen_color_deeper ?> !important;
}
.hopscotch-nav-button:hover{
background-color: <?= $chosen_color ?> !important;
}
.custom_login_backview{
background: url('<?= $site_banner_url?>') !important;
width: 100% !important;
background-size: cover !important;
}

@media only screen and (min-width: 768px) {
/* For desktop: */
.custom-wrapper-page {
margin: unset !important; ;
float: right;
right: 5%;
padding-top: 6%;
width:100%;
}
}





