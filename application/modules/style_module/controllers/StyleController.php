<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StyleController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
    }

    public function loadStyle()
    {
        $subdomain = $this->Utility_model->getSubdomain();
        $employer_id = $subdomain != "" ? $this->Utility_model->getUserIdBySubdmain($subdomain) : 0;
        $user_id = $this->session->userdata('user_id');


        //--------------------------------------------------------------------------------------------------------------

        $portal = $this->uri->segment(3);
        $portal = !$portal ? 'office' : $portal;
        $chosen_color = '#3f3f3f';
        $chosen_color_deeper = '#2a2a2a';

        if ($portal == 'office') {
            $office_color = $this->getChosenColor('office', '');
            $office_color_deeper = $this->getChosenColor('office', '_deeper');

            $chosen_color = $office_color ? $office_color : $chosen_color;
            $chosen_color_deeper = $office_color_deeper ? $office_color_deeper : $chosen_color_deeper;
        }

        if ($portal == 'partner') {
            $partner_color = $this->getChosenColor('partner', '');
            $partner_color_deeper = $this->getChosenColor('partner', '_deeper');

            $chosen_color = $partner_color ? $partner_color : $chosen_color;
            $chosen_color_deeper = $partner_color_deeper ? $partner_color_deeper : $chosen_color_deeper;
        }

        if ($portal == 'thrift') {
            $thrift_color = $this->getChosenColor('thrift', '');
            $thrift_color_deeper = $this->getChosenColor('thrift', '_deeper');

            $chosen_color = $thrift_color ? $thrift_color : $chosen_color;
            $chosen_color_deeper = $thrift_color_deeper ? $thrift_color_deeper : $chosen_color_deeper;
        }

        if ($portal == 'trustee') {
            $trustee_color = $this->getChosenColor('trustee', '');
            $trustee_color_deeper = $this->getChosenColor('trustee', '_deeper');

            $chosen_color = $trustee_color ? $trustee_color : $chosen_color;
            $chosen_color_deeper = $trustee_color_deeper ? $trustee_color_deeper : $chosen_color_deeper;
        }

        $site_banner_url = "";
        $site_banner = $this->getSiteBanner();

        if ($site_banner) {
            $site_banner_url = $this->config->item('pg_upload_source_path') . '/image/' . $site_banner;
        }


        $show_individual_color = false;
        $individual_color = false;
        $individual_color_deeper = false;
        if ($employer_id) {
            if ($this->ion_auth->in_group("employer", $employer_id) && $portal = "thrift") {
                $show_individual_color = true;
                $individual_color = $this->getIndividualChosenColor('', $employer_id);
                $individual_color_deeper = $this->getIndividualChosenColor('_deeper', $employer_id);
            }
        }

        if ($user_id) {
            if ($this->ion_auth->in_group("employer", $user_id) && $portal = "partner") {
                $show_individual_color = true;
                $individual_color = $this->getIndividualChosenColor('', $user_id);
                $individual_color_deeper = $this->getIndividualChosenColor('_deeper', $user_id);
            }
        }

        if ($show_individual_color && $individual_color) {
            $chosen_color = $individual_color;
        }
        if ($show_individual_color && $individual_color_deeper) {
            $chosen_color_deeper = $individual_color_deeper;
        }

        header("Content-type: text/css");

        $data['chosen_color'] = $chosen_color;
        $data['chosen_color_deeper'] = $chosen_color_deeper;
        $data['site_banner_url'] = $site_banner_url;

        $style = $this->load->view('style', $data, true);
        echo $style;
    }

    public function getIndividualChosenColor($shade, $user_id)
    {
        $color = $this->custom_settings_library->getAUserSettingsValue('individual_settings', 'individual_chosen_color' . $shade, $user_id);

        if ($color == null || $color == '') {
            $color = false;
        }

        return $color;
    }

    public function getChosenColor($portal, $shade)
    {
        $color = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', $portal . '_chosen_color' . $shade);

        if ($color == null || $color == '') {
            $color = false;
        }

        return $color;
    }

    public function getSiteBanner()
    {
        $site_banner = $this->custom_settings_library->getASettingsValue('general_settings', 'site_banner');

        if ($site_banner == null || $site_banner == '') {
            $site_banner = false;
        }

        return $site_banner;
    }

}
