<?php

/*page texts*/
$lang['page_title_text'] = 'Email Templates';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrumb_page_text'] = 'Email Templates';
$lang['page_form_title_text'] = 'All Email Templates';

// BANK COLUMN


$lang['column_serial_text'] = 'Serial';
$lang['column_email_template_type_text'] = 'Template Type';
$lang['column_email_template_subject_text'] = 'Template Subject';
$lang['column_actions_text'] = 'Action';



$lang['field_mandatory_text'] = '** This field is Required';


$lang['success_bank_upload_text'] = 'Bank Detail is Successfully Inserted';
$lang['bank_upload_err_text'] = 'Please Fill all the (*) Marked field correctly';





?>