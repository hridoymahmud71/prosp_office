<?php

/*page texts*/
$lang['page_title_text'] = 'Edit Email Template';
$lang['breadcrum_home_text'] = 'Email Template';
$lang['breadcrumb_page_text'] = 'Edit Template';
$lang['page_form_title_text'] = 'Email Template Details';


$lang['label_email_template_type_text'] = 'Template Type';
$lang['label_email_template_subject_text'] = 'Template Subject';
$lang['label_email_template_text'] = 'Email Template';

$lang['placeholder_email_template_type_text'] = 'Enter Template Type';
$lang['placeholder_email_template_subject_text'] = 'Enter Template Subject';
$lang['placeholder_email_template_text'] = 'Enter Email Template';


$lang['file_submit_text'] = 'Submit';
$lang['modal_cancel_text'] = 'Reset';

$lang['field_mandatory_text'] = '** This field is Required';


$lang['successful_text'] = 'Successful !';
$lang['update_success_text'] = 'Template Successfully Updated';






?>