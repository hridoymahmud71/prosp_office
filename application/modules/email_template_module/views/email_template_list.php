<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">

                    <h4 class="page-title float-left"><?php echo lang('page_form_title_text') ?></h4>

                    <!-- Main content -->
                    <section class="content">
                        <div>
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <table id="email-template-datatable"
                                               class="table table-striped table-bordered table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <!--<th><?/*= lang('column_serial_text'); */?></th>-->
                                                <th><?= lang('column_email_template_type_text'); ?></th>
                                                <th><?= lang('column_email_template_subject_text'); ?></th>
                                                <th><?= lang('column_actions_text'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php if ($email_templates) {
                                                $i = 0; ?>
                                                <?php foreach ($email_templates as $t) {
                                                    $i++; ?>
                                                    <tr>
                                                        <!--<td><?/*= $i; */?></td>-->
                                                        <td><?= $t->email_template_type; ?></td>
                                                        <td><?= $t->email_template_subject; ?></td>

                                                        <td> &nbsp;
                                                            <a title="<?php echo lang('tooltip_edit_text') ?>"
                                                               style="color: #2b2b2b"
                                                               href="email_template_module/edit_email_template/<?= $t->email_template_id; ?>"
                                                               class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                           aria-hidden="true"></i>
                                                            </a> &nbsp;
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <div class="clearfix"></div>
                </div>
            </div><!-- end col -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div> <!-- container -->


<script type="text/javascript">
    $(document).ready(function () {

    });
</script>


<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>