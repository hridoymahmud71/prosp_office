<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                    <!-- <small><?php echo lang('page_subtitle_add_text') ?></small> -->
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="email_template_module/view_templates"><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('successful')) { ?>
        <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>
                <?= lang('successful_text')?>
            </strong>
            &nbsp;
            <?php if ($this->session->flashdata('update_success')){
               echo  lang('update_success_text');
            } ?>
        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('error')) { ?>
        <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong>
                <?php if ($this->session->flashdata('validation_errors')) {
                    echo $this->session->flashdata('validation_errors');
                } ?>
            </strong>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('page_form_title_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">

                        <form role="form" action="email_template_module/update_email_template" method="post" data-parsley-validate novalidate>

                            <input type="hidden" name="email_template_id" value="<?= $template->email_template_id; ?>">

                            <div class="form-group ">
                                <label for="bank_name"
                                       class="col-sm-4 form-control-label"><?php echo lang('label_email_template_type_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" readonly parsley-type="email_template_type" class="form-control"
                                           name="email_template_type" value="<?= $template->email_template_type; ?>"
                                           id="" placeholder="<?php echo lang('placeholder_email_template_type_text'); ?>">
                                    <?php if ($this->session->flashdata('error')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="email_template_subject"
                                       class="col-sm-4 form-control-label"><?php echo lang('label_email_template_subject_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text"  parsley-type="email_template_subject" class="form-control"
                                           name="email_template_subject" value="<?= $template->email_template_subject; ?>"
                                           id="" placeholder="<?php echo lang('placeholder_email_template_subject_text'); ?>">
                                    <?php if ($this->session->flashdata('error')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="email_template"
                                       class="col-sm-4 form-control-label"><?php echo lang('label_email_template_text'); ?><span
                                            class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <textarea id="email_template" class="form-control" name="email_template"
                                              rows="3"><?= $template->email_template; ?></textarea>
                                    <?php if ($this->session->flashdata('error')) { ?>
                                        <label class="text-danger"
                                               for="confirm2"><?php echo lang('field_mandatory_text'); ?></label>
                                    <?php } ?>
                                </div>
                            </div>


                            <div class="form-group  m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('file_submit_text'); ?>
                                </button>
                            </div>
                        </form>
                    </div><!-- end row -->
                </div>
            </div><!-- end col -->


        </div>
    </div>
</div>

<script>
    $(function () {

        tinymce.init({
            selector: '#email_template',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });



    });
</script>
