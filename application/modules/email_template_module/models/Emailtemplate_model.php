<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Emailtemplate_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function getEmailTemplates()
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getEmailTemplate($template_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        $this->db->where('email_template_id',$template_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function updateEmailTemplate($data,$email_template_id)
    {
        $this->db->where('email_template_id',$email_template_id);
        $this->db->update('tbl_email_template',$data);
    }





}