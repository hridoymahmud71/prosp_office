<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailtemplateController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('');
        }

        if (!$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        //$this->load->model('Contact_model');
        $this->load->model('user/Ion_auth_model');
        $this->load->model('email_template_module/Emailtemplate_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');

    }

    public function viewTemplates()
    {
        $this->lang->load('email_template_list');

        $data['email_templates'] = $this->Emailtemplate_model->getEmailTemplates();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("email_template_list", $data);
        $this->load->view("common_module/footer");
    }

    public function addEmailTemplate()
    {
        //not needed
    }

    public function editEmailTemplate()
    {
        $this->lang->load('email_template_form');

        $template_id = $this->uri->segment(3);
        $data['template'] = $this->Emailtemplate_model->getEmailTemplate($template_id);

        $data['form_action'] = 'email_template_module/update_email_template';

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("email_template_form",$data);
        $this->load->view("common_module/footer");
    }

    public function updateEmailTemplate()
    {
        $template_id = $this->input->post('email_template_id');

        $data['email_template_type'] = trim($this->input->post('email_template_type'));
        $data['email_template_subject'] = trim($this->input->post('email_template_subject'));
        $data['email_template']=$this->input->post('email_template');

        $this->form_validation->set_rules('email_template_type', 'Email Template Type', 'required');
        $this->form_validation->set_rules('email_template_subject', 'Email Template Subject', 'required');
        $this->form_validation->set_rules('email_template', 'Email Template', 'required');


        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error','error');
            $this->session->set_flashdata('validation_errors',validation_errors());
            redirect('email_template_module/edit_email_template/'.$template_id);
        }

        $this->Emailtemplate_model->updateEmailTemplate($data, $template_id);
        $this->session->set_flashdata('successful','successful');
        $this->session->set_flashdata('update_success','update_success');
        redirect('email_template_module/edit_email_template/'.$template_id);
    }

}



    ?>