
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                            <small><?php echo lang('page_subtitle_text') ?></small>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/add_product_info"><?php echo lang('breadcrumb_home_text')?></a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url().'users/auth'?>"><?php echo lang('breadcrumb_section_text')?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text')?></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30"><?php echo lang('product_add_form_header_text') ?></h4>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-6">
                              <div class="box box-primary">
                                  <div class="box-header with-border">
                                    <h3 class="m-t-0 header-title"><?php echo lang('box_title_text') ?></h3>
                                        <?php if(isset($message)){?>
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <strong><?=$message;?></strong>
                                            </div>
                                          <?php }?>

                                        <?php
                                        if ($this->session->flashdata('group_selection_error_1')) {?>
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <strong><?=$this->session->flashdata('group_selection_error_1');?></strong>
                                            </div>
                                        <?php  }?>

                                        <?php
                                        if ($this->session->flashdata('group_selection_error_2')) {?>
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <strong><?=$this->session->flashdata('group_selection_error_2');?></strong>
                                            </div>
                                        <?php  }?>

                                        <?php
                                        if ($this->session->flashdata('group_selection_error_pg_thrift')) {?>
                                            <div class="alert alert-danger">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                                <strong><?=$this->session->flashdata('group_selection_error_pg_thrift');?></strong>
                                            </div>
                                        <?php  }?>
                                    <div class="col-md-2"></div>
                                </div>
                              </div>

                                <form class="user_edit_form" action="<?php echo base_url() . 'users/auth/edit_user/' . $user->id ?>" role="form" id="" method="post" enctype="multipart/form-data">
                                  <div class="box-body">
                                      <div class="form-group">
                                          <input type="hidden" name="id" value="<?php echo $user->id ?>">
                                            <?php if ($csrf) foreach ($csrf as $k => $v) { ?>
                                                <input class="group_<?php echo $k ?>" type="hidden" name="<?php echo $k ?>"
                                                       value="<?php echo $v ?>">
                                            <?php } ?>
                                          <div class="form-group" <?php if ($this->ion_auth->in_group('employer',$user->id) ) { ?> style="display: none" <?php } ?>>
                                              <label for="first_name"><?php echo lang('label_firstname_text') ?></label>
                                                <input type="text" name="first_name" class="form-control" id="first_name"
                                               value="<?php echo $user->first_name ?>"
                                               placeholder="<?php echo lang('placeholder_firstname_text') ?>">
                                          </div>
                                          <div class="form-group" <?php if ($this->ion_auth->in_group('employer',$user->id) ) { ?> style="display: none" <?php } ?>>
                                              <label for="last_name"><?php echo lang('label_lastame_text') ?></label>
                                              <input type="text" name="last_name" class="form-control" id="last_name"
                                               value="<?php echo $user->last_name ?>"
                                               placeholder="<?php echo lang('placeholder_lastame_text') ?>">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <label for="phone"><?php echo lang('label_phone_text') ?></label>
                                          <input type="text" class="form-control" name="phone" id="user_position"
                                           value="<?php echo $user->phone ?>"
                                           placeholder="<?php echo lang('placeholder_phone_text') ?>">
                                      </div>

                                      <div class="form-group"  <?php if (!($this->ion_auth->in_group('employer',$user->id) )) { ?> style="display: none" <?php } ?>>
                                          <label for="company"><?php echo lang('label_company_name_text') ?></label>
                                          <input type="text" class="form-control" name="company" id="company"
                                           value="<?php echo $user->company ?>"
                                           placeholder="<?php echo lang('placeholder_company_name_text') ?>">
                                      </div>
                                      <div class="form-group">
                                          <label for="password"><?php echo lang('label_password_text') ?>
                                              <span id="see_password" title="see"  style="color:#2b2b2b"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                          </label>
                                          <input type="password" name="password" class="form-control" id="password"
                                            placeholder="<?php echo lang('placeholder_password_text') ?>">

                                      </div>
                                      <div class="form-group">
                                          <label for="password_confirm"><?php echo lang('label_confirm_password_text') ?>
                                              <span id="see_password_confirm" title="see"  style="color:#2b2b2b"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></span>
                                          </label>
                                          <input type="password" name="password_confirm" class="form-control" id="password_confirm"
                                            placeholder="<?php echo lang('placeholder_confirm_password_text') ?>">
                                      </div>
                                      <!-- Only A super admin can force true factor on other admins , but a super admin cannot force tf on a super admin -->
                                      <?php
                                      if(
                                      $this->ion_auth->in_group('superadmin')
                                      && $this->ion_auth->in_group('admin',$user->id)
                                      && !$this->ion_auth->in_group('superadmin',$user->id)
                                      ){
                                       ?>
                                      <div class="form-group">
                                          <label for="password_confirm"><?php echo lang('label_google_tf_auth_forced_text') ?>
                                          <select name="google_tf_auth_forced" class="form-control" id="">
                                              <option value="0" <?= $user->google_tf_auth_forced == 0 ? "selected= 'selected' ":""; ?> >
                                                  <?php echo lang('no_text') ?>
                                              </option>
                                              <option value="1" <?= $user->google_tf_auth_forced == 1 ? "selected= 'selected' ":""; ?> >
                                                  <?php echo lang('yes_text') ?>
                                              </option>
                                          </select>
                                      </div>
                                      <?php } ?>
                                        <div class="form-group">
                                            <label for="groups">Groups</label>
                                            <?php foreach ($groups as $group) { ?>

                                                    <div
                                                        <?php
                                                        if($this->ion_auth->in_group('superadmin')){
                                                            if($group['id']==1 || $group['id']==2 ||  $group['id']==6 || $group['id']==7 ||$group['id'] == 9){ ?> style="display: none" <?php }
                                                        }else{
                                                            if($group['id']==1 || $group['id']==2 || $group['id']==3 ||  $group['id']==6 || $group['id']==7 || $group['id']== 9){ ?> style="display: none" <?php }
                                                        }
                                                        ?>
                                                    >
                                                        <label class="form-check-label">
                                                            <?php
                                                            $gID = $group['id'];
                                                            $checked = null;
                                                            $item = null;
                                                            foreach ($currentGroups as $grp) {
                                                                if ($gID == $grp->id) {
                                                                    $checked = ' checked="checked"';
                                                                    break;
                                                                }
                                                            }
                                                            ?>


                                                            <input class="form-check-input grp_chk" type="checkbox" class="" name="groups[]"
                                                                   value="<?php echo $group['id']; ?>"<?php echo $checked; ?>
                                                                > <!--do not show member group-->

                                                            <!--do not show member group-->
                                                            <?php  echo ucfirst( htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8') ); ?>

                                                        </label>
                                                    </div>

                                            <?php } ?>
                                        </div>
                                  </div>
                                  <!-- /.box-body -->

                                  <div class="box-footer">
                                        <button type="submit" id="btnsubmit" class="btn btn-primary update_user_button"><?php echo lang('button_submit_text') ?></button>
                                  </div>
                              </form>

                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>
        </div>

        <script>
            $(function () {
                    
                    $('#see_password').on('click',function () {
                        $('#password').attr('type', 'text');
                    });

                $('#see_password_confirm').on('click',function () {
                    $('#password_confirm').attr('type', 'text');
                });
                

            });
        </script>

