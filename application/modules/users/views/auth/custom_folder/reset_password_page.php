<base href="<?php echo base_url();?>">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/custom_asset/prosperis_favicon.ico">
    <!-- App title -->
    <title>Reset Password</title>
    <!-- Bootstrap CSS -->
    <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- App CSS -->
    <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Modernizr js -->
    <script src="assets/backend_assets/js/modernizr.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url().'style_module/load_style/office' ?>">

</head>
<body>
<div class="account-pages custom_login_backview"></div>
<div class="clearfix"></div>
<div class="wrapper-page custom-wrapper-page">
    <div class="account-bg">
        <div class="card-box mb-0">
            <div class="text-center m-t-20">
                <a href="<?= base_url()?>" class="logo">
                    <img style="max-width: 50%;" src="<?php echo $this->config->item('pg_upload_source_path').'image/' . $site_logo;?>">
                </a>
            </div>
            <div class="m-t-10 p-20">
                <div class="row">
                    <div class="col-12 text-center">
                        <h6 class="text-muted text-uppercase m-b-0 m-t-0"><?php echo lang('reset_password_heading') ?></h6>
                    </div>
                </div>

                <?php if ($message) { ?>
                    <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $message;?>
                    </div>
                <?php } ?>


                <form class="m-t-20" action="users/auth/reset_password/<?=$code;?>" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <input type="password" name="new" class="form-control" required
                                   placeholder="<?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?>"
                                   value="" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <input type="password" name="new_confirm" class="form-control" required
                                   placeholder="<?php echo sprintf(lang('reset_password_new_password_confirm_label'), $min_password_length);?>"
                                   value="" >
                        </div>
                    </div>

                    <input type="hidden" name="user_id" value="<?php print_r($user_id['value']) ?>">
                    <?php foreach ($csrf as $k=>$v) { ?>
                        <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>">
                    <?php } ?>


                    <div class="form-group text-center row m-t-10">
                        <div class="col-12">
                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit"><?php echo lang('reset_password_submit_btn')?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end wrapper page -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="assets/backend_assets/js/jquery.min.js"></script>
<script src="assets/backend_assets/js/popper.min.js"></script>Tether for Bootstrap
<script src="assets/backend_assets/js/bootstrap.min.js"></script>
<script src="assets/backend_assets/js/detect.js"></script>
<script src="assets/backend_assets/js/fastclick.js"></script>
<script src="assets/backend_assets/js/jquery.blockUI.js"></script>
<script src="assets/backend_assets/js/waves.js"></script>
<script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
<script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
<script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>
<!-- App js -->
<script src="assets/backend_assets/js/jquery.core.js"></script>
<script src="assets/backend_assets/js/jquery.app.js"></script>

</body>
</html>
