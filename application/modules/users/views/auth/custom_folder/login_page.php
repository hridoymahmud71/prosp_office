<base href="<?php echo base_url(); ?>">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/custom_asset/favicon.ico">
    <!-- App title -->
    <title>Administration – Prosperis Gold</title>
    <!-- Bootstrap CSS -->
    <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- App CSS -->
    <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css">
    <!-- Modernizr js -->
    <script src="assets/backend_assets/js/modernizr.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url() . 'style_module/load_style/office' ?>">

</head>
<body>
<div class="account-pages custom_login_backview"></div>
<div class="clearfix"></div>
<div class="wrapper-page custom-wrapper-page">
    <div class="account-bg">
        <div class="card-box mb-0">
            <div class="text-center m-t-20">
                <a href="<?= base_url() ?>" class="logo">
                    <img style="max-width: 50%;"
                         src="<?php echo $this->config->item('pg_upload_source_path') . 'image/' . $site_logo; ?>">
                    <!-- <span class="custom_logo_color">Prosperis Gold</span> -->
                </a>
            </div>
            <div class="m-t-10 p-20">
                <div class="row">
                    <div class="col-12 text-center">
                        <h6 class="text-muted text-uppercase m-b-0 m-t-0"><?php echo lang('sign_in_to_start_session_text') ?></h6>
                    </div>
                </div>

                <?php if ($this->session->flashdata('gtf_message')) { ?>
                    <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $this->session->flashdata('gtf_message'); ?>
                    </div>
                <?php } ?>
                <?php if ($message) { ?>
                    <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?php echo $message; ?>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['message'])) { ?>
                    <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <?= !empty($_GET['message']) ? $_GET['message'] : ""; ?>
                    </div>
                <?php } ?>

                <div id="tf_email_error_alert" style="display: none"
                     class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <span id="tf_email_error_alert_message"></span>
                </div>

                <form id="login_form" class="m-t-20" action="users/auth/login" method="post">
                    <div class="form-group row">
                        <div class="col-12">
                            <input id="identity" class="form-control" type="text" name="identity" required
                                   placeholder="<?php echo lang('login_identity_label') ?>"
                                   value="<?php if ($this->session->flashdata('identity')) {
                                       echo $this->session->flashdata('identity');
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <input class="form-control" type="password" name="password" required
                                   placeholder="<?php echo lang('login_password_label') ?>">
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="tf_modal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Google Two Factor Authentication</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <div class="col-12">
                                            <input class="form-control" type="text" name="code"
                                                   placeholder="Google Authentication code" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button id="tf_modal_btn" type="button" class="btn btn-primary">Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="checkbox checkbox-custom">
                                <input id="checkbox-signup" type="checkbox" name="remember" value="1">
                                <label for="checkbox-signup">
                                    <?php echo lang('login_remember_label') ?>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group text-center row m-t-10">
                        <div class="col-12">
                            <button id="login_form_btn" class="btn btn-primary btn-block waves-effect waves-light"
                                    type="button"><?php echo lang('login_submit_btn') ?></button>
                        </div>
                    </div>
                    <div class="form-group row m-t-30 mb-0">
                        <div class="col-12">
                            <a href="users/auth/forgot_password" class="text-muted"><i
                                        class="fa fa-lock m-r-5"></i><?php echo lang('login_forgot_password'); ?></a>
                        </div>
                    </div>
                    <!-- <div class="form-group row m-t-30 mb-0">
                        <div class="col-12 text-center">
                            <h5 class="text-muted"><b>Sign in with</b></h5>
                        </div>
                    </div>
                    <div class="form-group row mb-0 text-center">
                        <div class="col-12">
                            <button type="button" class="btn btn-facebook waves-effect font-14 waves-light m-t-20">
                               <i class="fa fa-facebook m-r-5"></i> Facebook
                            </button>

                            <button type="button" class="btn btn-twitter waves-effect font-14 waves-light m-t-20">
                               <i class="fa fa-twitter m-r-5"></i> Twitter
                            </button>

                            <button type="button" class="btn btn-googleplus waves-effect font-14 waves-light m-t-20">
                               <i class="fa fa-google-plus m-r-5"></i> Google+
                            </button>
                        </div>
                    </div> -->
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- end card-box-->
    <!-- <div class="m-t-20">
        <div class="text-center">
            <p class="text-white">Don't have an account? <a href="pages-register.html" class="text-white m-l-5"><b>Sign Up</b></a></p>
        </div>
    </div> -->
</div>
<!-- end wrapper page -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="assets/backend_assets/js/jquery.min.js"></script>
<script src="assets/backend_assets/js/popper.min.js"></script>
<script src="assets/backend_assets/js/bootstrap.min.js"></script>
<script src="assets/backend_assets/js/detect.js"></script>
<script src="assets/backend_assets/js/fastclick.js"></script>
<script src="assets/backend_assets/js/jquery.blockUI.js"></script>
<script src="assets/backend_assets/js/waves.js"></script>
<script src="assets/backend_assets/js/jquery.nicescroll.js"></script>
<script src="assets/backend_assets/js/jquery.scrollTo.min.js"></script>
<script src="assets/backend_assets/js/jquery.slimscroll.js"></script>
<script src="assets/backend_assets/plugins/switchery/switchery.min.js"></script>
<!-- App js -->
<script src="assets/backend_assets/js/jquery.core.js"></script>
<script src="assets/backend_assets/js/jquery.app.js"></script>

<script>
    $(function () {

        $(document).on('keypress', function (e) {
            if (e.which == 13) {
                $("#login_form_btn").click();
            }
        });

        $("#login_form_btn").on("click", function (e) {

            e.preventDefault();

            $.ajax({
                url: 'users/auth/ajax_check_tf_auth_is_needed',
                type: 'POST',
                data: {
                    'email': $("#identity").val()
                },
                dataType: 'json',
                success: function (resp) {
                    console.log(resp);


                    if (resp.need_email == true) {

                        $('#tf_modal').modal('hide');
                        $('#tf_email_error_alert').show();
                        $('#tf_email_error_alert_message').html(resp.message);

                    }
                    else if (resp.need_tf_auth == true) {

                        $('#tf_email_error_alert').hide();
                        $('#tf_email_error_alert_message').html('');
                        $('#tf_modal').modal('show');

                    } else {

                        $("#login_form").submit();


                    }



                },
                error: function (request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });


        });

        $("#tf_modal_btn").on("click", function (e) {
            $("#login_form").submit();

        })

    });
</script>


</body>
</html>