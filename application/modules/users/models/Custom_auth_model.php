<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev, RS Soft
 *
 * */

class Custom_auth_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function deleteUser($id)
    {
        $this->db->set('deletion_status', 1);
        $this->db->where('id', $id);
        $this->db->update('users');
    }

    public function countUsers($common_filter_value = false, $specific_filters = false)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');


        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');


        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'group') {
                    $specific_group_filter_flag = true; // flag logic not written yet

                    if ($filter_value == 'all') {
                        $this->db->where('ug.group_id', 2); // member group_id 2
                    } else {
                        $this->db->where('ug.group_id', $filter_value);
                    }

                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }


        } else {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        if ($specific_group_filter_flag == false) {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getUsers($common_filter_value = false, $specific_filters = false, $order, $limit)
    {

        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');

        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');

        $specific_group_filter_flag = false; // flag logic not written yet
        if ($common_filter_value != false) {
            $this->db->group_start();
            $this->db->like('u.first_name', $common_filter_value);
            $this->db->or_like('u.last_name', $common_filter_value);
            $this->db->or_like('u.email', $common_filter_value);
            $this->db->group_end();
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'first_name' || $column_name == 'last_name' || $column_name == 'email') {
                    $this->db->like('u.' . $column_name, $filter_value);
                }

                if ($column_name == 'group') {
                    $specific_group_filter_flag = true; // flag logic not written yet

                    if ($filter_value == 'all') {
                        $this->db->where('ug.group_id', 2); // member group_id 2
                    } else {
                        $this->db->where('ug.group_id', $filter_value);
                    }

                }

                if ($column_name == 'active') {
                    if ($filter_value == 'yes') {
                        $this->db->where('u.active', 1);
                    } else {
                        $this->db->where('u.active!=', 1);
                    }

                }
            }


        } else {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }

        if ($specific_group_filter_flag == false) {
            $this->db->where('ug.group_id', 2); // member group_id 2
        }


        $this->db->order_by('u.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();


        $result = $query->result();


        return $result;
    }


    public function getGroups()
    {
        $this->db->select('*');
        $this->db->from('groups');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function insertUserInAGroup($ins_data)
    {
        $this->db->insert('users_groups', $ins_data);
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function removeStaffFromProjects($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('rspm_tbl_project_assigned_staff');
    }

    public function removeStaffFromTasks($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        $this->db->delete('rspm_tbl_task_assigned_staff');
    }

    public function countTotalEmployerBySelect2($keyword)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); //employer is 6

        $this->db->group_start();
        $this->db->like('u.company', $keyword);
        $this->db->or_like('u.email', $keyword);
        $this->db->group_end();

        $query = $this->db->get();

        $num_rows = $query->num_rows();

        return $num_rows;
    }

    public function getTotalEmployerBySelect2($keyword, $limit, $offset)
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
        ');
        $this->db->from('users as u');
        $this->db->where('u.deletion_status!=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 6); //employer is 6

        $this->db->group_start();
        $this->db->like('u.company', $keyword);
        $this->db->or_like('u.email', $keyword);
        $this->db->group_end();

        $this->db->limit($limit, $offset);

        $query = $this->db->get();

        $result = $query->result();

        return $result;
    }


    public function setEmployerForEmployee($employer_id, $employee_id)
    {
        $this->db->set('user_employer_id', $employer_id);
        $this->db->where('user_id', $employee_id);
        $this->db->update('rspm_tbl_user_details');
    }

    public function checkIfMemIdNumExists($mem_id_num)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.mem_id_num', $mem_id_num);

        $query = $this->db->get();

        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getBanks($only_undeleted = false)
    {
        $this->db->select('*');
        $this->db->from('pg_bank');

        if($only_undeleted){
            $this->db->where('bank_deletion_status!=',1);
        }
        $this->db->order_by('bank_name');

        $query = $this->db->get();
        $num_rows = $query->result();
        return $num_rows;
    }

    public function insert_employee_credential($data1)
    {
        $this->db->insert('users', $data1);
        return $this->db->insert_id();
    }

    public function insert_employee_optional_credential($data2)
    {
        $this->db->insert('rspm_tbl_user_details', $data2);
    }

    public function insert_as_group_member($grp_mem)
    {
        $this->db->insert('users_groups',$grp_mem);
    }

    public function ifEmailExist($email)
    {
        $this->db->select('email');
        $this->db->from('users');
        $this->db->where('email',$email);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if($num_rows > 0){
            return true;
        }else{
            return false;
        }

    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id',$user_id);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }

    public function getUserDetails($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id',$user_id);
        $this->db->join('rspm_tbl_user_details as ud','u.id=ud.user_id');

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }


    public function getUserByEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.email',$email);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }

    public function verifyUser($id)
    {
        $this->db->set('deletion_status', 0);
        $this->db->set('verification', 1);
        $this->db->where('id', $id);
        $this->db->update('users');
    }

    public function activateUser($id)
    {
        $this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update('users');
    }

    public function getEmailTempltateByType($type)
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        $this->db->where('email_template_type',$type);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getActiveNonBotAdminsIds()
    {
        $this->db->select('u.id as user_id');
        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->where('u.is_user_bot', 0);
        $this->db->where('u.deletion_status !=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id =', 1);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $result = $query->result();
        return $result;
    }

    public function updateNewPassword($hashed_password,$user_id)
    {
        $this->db->set('password', $hashed_password);
        $this->db->where('id', $user_id);
        $this->db->update('users');
    }
	
	public function getExternalCustomProductInvitationMemberInMultiRow($email)
    {
        $this->db->select('*');
        $this->db->from('pg_custom_product_invited_members as cpim');

        $this->db->where('cpim.cpi_external_member', 1);
        $this->db->where('cpim.cpi_external_member_email', $email);

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function updateInvitationMember($data,$serial)
    {
        $this->db->where('cpi_inv_mem_serial', $serial);
        $this->db->update('pg_custom_product_invited_members', $data);

    }

    public function insertMessage($ins_data)
    {
        $this->db->insert('pg_message', $ins_data);

        return $this->db->insert_id();
    }

    public function insertMessageReciever($ins_data)
    {
        $this->db->insert('pg_message_reciever', $ins_data);
    }

    public function insertMessageComment($ins_data)
    {
        $this->db->insert('pg_message_comment', $ins_data);

        return $this->db->insert_id();
    }

    public function doesMessageNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_message');
        $this->db->where('message_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }


}