<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Modified By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        /* extra line <starts> */
        $this->Utility_model->setTargetInACookie();
        $this->Utility_model->checkMaintenanceMode();
        /* extra line <ends> */

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));

        $this->load->library('custom_log_library');

        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->lang->load('users');

        $this->load->model('Custom_auth_model');

        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('GoogleAuthenticator');

        /*for test  purpose : remove code in between*/
        set_time_limit(0);
        ini_set('MAX_EXECUTION_TIME', 3600);
        /*for test  purpose : remove code in between*/

    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            /*redirect('auth/login', 'refresh');*/

            /*Redirect above is changed for HMVC Conversion
            see:  http://dmitriykravchuk.co.za/blog/2015/10/30/codeigniter-3-hmvc-ion-auth/
            */
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {      // remove this if if you want to enable this for non-admins

            // redirect them to the home page because they must be an administrator to view this

            //originally
            //return show_error('You must be an administrator to view this page.');

            redirect('users/auth/need_permission');
        } else {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');


            // application/libraries
            // this segment us added for customized datetime
            $this->load->library('custom_datetime_library');

            //original demo
            //$this->_render_page('users/auth/index', $this->data);

            //print_r($this->data);die();
            $this->data['group_list'] = $this->Custom_auth_model->getGroups();

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/users_page", $this->data);
            $this->load->view("common_module/footer");

        }
    }

    public function getUsersByAjax()
    {
        $users = array();

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'first_name';
        $columns[1] = 'last_name';
        $columns[2] = 'email';
        $columns[3] = 'group';
        $columns[4] = 'active';
        $columns[5] = 'created_on';
        $columns[6] = 'last_login';
        $columns[7] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['first_name'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['last_name'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['email'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['group'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['active'] = $requestData['columns'][4]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Custom_auth_model->countUsers(false, false);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Custom_auth_model->countUsers($common_filter_value, $specific_filters);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $users = $this->Custom_auth_model->getUsers($common_filter_value, $specific_filters, $order, $limit);

        if ($users == false || empty($users) || $users == null) {
            $users = false;
        }

        $last_query = $this->db->last_query();

        if ($users) {
            foreach ($users as $k => $user) {
                $user_groups = $this->ion_auth->get_users_groups($user->id)->result();
                $users[$k]->groups = '';

                $iter = 0;
                foreach ($user_groups as $a_group) {

                    /*if ($iter != 0) {
                        $users[$k]->groups .= '<br>';
                    }*/

                    /*do not show member group */
                    if (!($a_group->id == 1 || $a_group->id == 2)) {
                        if ($a_group->name == 'employee') {
                            $users[$k]->groups .= 'Thrifter';
                        } else if ($a_group->name == 'employer') {
                            $users[$k]->groups .= 'Organization';
                        } else {
                            $users[$k]->groups .= ucfirst(str_replace('_', ' ', $a_group->name));;
                        }
                    }

                    $iter++;
                }
            }
        }

        $this->load->library('custom_datetime_library');


        if ($users) {
            $i = 0;
            foreach ($users as $a_user) {

                /*date time starts*/
                $users[$i]->cr_on = new stdClass();
                $users[$i]->lt_lg_in = new stdClass();

                $users[$i]->cr_on->timestamp = $a_user->created_on;
                $users[$i]->lt_lg_in->timestamp = $a_user->last_login;

                if ($a_user->created_on == 0) {
                    $users[$i]->cr_on->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $users[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->created_on);
                }

                if ($a_user->last_login == 0) {
                    $users[$i]->lt_lg_in->display = $this->lang->line('never_logged_in_text');
                } else {
                    $users[$i]->lt_lg_in->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_user->last_login);
                }
                /*date time ends*/

                /*active - inactive starts*/
                $users[$i]->act = new stdClass();
                $users[$i]->act->int = $a_user->active;

                if ($a_user->active == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'users/auth/deactivateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'users/auth/activateUser/' . $a_user->id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                if ($this->ion_auth->is_admin()) {
                    $users[$i]->act->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                } else {
                    $users[$i]->act->html = $status_span;
                }

                /*active - inactive ends*/

                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'user_profile_module/user_profile_overview/' . $a_user->id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $message_tooltip = $this->lang->line('tooltip_message_text');
                $message_url = base_url() . 'message' . $a_user->id; //undefined
                $message_anchor = '<a ' . ' title="' . $message_tooltip . '" ' . ' href="' . $message_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'users/auth/edit_user/' . $a_user->id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'users/auth/deleteUser/' . $a_user->id;
                $delete_anchor = '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true">'
                    . '</a>';

                $password_tooltip = $this->lang->line('tooltip_password_text');
                $password_url = base_url() . 'users/auth/sendPasswordViaMail/' . $a_user->id;
                $password_anchor = '<a ' . ' title="' . $password_tooltip . '" ' . ' href="' . $password_url . '" ' . ' class="password_confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-key fa-lg" aria-hidden="true">'
                    . '</a>';

                if (!$this->ion_auth->in_group('superadmin') && $this->ion_auth->is_admin($a_user->id)) {
                    $password_anchor = '';
                }

                if ($this->ion_auth->is_admin()) {
                    $users[$i]->action = $view_anchor . '&nbsp;&nbsp;' . $edit_anchor . '&nbsp;&nbsp;' . '&nbsp;&nbsp;' . $password_anchor;
                    /*action ends*/
                } else {
                    $users[$i]->action = $view_anchor; //message is not defined yet
                    /*action ends*/
                }


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$users = $this->removeKeys($users); // converting to numeric indices.
        $json_data['data'] = $users;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }


    // log the user in
    public function login()
    {
        $ga = new GoogleAuthenticator();

        $this->data['title'] = $this->lang->line('login_heading');

        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required',
            array(
                'required' => $this->lang->line('identity_required')
            )
        );

        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required',
            array(
                'required' => $this->lang->line('password_required')
            )
        );

        if ($this->form_validation->run() == true) {

            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool)$this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page

                //only admin can pass through here
                if (!$this->ion_auth->is_admin()) {
                    $this->session->set_flashdata('message', $this->lang->line('only_admin_can_login_text'));
                    redirect('users/auth/login');
                }

                $user = $this->Custom_auth_model->getUser($this->session->userdata('user_id'));

                if ($user) {
                    if ($user->google_tf_auth_status == 2 && $user->google_tf_secret_code != null && $user->google_tf_secret_code != '') {
                        $googleTfValidated_arr = $this->googleTfValidated($this->input->post(), $ga, $user->google_tf_secret_code);

                        $gtf_message = $googleTfValidated_arr['message'];

                        if ($googleTfValidated_arr['res'] == 'no') {
                            $this->session->set_flashdata('gtf_message', $gtf_message);

                            redirect("users/auth/login");
                        }
                    }

                }


                $this->session->set_flashdata('message', $this->ion_auth->messages());

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'login_logout',                                                         //3.    $type
                    '',                                                                     //4.    $type_id
                    'logged_in',                                                            //5.    $activity
                    'admin',                                                                //6.    $activity_by
                    'admin',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/


                /*force to go to tf auth set up page <starts> */
                if ($user) {
                    $tf_cond_1 = !$this->ion_auth->in_group('superadmin') && $this->ion_auth->in_group('admin');
                    $tf_cond_2 = $user->google_tf_auth_forced == 1 && empty($user->google_tf_secret_code);

                    if ($tf_cond_1 && $tf_cond_2) {
                        redirect("user_profile_module/google_tf_auth/$user->id");
                    }

                }
                /*force to go to tf auth set up page <ends> */

                /*if cookie has a path redirect there <starts>*/
                $cookie_name = 'redirect_path_after_login';
                if (isset($_COOKIE[$cookie_name])) {

                    $rediect_from_cookie = $_COOKIE[$cookie_name];
                    unset($_COOKIE[$cookie_name]);
                    setcookie($cookie_name, '', time() - 36000, '/');

                    redirect($rediect_from_cookie);
                }
                /*if cookie has a path redirect there <ends>*/

                redirect('common_module', 'refresh');
            } else {
                // if the login was un-successful
                // redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/login'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //set email or username in flash so that
            $this->session->set_flashdata('identity', $this->input->post('identity'));

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
            );

            $settings_code = 'general_settings';
            $office_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'office_site_logo');

            if ($office_site_logo == '' || $office_site_logo == null || $office_site_logo == false) {
                $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
            } else {
                $this->data['site_logo'] = $office_site_logo;
            }


            /*$this->_render_page('users/auth/login', $this->data);*/

            $this->_render_page('users/auth/custom_folder/login_page', $this->data);
        }
    }

    public function ajax_check_tf_auth_is_needed()
    {
        $user = $this->Custom_auth_model->getUserByEmail($_POST['email']);

        $resp = array();
        $resp['need_email'] = false;
        $resp['need_tf_auth'] = false;
        $resp['message'] = "";

        if (empty($_POST['email'])) {
            $resp['need_email'] = true;
            $resp['need_tf_auth'] = false;
            $resp['message'] = "Enter an email first";
        }
        if ($user) {
            if ($user->google_tf_auth_status == 2 && $user->google_tf_secret_code != null && $user->google_tf_secret_code != '') {
                $resp['need_tf_auth'] = true;
                $resp['message'] = "Show tf modal";
            }

        }

        echo json_encode($resp);
        exit;
    }

    public function googleTfValidated($post, $ga, $secret)
    {
        $error_message = '';
        $res = "no";
        $code = $post['code'];
        $message = "gg";
        if ($code == "") {
            $message = 'Please enter authentication code to validated!';
        } else {
            if ($ga->verifyCode($secret, $code, 2)) {
                // success
                $message = "Success";
                $res = "yes";
            } else {
                // fail
                $message = 'Invalid Authentication Code!';
            }
        }

        $data = array();
        $data['message'] = $message;
        $data['res'] = $res;

        return $data;

    }

    // log the user out
    public function logout()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $this->data['title'] = "Logout";

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'login_logout',                                                         //3.    $type
            '',                                                                     //4.    $type_id
            'logged_out',                                                           //5.    $activity
            'admin',                                                                //6.    $activity_by
            'admin',                                                                //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        // log the user out
        $logout = $this->ion_auth->logout();

        // redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('users/auth/login', 'refresh');
    }

    // change password
    public function change_password()
    {
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            // display the form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $this->data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            // render
            $this->_render_page('users/auth/change_password', $this->data);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/change_password', 'refresh');
            }
        }
    }

    // forgot password
    public function forgot_password()
    {
        /*extra line st*/
        $settings_code = 'general_settings';
        $office_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'office_site_logo');

        if ($office_site_logo == '' || $office_site_logo == null || $office_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $office_site_logo;
        }
        /*extra line en*/

        // setting validation rules by checking whether identity is username or email
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }


        if ($this->form_validation->run() == false) {
            $this->data['type'] = $this->config->item('identity', 'ion_auth');
            // setup the input
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
            );

            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            // set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            //$this->_render_page('users/auth/forgot_password', $this->data);
            $this->_render_page('users/auth/custom_folder/forgot_password_page', $this->data);
        } else {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            if (empty($identity)) {

                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("users/auth/forgot_password", 'refresh');
            }

            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                // if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                //redirect("users/auth/forgot_password", 'refresh');
                redirect("users/auth/forgot_password", 'refresh');
            }
        }
    }

    // reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        if (!$code) {
            show_404();
        }

        /*extra line st*/
        $settings_code = 'general_settings';
        $office_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'office_site_logo');

        if ($office_site_logo == '' || $office_site_logo == null || $office_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $office_site_logo;
        }
        /*extra line en*/

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            // if the code is valid then display the password reset form

            //original rules
            //$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            //$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');
            //re-written rule
            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|callback_validate_password');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required|matches[new]');


            if ($this->form_validation->run() == false) {
                // display the form

                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );

                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                // render
                //$this->_render_page('users/auth/reset_password', $this->data);
                $this->_render_page('users/auth/custom_folder/reset_password_page', $this->data);
            } else {
                // do we have a valid request?

                //if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) { //orginally
                if ($user->id != $this->input->post('user_id')) {
                    // something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error($this->lang->line('error_csrf'));

                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("users/auth/login", 'refresh');
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        //redirect('users/auth/reset_password/' . $code, 'refresh');
                        redirect('users/auth/reset_password/' . $code, 'refresh');

                    }
                }
            }
        } else {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/auth/forgot_password", 'refresh');
        }
    }

    function validate_password($str)
    {
        if (preg_match("/^(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,32}$/", $str) !== 0) {
            return true;
        } else {
            $this->form_validation->set_message("validate_password", $this->lang->line('strong_password_text'));
            return false;
        }
    }


    // activate the user
    public function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->ion_auth->is_admin()) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            // redirect them to the auth page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("users/auth", 'refresh');
        } else {
            // redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/auth/forgot_password", 'refresh');
        }
    }

    // deactivate the user
    public function deactivate($id = NULL)
    {
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            // redirect them to the home page because they must be an administrator to view this

            //oruginal
            //return show_error('You must be an administrator to view this page.');

            redirect('users/auth/need_permission');
        }

        $id = (int)$id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {
            // insert csrf check
            $this->data['csrf'] = $this->_get_csrf_nonce();
            $this->data['user'] = $this->ion_auth->user($id)->row();

            $this->_render_page('users/auth/deactivate_user', $this->data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {
                    show_error($this->lang->line('error_csrf'));
                }

                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
                    $this->ion_auth->deactivate($id);
                }
            }

            // redirect them back to the auth page
            redirect('users/auth', 'refresh');
        }
    }

    // create a new user
    public function create_user()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        //echo '<pre>'; print_r($_POST);die();


        $this->lang->load('create_user');

        $this->data['all_groups'] = $this->Custom_auth_model->getGroups();

        $this->data['title'] = $this->lang->line('create_user_heading');

        /*if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('users/auth', 'refresh');
        }*/

        $tables = $this->config->item('tables', 'ion_auth');
        $identity_column = $this->config->item('identity', 'ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('label_firstname_text'), 'required',
            array(
                'required' => $this->lang->line('firstname_required_text')
            )
        );
        $this->form_validation->set_rules('last_name', $this->lang->line('label_lastname_text'), 'required',
            array(
                'required' => $this->lang->line('lastname_required_text')
            )
        );
        if ($identity_column !== 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('identity_text'), 'required|is_unique[' . $tables['users'] . '.' . $identity_column . ']',
                array(
                    'required' => $this->lang->line('identity_required_text'),
                    'is_unique' => $this->lang->line('identity_not_unique_text')
                )
            );
            $this->form_validation->set_rules('email', $this->lang->line('label_email_text'), 'required|valid_email',
                array(
                    'required' => $this->lang->line('email_required_text'),
                    'valid_email' => $this->lang->line('identity_not_unique_text')
                )
            );
        } else {
            $this->form_validation->set_rules('email', $this->lang->line('label_email_text'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]',
                array(
                    'required' => $this->lang->line('email_required_text'),
                    'valid_email' => $this->lang->line('email_not_valid_text'),
                    'is_unique' => $this->lang->line('identity_not_unique_text')
                )
            );
        }
        $this->form_validation->set_rules('phone', $this->lang->line('label_phone_text'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('label_company_name_text'), 'trim');

        $this->form_validation->set_rules('password', $this->lang->line('label_password_text'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]',
            array(
                'required' => $this->lang->line('password_required_text'),
                'min_length' => $this->lang->line('password_min_length_text') . $this->config->item('min_password_length', 'ion_auth'),
                'max_length' => $this->lang->line('password_max_length_text') . $this->config->item('max_password_length', 'ion_auth'),
                'matches' => $this->lang->line('password_not_match_text')
            )
        );

        $this->form_validation->set_rules('password_confirm', $this->lang->line('label_confirm_password_text'), 'required',
            array(
                'required' => $this->lang->line('confirm_password_required_text')
            )
        );

        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
            $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $first_name = ucwords($first_name, " \t\r\n\f\v-");
            $last_name = ucwords($last_name, " \t\r\n\f\v-");

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
                'mem_id_num' => $this->getMemIdNum($this->input->post('first_name'), $this->input->post('last_name')),
            );
        }


        /*slighty moderated the code below #see orignal if anything goes wrong

        #originally $if_registered didn't exist. code looked like:

            if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data)
                {
                    //code here
                    if($if_registered >0 {} #this block didn't exist
                    //code here
                }
       */


        if ($this->form_validation->run() == true &&
            $register_returned = $this->ion_auth->register($identity, $password, $email, $additional_data)
        ) {

            // check to see if we are creating the user

            /*$this->ion_auth->register($identity, $password, $email, $additional_data);
            retutns last created id of user if succsessfull*/

            $this->load->model('user_profile_module/Userprofile_model');

            if ($this->Userprofile_model->checkUserIn_UserDetailsTBL($register_returned) == true) {
                $this->Userprofile_model->insertUserIdIn_UserDetailsTBL($register_returned);
            }

            if ($this->input->post('select_group')) {
                $group_id = $this->input->post('select_group');

                if ($group_id != 0) {
                    $ins_data['user_id'] = $register_returned;
                    $ins_data['group_id'] = $group_id;
                    $this->Custom_auth_model->insertUserInAGroup($ins_data);

                    //if group is superadmin/administrator/analyst also add in admin
                    if ($group_id == 3 || $group_id == 4 || $group_id == 8) {
                        $ins_data['user_id'] = $register_returned;
                        $ins_data['group_id'] = 1;
                        $this->Custom_auth_model->insertUserInAGroup($ins_data);
                    }
                }

            }

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'user',                                                                 //3.    $type
                $register_returned,                                                     //4.    $type_id
                'created',                                                              //5.    $activity
                'admin',                                                                //6.    $activity_by
                'admin',                                                                //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->snd_pw_link_v_mail($ins_data['user_id']);

            // redirect them back to the login page
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('flash_user_id', $register_returned);
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            $this->session->set_flashdata('add_success', $this->ion_auth->messages());
            redirect("users/auth", 'refresh');
        } else {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name' => 'company',
                'id' => 'company',
                'type' => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            /*keepping the value in the fileds after getting val_errors*/
            $this->session->set_flashdata('first_name', $this->input->post('first_name'));
            $this->session->set_flashdata('last_name', $this->input->post('last_name'));
            $this->session->set_flashdata('email', $this->input->post('email'));
            $this->session->set_flashdata('phone', $this->input->post('phone'));
            $this->session->set_flashdata('company', $this->input->post('company'));


            //original demo
            //$this->_render_page('users/auth/create_user', $this->data);

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/create_user_page", $this->data);
            $this->load->view("common_module/footer");
        }
    }

    private function getMemIdNum($f_name, $l_name)
    {
        if (!$f_name) {
            $f_name = 'X';
        } else {
            $f_name = ucfirst($f_name[0]);
        }

        if (!$l_name) {
            $l_name = 'X';
        } else {
            $l_name = ucfirst($l_name[0]);
        }

        $mem_id_num = 'M' . $this->alphaNum(5, false, true) . $f_name . $l_name;

        $exists = $this->Custom_auth_model->checkIfMemIdNumExists($mem_id_num);

        if ($exists) {
            $x = $this->getMemIdNum($f_name, $l_name);
        } else {
            return $mem_id_num;
        }
        return $x;


    }

    private function alphaNum($length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }

        $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $integers = '0123456789';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public
    function getEmployersBySelect2()
    {

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Custom_auth_model->countTotalEmployerBySelect2($keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $employers = $this->Custom_auth_model->getTotalEmployerBySelect2($keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($employers) {

            foreach ($employers as $an_employer) {
                $p = array();
                $p['id'] = $an_employer->id;
                $p['text'] =
                    $an_employer->company;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Employer Found';

            $items = $p;
            $json_data['items'][] = $items;
        }

        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    /*custom func starts*/
    public
    function getGroupName($group_id)
    {
        $group_info = $this->ion_auth_model->group($group_id)->row();

        if ($group_info) {
            return $group_info->name;
        }
    }

    /*custom func starts*/

    // edit a user
    public
    function edit_user($id)
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        //non super admin cannot edit super admin
        if ($this->ion_auth->in_group('superadmin', $id) && !$this->ion_auth->in_group('superadmin')) {
            redirect('users/auth/need_permission');
        }

        //analyst cannot edit any type of admin
        if (($this->ion_auth->in_group('admin', $id))
            && $this->ion_auth->in_group('analyst')) {
            redirect('users/auth/need_permission');
        }

        $this->lang->load('edit_user');


        $this->data['title'] = $this->lang->line('edit_user_heading');

        /*if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('users/auth', 'refresh');
        }*/

        $user = $this->ion_auth->user($id)->row();
        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();

        // validate form input

        if ($this->ion_auth->in_group('employer', $id)) {
            $this->form_validation->set_rules('company', $this->lang->line('label_company_text'), 'required', array(
                    'required' => $this->lang->line('company_required_text')
                )
            );
        } else {
            $this->form_validation->set_rules('first_name', $this->lang->line('label_firstname_text'), 'required',
                array(
                    'required' => $this->lang->line('firstname_required_text')
                )
            );
            $this->form_validation->set_rules('last_name', $this->lang->line('label_lastame_text'), 'required',
                array(
                    'required' => $this->lang->line('lastname_required_text')
                )
            );
        }


        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?

            //originally
            /*if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id')) {

                show_error($this->lang->line('error_csrf'));
            }*/

            if ($id != $this->input->post('id')) {

                show_error($this->lang->line('error_csrf'));
            }

            // update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('label_password_text'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]',
                    array(
                        'required' => $this->lang->line('password_required_text'),
                        'min_length' => $this->lang->line('password_min_length_text') . $this->config->item('min_password_length', 'ion_auth'),
                        'max_length' => $this->lang->line('password_max_length_text') . $this->config->item('max_password_length', 'ion_auth'),
                        'matches' => $this->lang->line('password_not_match_text')
                    )
                );
                $this->form_validation->set_rules('password_confirm', $this->lang->line('label_confirm_password_text'), 'required',
                    array(
                        'required' => $this->lang->line('confirm_password_required_text')
                    )
                );
            }

            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $first_name = ucwords($first_name, " \t\r\n\f\v-");
            $last_name = ucwords($last_name, " \t\r\n\f\v-");

            if ($this->form_validation->run() === TRUE) {
                $data = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                );

                // update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                //---------- force tf auth to 'other' admins -----------<starts>
                if ($this->input->post('google_tf_auth_forced')) {

                    //echo "[{$_POST['google_tf_auth_forced']}]";
                    $this->tf_force_update($id, $this->input->post('google_tf_auth_forced'));
                }
                //---------- force tf auth to 'other' admins -----------<starts>


                // Only allow updating groups if user is admin
                if ($this->ion_auth->is_admin()) {
                    //Update the groups user belongs to
                    $groupData = $this->input->post('groups');

                    if (isset($groupData) && !empty($groupData)) {

                        /*custom code starts*/


                        /*logic written for prosperious gold  thrifting*/
                        $admin_checked = false;

                        $superadmin_checked = false;
                        $administrator_checked = false;
                        $analyst_checked = false;

                        $trustee_checked = false;
                        $employer_checked = false;
                        $employee_checked = false;
                        $org_contact_checked = false;

                        $checked_group_arr = array();

                        if ((in_array(1, $groupData))) {
                            $admin_checked = true;                      //admin group = 1
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(1)));
                        }

                        if ((in_array(3, $groupData))) {
                            $superadmin_checked = true;                 //superadmin = 3
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(3)));
                        }

                        if ((in_array(4, $groupData))) {
                            $administrator_checked = true;              //administrator = 4
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(4)));
                        }

                        if ((in_array(5, $groupData))) {
                            $trustee_checked = true;                    //trustee group = 5
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(5)));
                        }

                        if ((in_array(6, $groupData))) {
                            $employer_checked = true;                   //employer group = 6
                            /*$checked_group_arr[] = ucfirst(str_replace('_',' ',$this->getGroupName(6)) );*/
                            $checked_group_arr[] = 'Organization';
                        }

                        if ((in_array(7, $groupData))) {
                            $employee_checked = true;                   //employee group = 7
                            /*$checked_group_arr[] = ucfirst(str_replace('_',' ',$this->getGroupName(7)) );*/
                            $checked_group_arr[] = 'Thrifter';
                        }

                        if ((in_array(8, $groupData))) {
                            $analyst_checked = true;                    //analyst = 8
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(8)));
                        }

                        if ((in_array(9, $groupData))) {
                            $org_contact_checked = true;                //organization_contact group = 9
                            $checked_group_arr[] = ucfirst(str_replace('_', ' ', $this->getGroupName(9)));
                        }

                        $redirect = false;

                        if ($superadmin_checked || $administrator_checked || $analyst_checked) {
                            if (!in_array(1, $groupData)) {
                                $groupData[] = 1;
                                $admin_checked = true;
                            }
                        } else {
                            if (in_array(1, $groupData)) {
                                $key_here = array_search(1, $groupData);
                                unset($groupData[$key_here]);
                                $admin_checked = false;
                            }
                        }

                        if (
                            $superadmin_checked && ($administrator_checked || $analyst_checked)
                            ||
                            $administrator_checked && ($superadmin_checked || $analyst_checked)
                            ||
                            $analyst_checked && ($superadmin_checked || $administrator_checked)
                        ) {
                            $redirect = true;
                        }

                        if (
                            $admin_checked && ($trustee_checked || $employer_checked || $employee_checked || $org_contact_checked)
                            ||
                            $trustee_checked && ($admin_checked || $employer_checked || $employee_checked || $org_contact_checked)
                            ||
                            $employer_checked && ($admin_checked || $trustee_checked || $employee_checked || $org_contact_checked)
                            ||
                            $employee_checked && ($admin_checked || $trustee_checked || $employer_checked || $org_contact_checked)
                            ||
                            $org_contact_checked && ($admin_checked || $trustee_checked || $employer_checked || $employee_checked)
                        ) {
                            $redirect = true;
                        }

                        if ($redirect) {
                            $string_of_groupnames = '';
                            if ($checked_group_arr) {
                                $string_of_groupnames = implode(',', $checked_group_arr);
                            }

                            $group_selection_error_pg_text = sprintf($this->lang->line('group_selection_error_pg_thrift_text'), $string_of_groupnames);
                            $this->session->set_flashdata('group_selection_error_pg_thrift', $group_selection_error_pg_text);

                            redirect('users/auth/edit_user/' . $id);

                        }


                        /*custom code ends*/

                        $this->ion_auth->remove_from_group('', $id);


                        foreach ($groupData as $grp) {
                            $this->ion_auth->add_to_group($grp, $id);
                        }

                    }
                }

                // check to see if we are updating the user
                if ($this->ion_auth->update($user->id, $data)) {

                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        $this->session->userdata('user_id'),                                    //1.    $created_by
                        '',                                                                     //2.    $created_for
                        'user',                                                                 //3.    $type
                        $user->id,                                                              //4.    $type_id
                        'updated',                                                              //5.    $activity
                        'admin',                                                                //6.    $activity_by
                        'admin',                                                                //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        '',                                                                     //10.   $super_type
                        '',                                                                     //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                    /*creating log ends*/

                    // redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('message', $this->ion_auth->messages());
                    if ($this->ion_auth->is_admin()) {
                        $this->session->set_flashdata('success', 'success');
                        $this->session->set_flashdata('flash_user_id', $user->id);
                        $this->session->set_flashdata('update_success', 'update_success');
                        redirect('users/auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    // redirect them back to the admin page if admin, or to the base url if non admin

                    $this->session->set_flashdata('message', $this->ion_auth->errors());

                    if ($this->ion_auth->is_admin()) {
                        redirect('users/auth', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
        }

        // display the edit user form
        $this->data['csrf'] = $this->_get_csrf_nonce();
        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['user'] = $user;
        $this->data['groups'] = $groups;
        $this->data['currentGroups'] = $currentGroups;

        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
        );
        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
        );
        $this->data['company'] = array(
            'name' => 'company',
            'id' => 'company',
            'type' => 'text',
            'value' => $this->form_validation->set_value('company', $user->company),
        );
        $this->data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'text',
            'value' => $this->form_validation->set_value('phone', $user->phone),
        );
        $this->data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password'
        );
        $this->data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password'
        );


        // original page
        //$this->_render_page('users/auth/edit_user', $this->data);

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/edit_user_page", $this->data);
        $this->load->view("common_module/footer");
    }

    private function tf_force_update($id, $google_tf_auth_forced)
    {

        $upd_data = array();
        $upd_data['google_tf_auth_forced'] = $google_tf_auth_forced;


        $user = $this->Custom_auth_model->getUser($id);

        if (($user)) {

            if (!empty($user->google_tf_secret_code)) {
                if ((int)$google_tf_auth_forced == 1) {
                    $upd_data['google_tf_auth_status'] = 2;
                }
            }


            $this->db->where('id', $id);
            $this->db->update('users', $upd_data);
        }


    }

    // create a new group
    public
    function create_group()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('create_group');

        $this->data['title'] = $this->lang->line('create_group_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('users/auth', 'refresh');
        }

        // validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('label_group_name_text'), 'required|alpha_dash|is_unique[groups.name]',
            array(
                'required' => $this->lang->line('group_name_required_text'),
                'alpha_dash' => $this->lang->line('group_name_alpha_dash_text'),
                'is_unique' => $this->lang->line('group_name_not_unique_text')
            )
        );

        if ($this->form_validation->run() == TRUE) {
            $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
            if ($new_group_id) {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->session->set_flashdata('group_add_success', 'group_add_success');
                redirect("users/auth/show_user_groups", 'refresh');
            }
        } else {
            // display the create group form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $this->data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'value' => $this->form_validation->set_value('description'),
            );

            //originally
            //$this->_render_page('users/auth/create_group', $this->data);

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/create_group_page", $this->data);
            $this->load->view("common_module/footer");

        }
    }

    // edit a group
    public
    function edit_group($id)
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('edit_group');

        // bail if no group id given
        if (!$id || empty($id)) {
            redirect('users/auth', 'refresh');
        }

        $this->data['title'] = $this->lang->line('edit_group_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('users/auth', 'refresh');
        }

        $group = $this->ion_auth->group($id)->row();

        // validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash',
            array(
                'required' => $this->lang->line('group_name_required_text'),
                'alpha_dash' => $this->lang->line('group_name_alpha_dash_text')

            )
        );

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update) {
                    //$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
                    $this->session->set_flashdata('success', 'success');
                    $this->session->set_flashdata('group_update_success', 'group_update_success');
                } else {
                    $this->session->set_flashdata('message', $this->ion_auth->errors());
                    redirect("users/auth/edit_group/" . $id, 'refresh');
                }
                redirect("users/auth/show_user_groups", 'refresh');
            }
        }

        // set the flash data error message if there is one
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        // pass the user to the view
        $this->data['group'] = $group;

        $readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

        $this->data['group_name'] = array(
            'name' => 'group_name',
            'id' => 'group_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_name', $group->name),
            $readonly => $readonly,
        );
        $this->data['group_description'] = array(
            'name' => 'group_description',
            'id' => 'group_description',
            'type' => 'text',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        //originally
        //$this->_render_page('users/auth/edit_group', $this->data);

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/edit_group_page", $this->data);
        $this->load->view("common_module/footer");
    }


    public
    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public
    function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public
    function _render_page($view, $data = null, $returnhtml = false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }


    /*--------------------------------------------------------------------------------------------------------------------*/
    /*
     * custom functions below
     * author: Mahmudur Rahman
     * Web Dev : RS Soft
    */


    public
    function showNeedPermission()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('need_permission');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/need_permission_page");
        $this->load->view("common_module/footer");
    }

    public
    function showDoesNotExist()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('does_not_exist');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/does_not_exist_page");
        $this->load->view("common_module/footer");
    }

    public
    function showPaymentMethodErrorPage()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }
        $this->lang->load('payment_method_error');

        $data = array();

        $data['payment_method'] = $this->lang->line('unknown_text');
        $data['error_type'] = $this->lang->line('unknown_text');

        if (isset($_REQUEST['payment_method'])) {
            if (!($_REQUEST['payment_method'] == null || $_REQUEST['payment_method'] == '')) {
                $data['payment_method'] = $_REQUEST['payment_method'];
            }
        }

        if (isset($_REQUEST['error_type'])) {
            if (!($_REQUEST['error_type'] == null || $_REQUEST['error_type'] == '')) {
                $data['error_type'] = $_REQUEST['error_type'];
            }
        }


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/payment_method_error_page", $data);
        $this->load->view("common_module/footer");
    }

    public
    function showMaintenancePage()
    {
        $data = array();

        $html = 'Site Under Maintenance';

        $site_maintenance_html = $this->custom_settings_library->getASettingsValue('general_settings', 'site_maintenance_html');

        if ($site_maintenance_html != '' && $site_maintenance_html != false) {
            $html = $site_maintenance_html;
        }

        $data['html'] = $html;


        $this->load->view("users/auth/custom_folder/maintenance_page", $data);

    }

    public
    function activateUser($id)
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        // do we have the right userlevel?
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $id = (int)$id;
            $this->ion_auth->activate($id);

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'user',                                                                 //3.    $type
                $id,                                                                    //4.    $type_id
                'activated',                                                            //5.    $activity
                'admin',                                                                //6.    $activity_by
                'admin',                                                                //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->activationEmail($id);

        }

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('activate_success', 'activate_success');
        redirect('users/auth', 'refresh');
    }

    public
    function deactivateUser($id)
    {

        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        // only superadmin can deactivate superadmins and admins
        if (!$this->ion_auth->in_group('superadmin') && ($this->ion_auth->in_group('superadmin', $id) || $this->ion_auth->in_group('administrator', $id))) {
            redirect('users/auth/need_permission');
        }


        $id = (int)$id;
        $this->ion_auth->deactivate($id);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $id,                                                                    //4.    $type_id
            'deactivated',                                                          //5.    $activity
            'admin',                                                                //6.    $activity_by
            'admin',                                                                //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->deactivationEmail($id);

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('deactivate_success', 'deactivate_success');
        redirect('users/auth', 'refresh');
    }

    /*does not delete permanaently, just makes deletion status 1*/
    public
    function deleteUser($id)
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        // only superadmin can delete superadmins and admins
        if (!$this->ion_auth->in_group('superadmin') && ($this->ion_auth->in_group('superadmin', $id) || $this->ion_auth->in_group('administrator', $id))) {
            redirect('users/auth/need_permission');
        }

        $id = (int)$id;

        /*first deactivate , then delete*/
        $this->ion_auth->deactivate($id);
        $this->Custom_auth_model->deleteUser($id);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'user',                                                                 //3.    $type
            $id,                                                                    //4.    $type_id
            'deleted',                                                              //5.    $activity
            'admin',                                                                //6.    $activity_by
            'admin',                                                                //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->session->set_flashdata('success', 'success');
        $this->session->set_flashdata('flash_user_id', $id);
        $this->session->set_flashdata('delete_success', 'delete_success');


        redirect('users/auth', 'refresh');
    }

    /*------------------------------User Groups---------------------------*/
    public
    function showUserGroups()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('groups');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        // do we have the right userlevel?
        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        } else {
            $data['all_groups'] = $this->ion_auth->groups()->result();

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/groups_page", $data);
            $this->load->view("common_module/footer");
        }
    }

    private function activationEmail($id)
    {

        $user = $this->Custom_auth_model->getUser($id);

        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Custom_auth_model->getEmailTempltateByType('user_activation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($id);

                $actual_link = $base_url . 'users/auth/login';

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        } else {
            return false;
        }

    }

    private function deactivationEmail($id)
    {

        $user = $this->Custom_auth_model->getUser($id);

        if ($user) {
            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Custom_auth_model->getEmailTempltateByType('user_deactivation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($id);

                $actual_link = $base_url . 'users/auth/login';

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        } else {
            return false;
        }

    }

    public function sendPasswordViaMail($user_id)
    {

        if (!$this->ion_auth->is_admin()) {
            redirect('users/auth/need_permission');
        }

        // only super admin can reset admin passwords
        if (!$this->ion_auth->in_group('superadmin') && $this->ion_auth->is_admin($user_id)) {
            redirect('users/auth/need_permission');
        }

        /*generate and update this user's password and send it*/
        /*$password = $this->alphaNum(8, false, false, false);
        $hashed_password = $this->ion_auth_model->hash_password($password);
        $this->Custom_auth_model->updateNewPassword($hashed_password,$user_id);
        $this->snd_pw_v_mail($user_id, $password);*/

        /*by ion auth forgot password <starts>*/
        $link_send_success = false;
        $user = $this->Custom_auth_model->getUser($user_id);

        if ($user) {
            if ($user->email != null && $user->email != '') {

                $link_send_success = $this->ion_auth->forgotten_password($user->email);
            }
        }
        /*by ion auth forgot password <ends>*/

        if ($link_send_success) {
            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('password_send_success', 'password_send_success');
            $this->session->set_flashdata('flash_user_id', $user->id);
        } else {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('password_send_error', 'password_send_error');
            $this->session->set_flashdata('flash_user_id', $user->id);
        }

        redirect('users/auth');

    }

    private function snd_pw_link_v_mail($user_id)
    {
        /*by ion auth forgot password <starts>*/
        $link_send_success = false;
        $user = $this->Custom_auth_model->getUser($user_id);

        if ($user) {
            if ($user->email != null && $user->email != '') {

                $link_send_success = $this->ion_auth->forgotten_password($user->email);

            }
        }
        /*by ion auth forgot password <ends>*/
    }

    private function snd_pw_v_mail($user_id, $password)
    {
        $this->lang->load('auth_password_email');

        $user = $this->Custom_auth_model->getUser($user_id);


        if ($user) {

            $to = $user->email;
            //$to = 'mahmud@sahajjo.com';

            $subject = $this->lang->line('password_change_subject_text');
            $message = sprintf($this->lang->line('password_change_message_text'), $password);

            $mail_data['to'] = $user->email;
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);

        }
    }

    private function sendEmail($mail_data)
    {

        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');
        $site_email = $this->custom_settings_library->getASettingsValue('general_settings', 'site_email');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if (!$site_email) {
            $site_email = 'prosperis@info.com';
        }

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);

            $this->email->subject($mail_data['subject']);
            $this->email->message(PROSPERIS_MAIL_TOP . $mail_data['message'] . PROSPERIS_MAIL_BOTTOM);
            $this->email->set_mailtype("html");

            /*echo '<hr>'.'<br>';
            echo $mail_data['subject'].'<br>';
            echo $mail_data['message'],'<br>';
            echo '<hr>'.'<br>';*/
            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }

    private function getBaseUrl($id)
    {
        $base_url = '';
        if ($this->ion_auth->in_group('admin', $id)) {
            $base_url = $this->config->item('office_base_url');
        } else if ($this->ion_auth->in_group('employer', $id) || $this->ion_auth->in_group('organization_contact', $id)) {
            $base_url = $this->config->item('partner_base_url');
        } else if ($this->ion_auth->in_group('employee', $id)) {
            $base_url = $this->Utility_model->makeThriftUrlWithEmployerSubdomain($this->config->item('thrift_base_url'), $id);
        } else if ($this->ion_auth->in_group('trustee', $id)) {
            $base_url = $this->config->item('trustee_base_url');
        }

        return $base_url;
    }


}
