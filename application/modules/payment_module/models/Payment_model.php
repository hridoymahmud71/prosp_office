<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

    }

    public function getNonPaystackCustomerEmployees()
    {
        $this->db->select('
                        u.id as id,
                        u.email as email,
                        u.created_on as created_on,                        
                        u.last_login as last_login,
                        u.active as active,
                        u.first_name as first_name,
                        u.last_name as last_name,
                        u.company as company,
                        u.phone as phone,
                        ug.id as group_serial,
                        ug.group_id as group_id,
                        ug.user_id as user_id,
                        ud.paystack_customer_code,
                        ud.paystack_customer_id,
                        ud.paystack_integration,
        ');

        $this->db->from('users as u');
        $this->db->where('u.deletion_status', 0);
        $this->db->where('u.verification', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id', 7); // employee group_id 7

        $this->db->join('rspm_tbl_user_details as ud', 'ug.user_id=ud.user_id');

        $this->db->group_start();
        $this->db->where('ud.paystack_customer_code', null);
        $this->db->or_where('ud.paystack_customer_id', 0);
        $this->db->group_end();

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function updateEmployeeAsPaystackCustomer($upd_data,$user_id)
    {
        $this->db->where('user_id',$user_id);
        $this->db->update('rspm_tbl_user_details',$upd_data);
    }

    public function getUserWithEmail($email)
    {
        $this->db->select('*');

        $this->db->from('users as u');
        $this->db->where('u.email', $email);

        $query = $this->db->get();
        $row = $query->row();

        return $row;
    }



}