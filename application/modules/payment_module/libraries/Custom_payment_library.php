<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *   This Library depends on the Settings library
 *
 * */

/*use Flutterwave\Bin;
use Flutterwave\Account;
use Flutterwave\FlutterEncrypt;
use Flutterwave\Flutterwave;*/


class Custom_payment_library
{

    public $CI;

    public $paystack_test_mode = true;
    public $paystack_sk = false;

    /*public $flutterwave_env = 'staging';
    public $flutterwave_mk = false;
    public $flutterwave_ak = false;
    public $flutterwave_version = '2';*/

    public function __construct()
    {

        $this->CI = &get_instance();

        $this->CI->load->model('payment_module/Payment_model');

        $this->CI->load->library('custom_log_library');
        $this->CI->load->library('settings_module/Custom_settings_library');

        $this->paystack_test_mode = $this->getPaystackTestmode();
        $this->paystack_sk = $this->paystack_test_mode ? $this->getPaystackKey('paystack_test_secret_key') : $this->getPaystackKey('paystack_live_secret_key');

       /* $this->flutterwave_env = $this->getFlutterwaveEnvronment();
        $this->flutterwave_mk = $this->flutterwave_env == 'staging' ? $this->getFlutterwaveKey('flutterwave_test_merchant_key') : $this->getFlutterwaveKey('flutterwave_live_merchant_key');
        $this->flutterwave_ak = $this->flutterwave_env == 'staging' ? $this->getFlutterwaveKey('flutterwave_test_api_key') : $this->getFlutterwaveKey('flutterwave_live_api_key');*/

        require APPPATH . 'libraries/paystack-php-master/src/autoload.php';
        require_once APPPATH . 'libraries/vendor/autoload.php';
    }


    /*------------------------------------utility <starts>--------------------------------------------------------------*/

    public function ggbb()
    {
        $ptsk = $this->getPaystackKey('paystack_test_secret_key');
        $ptpk = $this->getPaystackKey('paystack_test_public_key');
        $pltsk = $this->getPaystackKey('paystack_live_secret_key');
        $pltpk = $this->getPaystackKey('paystack_live_public_key');

        /*$ftmk = $this->getPaystackKey('flutterwave_test_merchant_key');
        $ftak = $this->getPaystackKey('flutterwave_test_api_key');
        $flmk = $this->getPaystackKey('flutterwave_live_merchant_key');
        $flak = $this->getPaystackKey('flutterwave_live_api_key');*/

        echo '$ptsk :' . $ptsk;
        echo '<br>';
        echo '$ptpk :' . $ptpk;
        echo '<br>';
        echo '$pltsk :' . $pltsk;
        echo '<br>';
        echo '$pltpk :' . $pltpk;
        echo '<br>';
        echo '$paystack_sk :' . $this->paystack_sk;
        echo '<br>';
       /* echo '<hr>';
        echo '$ftmk :' . $ftmk;
        echo '<br>';
        echo '$ftak :' . $ftak;
        echo '<br>';
        echo '$flmk :' . $flmk;
        echo '<br>';
        echo '$flak :' . $flak;
        echo '<br>';
        echo '$fluutterwave_env :' . $this->flutterwave_env;
        echo '<br>';
        echo '$flutterwave_mk :' . $this->flutterwave_mk;
        echo '<br>';
        echo '$flutterwave_ak :' . $this->flutterwave_ak;
        echo '<br>';*/
    }

    public function getPaystackKey($which_key)
    {
        $key = $this->CI->custom_settings_library->getASettingsValue('payment_settings', $which_key);
        if ($key == '' || $key == null) {
            return false;
        } else {
            return $key;
        }
    }

    public function getPaystackTestmode()
    {
        $mode = $this->CI->custom_settings_library->getASettingsValue('payment_settings', 'paystack_test_mode');
        if ($mode == '' || $mode == null || $mode == 'on') {
            return true;
        } else if ($mode == 'off') {
            return false;
        }

        return true;
    }

    /*public function getFlutterwaveKey($which_key)
    {
        $key = $this->CI->custom_settings_library->getASettingsValue('payment_settings', $which_key);
        if ($key == '' || $key == null) {
            return false;
        } else {
            return $key;
        }
    }

    public function getFlutterwaveEnvronment()
    {
        $env = $this->CI->custom_settings_library->getASettingsValue('payment_settings', 'flutterwave_environment');
        if ($env == '' || $env == null) {
            return 'staging';
        } else {
            return $env;
        }

    }*/

    //will be called by outside function
    public function getPayStackSecretKey()
    {
        return $this->paystack_sk;
    }
    /*------------------------------------utility <ends>--------------------------------------------------------------*/

    /*------------------------------------paystack <starts>-----------------------------------------------------------*/

    public function createSinglePaystackCustomer($customer, $user_id)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $created_customer = $paystack->customer->create($customer);

                $ret_data['got_data'] = $created_customer;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }


    public function getPaystackCustomerList()
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $paystack_customers = $paystack->customer->getList(['perPage' => 200]);

                $ret_data['got_data'] = $paystack_customers;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function createPaystackPlan($plan_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $created_plan = $paystack->plan->create($plan_data);

                $ret_data['got_data'] = $created_plan;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function updatePaystackPlan($plan_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $updated_plan = $paystack->plan->update($plan_data);

                $ret_data['got_data'] = $updated_plan;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function initializePaystackTransaction($transaction_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $created_transaction = $paystack->transaction->initialize($transaction_data);

                $ret_data['got_data'] = $created_transaction;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function chargeAuthorizationPaystackTransaction($transaction_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $charged_transaction = $paystack->transaction->chargeAuthorization($transaction_data);

                $ret_data['got_data'] = $charged_transaction;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function verifyPaystackTransaction($transaction_verification_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $verified_transaction = $paystack->transaction->verify($transaction_verification_data);

                $ret_data['got_data'] = $verified_transaction;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function getSinglePaystackTransaction($transaction_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $transaction = $paystack->transaction->fetch($transaction_data);

                $ret_data['got_data'] = $transaction;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function getPaystackTransactionList($transaction_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $transaction_list = $paystack->transaction->getList($transaction_data);

                $ret_data['got_data'] = $transaction_list;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function createPaystackSubscription($subscription_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $created_subscription = $paystack->subscription->create($subscription_data);

                $ret_data['got_data'] = $created_subscription;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function disablePaystackSubscription($subscription_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $disabled_subscription = $paystack->subscription->disable($subscription_data);

                $ret_data['got_data'] = $disabled_subscription;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function fetchPaystackSubscription($subscription_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {

            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $fetched_subscription = $paystack->subscription->fetch($subscription_data);

                $ret_data['got_data'] = $fetched_subscription;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function getListPaystackSubscription($subscription_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $listed_subscription = $paystack->subscription->getList($subscription_data);

                $ret_data['got_data'] = $listed_subscription;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function getPaystackBankList()
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $bank_list = $paystack->bank->getList();

                $ret_data['got_data'] = $bank_list;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function createPaystackTransferRecipient($recipient_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);

            try {
                $created_recipient = $paystack->transferrecipient->create($recipient_data);

                $ret_data['got_data'] = $created_recipient;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }


        return $ret_data;
    }

    public function initiatePaystackTransfer($transfer_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $initiated_transfer = $paystack->transfer->initiate($transfer_data);

                $ret_data['got_data'] = $initiated_transfer;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }

        return $ret_data;
    }

    public function getPaystackTransferrecipientList($transfer_data)
    {
        $ret_data = array();
        $ret_data['got_data'] = false;
        $ret_data['error_response_object'] = false;
        $ret_data['error_message'] = false;
        $ret_data['error_sk'] = false;
        if ($this->paystack_sk) {
            $paystack = new Yabacon\Paystack($this->paystack_sk);
            try {
                $transferrecipient_list = $paystack->transferrecipient->getList($transfer_data);

                $ret_data['got_data'] = $transferrecipient_list;

            } catch (\Yabacon\Paystack\Exception\ApiException $e) {
                $ret_data['error_response_object'] = $e->getResponseObject();
                $ret_data['error_message'] = $e->getMessage();
            }
        } else {
            $ret_data['error_sk'] = 'Secret Key Error';
        }

        return $ret_data;
    }

    /*------------------------------------paystack <ends>-------------------------------------------------------------*/


    /*------------------------------------flutterwave <starts>--------------------------------------------------------*/
    /*public function flutterwaveBinCheck()
    {
        $merchantKey = $this->flutterwave_mk;
        $apiKey = $this->flutterwave_ak;
        $env = $this->flutterwave_env;

        if ($merchantKey && $apiKey) {
            $version = $this->flutterwave_version;
            Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env, $version); //version is optional and can be 1 or 2, when not passed, it defaults to 1

            $first6digits = "763537";
            $result = Bin::check($first6digits);

            echo '<pre>';
            print_r($result);
            echo '</pre>';
        }

        if (!$merchantKey) {
            echo 'Merchant Key Error';
        }
        if (!$apiKey) {
            echo 'API Key Error';
        }

        //$result is an instance of ApiResponse class which has
        //methods like getResponseData(), getStatusCode(), getResponseCode(), isSuccessfulResponse()
    }

    public function flutterwaveEncrypt($data)
    {
        $apiKey = $this->flutterwave_ak;
        $encrypted = FlutterEncrypt::encrypt3Des($data, $apiKey);
        return $encrypted;
    }

    public function flutterwaveRecurrentPaymentInitiate()
    {
        $merchantKey = $this->flutterwave_mk;
        $apiKey = $this->flutterwave_ak;
        $env = $this->flutterwave_env;

        if ($merchantKey && $apiKey) {
            $version = $this->flutterwave_version;
            Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env, $version); //version is optional and can be 1 or 2, when not passed, it defaults to 1

            $accountNumber = "13135423231534"; //account number you want to charge
            $result = Account::initiate($accountNumber);


            echo '<pre>';
            print_r($result);
            echo '</pre>';
        }

        if (!$merchantKey) {
            echo 'Merchant Key Error';
        }
        if (!$apiKey) {
            echo 'API Key Error';
        }

        //$result is an instance of ApiResponse class which has
        //methods like getResponseData(), getStatusCode(), getResponseCode(), isSuccessfulResponse()
    }

    public function flutterwaveCreateUser()
    {


        $post_data['username'] = urlencode($this->flutterwaveEncrypt('Samuel Jackson'));
        $post_data['password'] = urlencode($this->flutterwaveEncrypt('123456'));
        $post_data['merchantid'] = $this->flutterwave_mk;
        $post_data['pin'] = urlencode($this->flutterwaveEncrypt('654321'));
        $post_data['email'] = urlencode($this->flutterwaveEncrypt('sam@jackson.com'));
        $post_data['institution'] = urlencode($this->flutterwaveEncrypt('hollywood'));

        $fields_string='';
        foreach($post_data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        $ch = curl_init();                    // initiate curl
        $url = "http://staging1flutterwave.co:8080/pwc/rest/card/mvva/adduser"; // where you want to post data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something

        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data); // define what you want to post
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data)); // define what you want to post
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string); // define what you want to post

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format

        $headers[] = 'Content-Type: application/json';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //making security ssl check false
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch); // execute

        curl_close($ch); // close curl handle

        echo '<pre>';
        print_r($output); // show output
        echo '</pre>';
    }*/
    /*------------------------------------flutterwave <ends>----------------------------------------------------------*/
}