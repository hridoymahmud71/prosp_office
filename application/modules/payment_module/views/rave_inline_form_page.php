<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">

                <h4 class="page-title float-left"><?= lang('page_title_text') ?></h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <?php if ($this->session->flashdata('unsuccessful')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }
            ?>
        </div>
    <?php } ?>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">
                    Rave Inline Modal
                </h4>
                <div class="row">
                    <div class="col-md-12">
                        Empty Card
                        <!-- data-redirect_url="payment_module/PaymentController/raveCallback" -->
                        <hr>
                        <!-- paste inside <starts> -->
                        <a class="flwpug_getpaid" data-PBFPubKey="FLWPUBK-37845f4587723ac04a03e0b787868802-X"
                           data-txref="rave-checkout-1517146214" data-amount="" data-customer_email="user@example.com"
                           data-currency="USD" data-pay_button_text="" data-country="US" data-custom_title="hhjj"
                           data-custom_description="" data-redirect_url="payment_module/PaymentController/raveCallback" data-custom_logo=""
                           data-payment_method="both" data-exclude_banks=""></a>

                        <script type="text/javascript"
                                src="http://flw-pms-dev.eu-west-1.elasticbeanstalk.com/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
                        <!-- paste inside <ends> -->
                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>


</div> <!-- container -->

<script>
    $(function () {

        $('body').on('load', function () {
            $('.onload-click-btn').trigger("click");
        });

    });
</script>


